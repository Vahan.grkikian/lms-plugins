<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');

/**
 * @desc Include js to admin side
 * @param $hook
 * @date 28.02.2018 (11:00)
 */
function lms_admin_assets($hook) {
    wp_enqueue_script( 'lms-helper-admin', plugins_url( '/admin/assets/js/lms-helper-admin.js?v=58', __FILE__ ), array( 'jquery' ) );
    wp_localize_script( 'lms-helper-admin', 'ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
    wp_enqueue_style( 'lms-helper-css', plugins_url( '/admin/assets/css/lms-helper-login.css', __FILE__ ) );


    wp_enqueue_script( 'lms-datatable-js', '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'lms-datatable-buttons-js', '//cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'lms-datatable-html5-js', '//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'lms-datatable-pdf-js', '//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'lms-datatable-fonts-js', '//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js', array( 'jquery' ) );
    wp_enqueue_style( 'lms-datatable-css', '//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' );
    wp_enqueue_style( 'lms-datatable-buttons-css', '//cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css' );
}

function lms_admin_menu_settings_submenu(){
    add_submenu_page( '/admin.php?page=boss_options', 'Settings', 'Settings','manage_options', 'setings_login_redirect_section');
}

function setings_login_redirect_section(){
    die('123');
}

/**
 * @desc Create product for membership plan and add the redirect page to plan.
 * @param $post_id
 * @param $post
 * @return false
 * @date 05.03.2018 (11:41)
 */
function lms_helper_memebership_prod_create($post_id, $post ) {
    // If this is just a revision, don't send the email.
    if ( wp_is_post_revision( $post_id ) )
        return;
    global $wpdb, $wp_query;
    $user_id = get_current_user_id();
    if( !$post_id || get_post_type($post_id) != 'membership_plan' )return false;

    $key = '';
    $price = '0';
    $redirect_page = '';
    $prod_id = '';
    if ( isset($_POST['rpwcm_plan_key']) && !empty($_POST['rpwcm_plan_key']) ) {
        $key = $_POST['rpwcm_plan_key'];
    }
    if ( empty($key) ){
        $key = isset(get_post_meta($post_id,'key')[0]) ? get_post_meta($post_id,'key')[0] : '';
    }
    if ( isset($_POST['lms_helper_memb_price']) && !empty($_POST['lms_helper_memb_price']) ){
        $price = $_POST['lms_helper_memb_price'];
        update_post_meta( $post_id, 'lms_helper_memb_price', $price );
    }
    if ( isset($_POST['lms_helper_memb_redirect']) && !empty($_POST['lms_helper_memb_redirect']) ){
        $redirect_page = $_POST['lms_helper_memb_redirect'];
        update_post_meta( $post_id, 'lms_helper_memb_redirect', $redirect_page );
    }
    if ( !empty(get_post_meta( $post_id,'_memb_plan_prod')[0] ) ){
        $prod_id = get_post_meta( $post_id,'_memb_plan_prod' )[0];
    }else {
        $prod_id = wp_insert_post( array(
            'post_title' => get_the_title( $post_id ),
            'post_content' => get_the_title( $post_id ),
            'post_status' => 'publish',
            'post_type' => "product",
        ) );
    }

    if( !is_wp_error($prod_id) ){

        //Set product type
        wp_set_object_terms( $prod_id, 'simple', 'product_type' );

        update_post_meta( $prod_id, '_visibility', 'visible' );
        update_post_meta( $prod_id, '_stock_status', 'instock');
        update_post_meta( $prod_id, 'total_sales', '0' );
        update_post_meta( $prod_id, '_downloadable', 'no' );
        update_post_meta( $prod_id, '_virtual', 'yes' );
        update_post_meta( $prod_id, '_rpwcm', 'yes' );
        update_post_meta( $prod_id, '_regular_price', $price );
        update_post_meta( $prod_id, '_sale_price', $price );
        update_post_meta( $prod_id, '_purchase_note', '' );
        update_post_meta( $prod_id, '_featured', 'no' );
        update_post_meta( $prod_id, '_weight', '' );
        update_post_meta( $prod_id, '_length', '' );
        update_post_meta( $prod_id, '_width', '' );
        update_post_meta( $prod_id, '_height', '' );
        update_post_meta( $prod_id, '_sku', '' );
        update_post_meta( $prod_id, '_product_attributes', array() );
        update_post_meta( $prod_id, '_sale_price_dates_from', '' );
        update_post_meta( $prod_id, '_sale_price_dates_to', '' );
        update_post_meta( $prod_id, '_price', $price );
        update_post_meta( $prod_id, '_sold_individually', '' );
        update_post_meta( $prod_id, '_manage_stock', 'no' );
        update_post_meta( $prod_id, '_backorders', 'no' );
        update_post_meta( $prod_id, '_stock', NULL );
        update_post_meta( $prod_id,'_rpwcm_plans',$post_id );
        update_post_meta( $prod_id,'_product_author', $user_id);

        //add new product to new membership plan
        update_post_meta( $post_id, '_memb_plan_prod', $prod_id );
        update_post_meta( $prod_id, 'lms_helper_memb_redirect', $redirect_page );

        if( !get_post_meta($prod_id,'jojo')[0]  ) {

            $redLocation = "add-to-cart={Event Product ID (Hidden Field):11}";
            $redLocationAction = "checkout";
            $template_selection = 'simpleregistration';
            $returnValue = json_decode('{"0":{"title":"'.get_the_title( $post_id ).'","fields":[{"type":"name","id":4,"label":"Name","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","nameFormat":"advanced","inputs":[{"id":"4.2","label":"Prefix","name":"","choices":[{"text":"Mr.","value":"Mr.","isSelected":false,"price":""},{"text":"Mrs.","value":"Mrs.","isSelected":false,"price":""},{"text":"Miss","value":"Miss","isSelected":false,"price":""},{"text":"Ms.","value":"Ms.","isSelected":false,"price":""},{"text":"Dr.","value":"Dr.","isSelected":false,"price":""},{"text":"Prof.","value":"Prof.","isSelected":false,"price":""},{"text":"Rev.","value":"Rev.","isSelected":false,"price":""}],"isHidden":true,"inputType":"radio"},{"id":"4.3","label":"First","name":""},{"id":"4.4","label":"Middle","name":"","isHidden":true},{"id":"4.6","label":"Last","name":""},{"id":"4.8","label":"Suffix","name":"","isHidden":true}],"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"text","id":7,"label":"Company","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"phone","id":5,"label":"Phone","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"phoneFormat":"standard","formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","form_id":"","productField":"","displayOnly":""},{"type":"email","id":6,"label":"Email","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","emailConfirmEnabled":"","displayOnly":""},{"type":"hidden","id":10,"label":"Is User logged in? (Hidden Field)","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"{user:user_login}","choices":"","conditionalLogic":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"username","id":9,"label":"Username","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":{"actionType":"show","logicType":"all","rules":[{"fieldId":"10","operator":"is","value":""}]},"productField":"","enablePasswordInput":false,"maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"password","id":8,"label":"Password","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":[{"id":"8","label":"Enter Password","name":""},{"id":"8.2","label":"Confirm Password","name":""}],"displayOnly":true,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":{"actionType":"show","logicType":"all","rules":[{"fieldId":"10","operator":"is","value":""}]},"productField":"","passwordStrengthEnabled":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false},{"type":"hidden","id":11,"label":"Event Product ID (Hidden Field)","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":true,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"event_product_id","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""}],"id":25,"useCurrentUserAsAuthor":true,"postContentTemplateEnabled":false,"postTitleTemplateEnabled":false,"postTitleTemplate":"","postContentTemplate":"","lastPageButton":null,"pagination":null,"firstPageCssClass":null,"confirmations":[{"id":"59fa1bc389e40","name":"Default Confirmation - Do not alter","isDefault":true,"type":"redirect","message":"","url":"'.get_site_url().'\/'.$redLocationAction.'\/","pageId":0,"queryString":"'.$redLocation.'","disableAutoformat":false,"conditionalLogic":[]}],"notifications":[]},"version":"2.2.5"}', true);

            GFAPI::add_forms($returnValue);

            $formtable = $wpdb->prefix . "rg_form";
            $formid = $wpdb->get_var(
                "
              SELECT id 
              FROM $formtable
              ORDER BY ID DESC limit 0,1
            "
            );
            $tablename = $wpdb->prefix . "gf_addon_feed";
            $wpdb->insert(
                $tablename,
                array(
                    'form_id' => $formid,
                    'is_active' => '1',
                    'feed_order' => '0',
                    'meta' => '{"feedName":"User Registration","feedType":"create","username":"9","first_name":"4.3","last_name":"4.6","nickname":"4.3","displayname":"nickname","email":"6","password":"8","role":"subscriber","userMeta":[{"key":"billing_first_name","value":"4.3","custom_key":""},{"key":"billing_last_name","value":"4.6","custom_key":""},{"key":"billing_email","value":"6","custom_key":""}],"bpMeta":"","sendEmail":"0","userActivationEnable":"0","userActivationValue":"email","feed_condition_conditional_logic":"1","feed_condition_conditional_logic_object":{"conditionalLogic":{"actionType":"show","logicType":"all","rules":[{"fieldId":"10","operator":"is","value":""}]}}}',
                    'addon_slug' => 'gravityformsuserregistration'
                )
            );

            $siteurl = plugins_url('admin-pages/admin/partials/templates/img/');
            $template_content = file_get_contents(__DIR__.'/templates/'.$template_selection.'.txt');

            $template_content = str_replace("#formid#",$formid ,$template_content);
            $template_content = str_replace("#url#",$siteurl ,$template_content);

            $page_id = wp_insert_post( array(
                'post_title' => 'Landing Page for '.get_the_title( $post_id ),
                'post_content' => $template_content,
                'post_author' => $user_id,
                'post_status' => 'publish',
                'post_type' => "page",
                'comment_status' => 'closed'
            ));

            if (!$page_id) return false;
            update_post_meta( $page_id, '_wp_page_template', 'page-fullwidth.php' );
            update_post_meta( $page_id, '_yoast_wpseo_meta-robots-noindex', '1' );
            update_post_meta( $post_id,'postpagesresult', $page_id );
            update_post_meta( $page_id,'_event_product_id', $prod_id );

            update_post_meta($prod_id,'jojo',1);
        } // end if

    }

    return;
}

/**
 * @desc Update event data.
 * @date 14.03.2018 (11:09)
 */
function update_post_back_functions(){
        $post_id = isset($_POST['post_ID']) ?   $_POST['post_ID']   :   "";

        if (!empty($post_id) && isset($_POST['has_newsletter']) && !empty($_POST['has_newsletter']) ){
            $has_newsletter = $_POST['has_newsletter'];
            update_post_meta( $post_id, 'has_newsletter', $has_newsletter );
        }
}

/**
 * @desc Add price to membership plan product.
 * @date 05.03.2018 (18:00)
 */
function lms_helper_add_price_to_memb_plan(){

    $post_id = $_REQUEST['post'];
    $post_type = get_post($post_id)->post_type;
    $user_id = get_current_user_id();

    $page = get_post_meta($post_id,'lms_helper_memb_redirect')[0];
    $args = array(
        'author'        =>  $user_id,
        'post_type'     =>  'page',
        'orderby'       =>  'post_date',
        'order'         =>  'ASC',
        'posts_per_page' => -1 // no limit
    );
    $current_user_posts = get_posts( $args );
    ?>
    <?php if ( $post_type == 'membership_plan' ): ?>
    <div id="rpwcm_membership_plan_products" class="postbox ">
        <h2 class="hndle ui-sortable-handle"><span>Membership Price</span></h2>
        <div class="inside">
            <div class="rpwcm_membership_plan_members_footer_container">
                <div class="rpwcm_membership_plan_members_footer_left"></div>
                <div class="rpwcm_membership_plan_members_footer_right">
                    <span>&#36;</span>
                    <input type="number" maxlength="7" name="lms_helper_memb_price" value="<?= get_post_meta($post_id,'lms_helper_memb_price')[0]; ?>" />
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
    <div id="rpwcm_membership_plan_redirects" class="postbox ">
        <h2 class="hndle ui-sortable-handle"><span>Membership Redirect Page</span></h2>
        <div class="inside">
            <div class="rpwcm_membership_plan_members_footer_container">
                <div class="rpwcm_membership_plan_members_footer_left"></div>
                <div class="rpwcm_membership_plan_members_footer_right">
                    <select name="lms_helper_memb_redirect">
                        <option>Choose the page</option>
                        <?php foreach ($current_user_posts as $post): ?>
                            <option
                              <?= ( $page == $post->guid ) ? 'selected' :  ''; ?>
                                    value="<?= $post->guid; ?>"><?= $post->post_title; ?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
    <?php endif; ?>
<?php 
}

/** 
 * @desc Add newsletter checkbox to event page.
 * @date 14.03.2018 (10:35)
 */
function add_newsletter_checkbox(){

    $post_id = $_REQUEST['post'];
    $post_type = get_post($post_id)->post_type;
    ?>
    <?php if ( $post_type == 'sfwd-courses' ): ?>
    <div id="rpwcm_membership_plan_products" class="postbox ">
        <h2 class="hndle ui-sortable-handle"><span>Has newsletter</span></h2>
        <div class="inside">
            <div class="rpwcm_membership_plan_members_footer_container">
                <div class="rpwcm_membership_plan_members_footer_left <?php echo get_post_meta($post_id,'has_newsletter')[0];?>" >
                    <input type="hidden" name="has_newsletter" value="N"> 
                    <input type="checkbox" name="has_newsletter" value="Y" <?php if(get_post_meta($post_id,'has_newsletter')[0] == "Y"):?>checked="checked"<?php endif?>/>
                </div>
                <div class="rpwcm_membership_plan_members_footer_right">
                    
                </div> 
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
    <?php endif; ?>
<?php 
}

/**
 * @globals $wp_roles
 * @desc Change all users roles names and remove not needed
 * @date 12.03.2018 (11:58)
 */
function add_theme_caps() {
    remove_role( 'group_leader' );

    //Remove all user roles
   /* if( get_role('wpseo_editor') || get_role('wpseo_manager') || get_role('shop_manager') || get_role('customer') || get_role('group_leader') || get_role('reporting')  ){
        remove_role( 'wpseo_editor' );
        remove_role( 'wpseo_manager' );
        remove_role( 'shop_manager' );
        remove_role( 'customer' );
        remove_role( 'group_leader' );
        remove_role( 'contributor' );
        remove_role( 'reporting' );

    }*/
    global $wp_roles;
    $wp_roles->roles['author']['name'] = 'Instructor';
    $wp_roles->role_names['author'] = 'Instructor';
    $wp_roles->roles['subscriber']['name'] = 'Student';
    $wp_roles->role_names['subscriber'] = 'Student';


}

function lms_helper_remove_menus(){

    $currentUserData = wp_get_current_user();
    $currentUserRole = array_shift($currentUserData->roles);
    //echo '<pre>' . print_r( $GLOBALS[ 'menu' ], TRUE) . '</pre>';
    switch ($currentUserRole) {
        case "reporting":
            remove_menu_page( 'edit.php?post_type=membership_plan' );
            remove_menu_page( 'edit.php?post_type=membership_plan' );
            remove_menu_page( 'vc-welcome' );
            break;
        case "author":
            remove_menu_page( 'integrations' );
            remove_menu_page( 'admin.php?page=boss_options' );
            remove_menu_page( 'users.php' );
            remove_menu_page( 'users.php' );
            remove_menu_page( 'user-new.php' );
            remove_menu_page( 'users.php?page=bp-signups' );
            remove_menu_page( 'users.php?page=users-user-role-editor.php' );
            remove_menu_page( 'users.php?page=bp-profile-setup' );
            remove_menu_page( 'users.php?page=gf-pending-activations' );
            remove_menu_page( 'plugins.php' );
            remove_menu_page( 'options-general.php' );
            remove_menu_page( 'edit.php?post_type=shop_order' );
            break;
        case "editor":
            remove_menu_page( 'integrations' );
            remove_menu_page( 'admin.php?page=boss_options' );
            remove_menu_page( 'users.php' );
            remove_menu_page( 'users.php' );
            remove_menu_page( 'user-new.php' );
            remove_menu_page( 'users.php?page=bp-signups' );
            remove_menu_page( 'users.php?page=users-user-role-editor.php' );
            remove_menu_page( 'users.php?page=bp-profile-setup' );
            remove_menu_page( 'users.php?page=gf-pending-activations' );
            remove_menu_page( 'plugins.php' );
            remove_menu_page( 'options-general.php' );
            remove_menu_page( 'edit.php?post_type=shop_order' );
            break;
    }

}

/**
 * @globals $wpdb
 * @desc Generate export data for Video views and Courses
 * @date 22.06.2018 (11:58)
 */
function lms_helper_generateExportData(){
    global $wpdb;
    $data = '';
    $export_type = esc_sql($_POST['type']);
    if ($export_type == 'course'){
        $table = $wpdb->prefix . 'courses_analytic';

        $results  = $wpdb->get_results("SELECT * FROM $table");
        if (!empty($results)){

            $data = $results;
        }
        else{
            $data = 'Not Found';
        }

    }
    elseif ($export_type == 'video'){
        $results = getVideoAnalyticDataForExport();
        if (!empty($results)){

            $data = $results;
        }
        else{
            $data = 'Not Found';
        }
    }
    else{
        $data = 'Not Found';
    }
    wp_send_json_success( $data );
    wp_die();
}

/**
 * @globals $wpdb
 * @desc Get Video views for generate analytic
 * @date 22.06.2018 (14:58)
 */
function getVideoAnalyticDataForExport(){
    global $wpdb;
    $video_table = $wpdb->prefix . 'player_analytic';
    $data = array();

    $sql_start = "SELECT `userid`,`videoid`, `date`, `time`,`user_data`,`course_name`,SUM(`total_view_time`) as total_view_time FROM `{$video_table}` WHERE `eventid` = 'start' group by `userid`,videoid";
    $start = $wpdb->get_results($sql_start);
    
    foreach ( $start as $key => $item ){
        if ( !empty($item->user_data) ){
            $userData = json_decode($item->user_data);
            $data[$key]['first_name'] = @$userData->first_name;
            $data[$key]['last_name'] = @$userData->last_name;
            $data[$key]['company'] = @$userData->company;
            $data[$key]['address'] = @$userData->address_1;
            $data[$key]['city'] = @$userData->city;
            $data[$key]['state'] = @$userData->state;
            $data[$key]['postcode'] = @$userData->postcode;
            $data[$key]['country'] = @$userData->country;
        }
        $data[$key]['course_name'] = $item->course_name;
        $data[$key]['first_viewed'] = $item->date.' '.$item->time;
        $data[$key]['videoid'] = $item->videoid;
        $data[$key]['total_view_time'] = $item->total_view_time;
    }

    $sql_end = "SELECT `userid`,`videoid`,`date`,`time` from `{$video_table}` where ID in ( SELECT max(id) as id FROM `wp_player_analytic` group by `userid`,videoid )";
    $end = $wpdb->get_results($sql_end);
    foreach ( $end as $key => $item_for_end ){
        $data[$key]['last_viewed'] = $item_for_end->date.' '.$item_for_end->time;
    }


    $sql_count = "SELECT userid,videoid, count(videoid) as count FROM `{$video_table}` where eventid = 'start' group by `userid`,videoid";
    $count = $wpdb->get_results($sql_count);
    foreach ( $count as $key => $item_for_count ){
        $data[$key]['times_viewed'] = $item_for_count->count;
    }

    return $data;
}

/**
 * @desc Add settings menu of webex
 * @date 17.07.2018 (10:40)
 */
function wp_gotwebex_wp_menu() {
    add_options_page(
        'Webex Settings',
        'Webex Settings',
        'manage_options',
        'webex-settings',
        'webex_settings_options_page'
    );
}

/**
 * @desc Set template for webex setup page
 * @date 17.07.2018 (10:41)
 */
function webex_settings_options_page(){
    require_once(dirname(__FILE__).'/templates/webex-settings.php');
}

/**
 * @desc Set template for webex setup page
 * @date 17.07.2018 (10:41)
 */
function prefix_admin_save_webex_credentials(){
    $errorCheck = false;
    $teacher_email = ($_REQUEST['teacher_email']) ? $_REQUEST['teacher_email'] : $errorCheck = true;
    $webexId = ($_REQUEST['webexId']) ? $_REQUEST['webexId'] : $errorCheck = true;
    $webex_pass = ($_REQUEST['webex_pass']) ? $_REQUEST['webex_pass'] : $errorCheck = true;
    $siteID = ($_REQUEST['siteID']) ? $_REQUEST['siteID'] : $errorCheck = true;
    $PartnerID = ($_REQUEST['PartnerID']) ? $_REQUEST['PartnerID'] : $errorCheck = true;

    if( $errorCheck == true ){
        wp_die( __( 'Please fill the all required fields' ), __( 'Error' ), array(
            'response' 	=> 403,
            'back_link' => 'admin.php?page=webex-settings',
        ) );
    }

    $teacher = $user = get_user_by( 'email', $teacher_email );

    if ( empty($teacher) ){
        wp_die( __( 'User with this email not found' ), __( 'Error' ), array(
            'response' 	=> 403,
            'back_link' => 'admin.php?page=webex-settings',
        ) );
    }
    $data = array(
      'id' => $webexId,
      'pass' => $webex_pass,
      'siteID' => $siteID,
      'partnerID' => $PartnerID
    );
    update_user_meta($teacher->ID,'webex_cred',$data);

    wp_die( __( 'SUCCESS' ),__( 'SUCCESS' ), array(
        'response' 	=> 200,
        'back_link' => 'admin.php?page=webex-settings',
    ) );
}

function testWebex(){
/*    $url = 'https://apidemoeu.webex.com/WBXService/XMLService';

    $xmlData = '<?xml version="1.0" encoding="UTF-8"?>
                <serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <header>
                        <securityContext>
                            <siteName>apidemoeu</siteName>
                            <webExID>lms_test</webExID>
                            <password>Q!w2e3r4t5</password>   
                        </securityContext>
                    </header>
                        <body>
                        <bodyContent
                            xsi:type="java:com.webex.service.binding.meeting.CreateMeeting">
                            <accessControl>
                                <meetingPassword>pass123</meetingPassword>
                            </accessControl>
                            <metaData>
                                <confName>Sample Meeting</confName>
                                <agenda>Test</agenda>
                            </metaData>
                            <participants>
                                <maxUserNumber>4</maxUserNumber>
                                <attendees>
                                    <attendee>
                                        <person>
                                            <name>James Kirk</name>
                                            <email>Jkirk@sz.webex.com</email>
                                        </person>
                                    </attendee>
                                </attendees>
                            </participants>
                            <enableOptions>
                                <chat>true</chat>
                                <poll>true</poll>
                                <audioVideo>true</audioVideo>
                            </enableOptions>
                            <schedule>
                                <startDate>08/11/2018 10:10:10</startDate>
                                <openTime>900</openTime>
                                <joinTeleconfBeforeHost>true</joinTeleconfBeforeHost>
                                <duration>20</duration>
                                <timeZoneID>4</timeZoneID>
                            </schedule>
                            <telephony>
                                <telephonySupport>CALLIN</telephonySupport>
                                <extTelephonyDescription>
                                    Call 1-800-555-1234, Passcode 98765
                                </extTelephonyDescription>
                            </telephony>
                        </bodyContent>
                    </body>
                </serv:message>';

    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $xmlData );
    $result = curl_exec($ch);
    lms_show($result);die;
    curl_close($ch);*/

//lms_show(get_user_meta(38,'wp_capabilities',true));die;
}