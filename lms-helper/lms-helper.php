<?php
/*
Plugin Name: LMS Helper
Plugin URI:
Description: LMS Helper
Version: 1.0
Author: LMS Helper
*/
define("PLUGIN_NAME","lms-helper");
require_once ( __DIR__.'/back-functions.php' );
require_once ( __DIR__.'/front-functions.php' );
register_activation_hook( __FILE__, 'lms_helper_install' );

// Include styles and scripts
add_action('wp_enqueue_scripts', 'lms_helper_scripts_styles');
add_action('login_head', 'lms_helper_login');
add_action( 'admin_enqueue_scripts', 'lms_admin_assets' );

// Add forgot password option to user login page
add_action( 'login_form', 'lms_helper_forgot_pass' );

// Create order.
add_action( 'gform_after_submission', 'lms_helper_create_order', 10, 2 );

// Create product for membership plan.
add_action( 'publish_membership_plan', 'lms_helper_memebership_prod_create', 80, 2 );

// add the action
add_action( 'woocommerce_thankyou', 'lms_helper_memebership_redirect_button', 10, 1 );

// Add price field to memebership create page
add_action( 'edit_form_advanced', 'lms_helper_add_price_to_memb_plan' );

// Add newsletter checkbox to event page
add_action( 'edit_form_advanced', 'add_newsletter_checkbox' );

// Connect user to the membership plan
add_action( 'woocommerce_order_status_processing', 'lms_helper_woocommerce_order_status_completed', 10, 1 );

add_action( 'woocommerce_order_status_processing', 'webex_logic', 10, 1 );

//connect ro event update
add_action( 'post_updated', 'update_post_back_functions', 10, 3 );

//connect to order placement
//add_action( 'woocommerce_new_order', 'add_user_to_default_list' );

//add_action( 'admin_init', 'add_theme_caps');
add_action( 'admin_init', 'testWebex');

add_action('wp_login', 'lms_helper_admin_redirect');

//add_action( 'admin_menu', 'lms_helper_remove_menus' );

add_action( 'pre_get_posts', 'filter_cpt_listing_by_author_wpse_89233' );


add_action( 'template_redirect', 'get_custom_post_type_template' );

add_action( 'init', 'process_post' );

add_action( 'plugins_loaded', 'my_plugin_load_first' );

add_action("wp_ajax_lms_helper_generateExportData", "lms_helper_generateExportData");

add_action('admin_menu','wp_gotwebex_wp_menu');

add_action( 'admin_post_save_webex_credentials', 'prefix_admin_save_webex_credentials' );

add_action( 'admin_menu', 'lms_admin_menu_settings_submenu' );

add_filter( 'login_redirect', function( $url, $query, $user ) {

    $planIDs = get_user_meta($user->ID,'user_plan_id',true);
    $planID = '';

    if (empty($planIDs))return $url;
    if (is_array($planIDs)){
        $planID = $planIDs[0];
    }
    else {
        $planID = $planIDs;
    }
    $redirect_page = get_post_meta($planID,'lms_helper_memb_redirect',true);
    if (in_array('subscriber', $user->roles) && !empty($redirect_page)) {
        // redirect them to another URL, in this case, the homepage
        $url =  $redirect_page;
    }
    return $url;
}, 10, 3 );

?>