jQuery(document).ready(function () {

    var n = window.location.search.search("vc_action=vc_inline");
    if (n >= 0){
    //    jQuery('head title').html('Edit Page');
    }


    jQuery('button[name="rpwcm_plan_button"]').on('click',function () {
       var key =  jQuery('.membership_plan_details_key code').html();
       jQuery('input[name="rpwcm_plan_key"]').val(key);
    });

    if ( jQuery('body').hasClass('post-type-membership_plan') ){
        jQuery('.post-type-membership_plan #the-list tr').each(function () {
            var code = jQuery(this).find('th');
            if( jQuery(this).hasClass('author-other') && !code.children('label').length ){
                jQuery(this).remove();
            }

        });
    }
    var hambo = jQuery('.wp-editor-area');
    hambo.val( hambo.html() );
}); //Document Ready End

function LmsSelectExportForm(data) {
    if ( data == '' ) return false;

    $('.lms_analityc_report').css('display','none');
    $('.lms_analityc_report_video').css('display','none');
    $(".spinner").show();

    jQuery.ajax({
        type : "post",
        dataType : "json",
        url : ajax.ajaxurl,
        data : {action: "lms_helper_generateExportData", type : data},
        success: function(response) {
            if (response.success == true && data == 'course'){
                $('.lms_analityc_report').css('display','block');
                $('#lms_analityc_report').DataTable( {
                    dom: 'Bfrtip',
                    retrieve: true,
                    "searching": false,
                    data: response.data,
                    columns: [
                        { data: 'firstname' },
                        { data: 'lastname' },
                        { data: 'company' },
                        { data: 'address' },
                        { data: 'city' },
                        { data: 'state' },
                        { data: 'zip' },
                        { data: 'course' },
                        { data: 'registered' },
                        { data: 'completed' },
                    ],
                    buttons: [
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                } );
            }
            else if( response.success == true && data == 'video' ){
                $('.lms_analityc_report_video').css('display','block');
                $('#lms_analityc_report_video').DataTable( {
                    dom: 'Bfrtip',
                    retrieve: true,
                    "searching": false,
                    data: response.data,
                    columns: [
                        { data: 'first_name' },
                        { data: 'last_name' },
                        { data: 'company' },
                        { data: 'address' },
                        { data: 'city' },
                        { data: 'state' },
                        { data: 'postcode' },
                        { data: 'course_name' },
                        { data: 'first_viewed' },
                        { data: 'last_viewed' },
                        { data: 'times_viewed' },
                        { data: 'total_view_time' },
                    ],
                    buttons: [
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                } );
            }
            $(".spinner").hide();
            console.log(response.data);
        }
    })

}