<?php
require_once ABSPATH . '/wp-content/plugins/learndash_woocommerce/learndash_woocommerce.php';

// function to create the DB / Options / Defaults
function lms_helper_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'courses_analytic';

    // create the ECPT metabox database table
    if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name)
    {
        $sql = "CREATE TABLE " . $table_name . " (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`firstname` varchar(25) NOT NULL,
		`lastname` varchar(50) NOT NULL,
		`company` varchar(100) NOT NULL,
		`address` varchar(100) NOT NULL,
		`city` varchar(50) NOT NULL,
		`state` varchar(50) NOT NULL,
		`zip` varchar(25) NOT NULL,
		`course` varchar(25) NOT NULL,
		`registered` varchar(50) NOT NULL,
		`completed` varchar(50) NOT NULL,
		UNIQUE KEY id (id)
		);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        $sql = "CREATE INDEX lms_courses ON {$table_name} (course, firstname)" ;

        $wpdb->query ($sql) ;
    }

}
/**
 * @desc Debug function
 * @param $data
 * @date 28.02.2018 (10:43)
 */
function lms_show($data){
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function process_post() {
    //lms_show(delete_post_meta(8174,'_mailster_lists'));
    //lms_show(get_post_meta(8105,'_mailster_lists'));
}

/**
 * @desc Include plugin css and js files to wp-login page
 * @date 01.03.2018 (11:51)
 */
function lms_helper_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . plugins_url( '/admin/assets/css/lms-helper-login.css?v=7', __FILE__ ) . '" />';
}

/**
 * @desc Include plugin css and js files
 * @date 01.03.2018 (10:54)
 */
function lms_helper_scripts_styles() {
    wp_enqueue_style( 'lms-helper-css', plugins_url(PLUGIN_NAME.'/assets/css/lms-helper.css') );
    wp_enqueue_script( 'lms-helper-js', plugins_url(PLUGIN_NAME.'/assets/js/lms-helper.js'), array( 'jquery' ) );
}

/**
 * @desc Add Forgot Password option to user login page
 * @date 01.03.2018 (11:20)
 */
function lms_helper_forgot_pass(){ ?>
    <a class='lms-f-p' href="<?= wp_lostpassword_url(); ?>" title="Lost Password">Lost Password</a>
<?php }

function webex_logic($order_id){
    global $wpdb;
    $user_id = get_current_user_id();

    $order = wc_get_order( $order_id );
    $items = $order->get_items();
    $order_data = array(
        'billing_address' => array(
            'first_name' => $order->get_billing_first_name(),
            'last_name' => $order->get_billing_last_name(),
            'company' => $order->get_billing_company(),
            'address_1' => $order->get_billing_address_1(),
            'address_2' => $order->get_billing_address_2(),
            'city' => $order->get_billing_city(),
            'state' => $order->get_billing_state(),
            'formated_state' => WC()->countries->states[$order->get_billing_country()][$order->get_billing_state()], //human readable formated state name
            'postcode' => $order->get_billing_postcode(),
            'country' => $order->get_billing_country(),
            'formated_country' => WC()->countries->countries[$order->get_billing_country()], //human readable formated country name
            'email' => $order->get_billing_email(),
            'phone' => $order->get_billing_phone()
        ),
        'shipping_address' => array(
            'first_name' => $order->get_shipping_first_name(),
            'last_name' => $order->get_shipping_last_name(),
            'company' => $order->get_shipping_company(),
            'address_1' => $order->get_shipping_address_1(),
            'address_2' => $order->get_shipping_address_2(),
            'city' => $order->get_shipping_city(),
            'state' => $order->get_shipping_state(),
            'formated_state' => WC()->countries->states[$order->get_shipping_country()][$order->get_shipping_state()], //human readable formated state name
            'postcode' => $order->get_shipping_postcode(),
            'country' => $order->get_shipping_country(),
            'formated_country' => WC()->countries->countries[$order->get_shipping_country()] //human readable formated country name
        )
    );
    $address = array_merge($order_data['billing_address'],$order_data['shipping_address']);
    foreach ( $items as $item ) {
        $product_id = $item->get_product_id();
    }
    $currentEventId = get_post_meta($product_id,'_related_course')[0];
    $post_author_id = get_post_field( 'post_author', $currentEventId );
    $_event_type = get_post_meta($currentEventId,'_event_type')[0];

    if ( $_event_type == 'webex' ){
        $userData = array();
        $lastname = (!empty($address['last_name'])) ? $address['last_name'] : $address['first_name'];
        $userData['emails'] = $address['email'];
        $sessionKey = get_post_meta($currentEventId,'webex_sessionKey',true);
        registerMeetingAttendee( $address,$sessionKey,$post_author_id );
    }
    $subscriber_id = mailster( 'subscribers' )->add( array(
        'firstname' => $address['first_name'],
        'lastname' => $address['last_name'],
        'email' => $address['email'],
        'status' => 0, //1 = subscribed (default) , 0 = pending, 2 = unsubscribed, 3 = hardbounced
    ), $overwrite = true );

    if ( !is_wp_error($subscriber_id) ){
        $list[] = mailster( 'lists' )->get( get_post_meta($currentEventId,'event_mail_list')[0] )->ID;
        $list[] = 1;
        $success = mailster( 'subscribers' )->assign_lists( $subscriber_id, $list, $remove_old = false);
    }

    $_start_date = get_post_meta($currentEventId, '_start_date',true);
    $_start_time = get_post_meta($currentEventId, '_start_time',true);
    $_duration = get_post_meta($currentEventId, '_duration',true);
    $_timezone = get_post_meta($currentEventId, '_timezone',true);
    date_default_timezone_set($_timezone);
    $register_date = date('Y-m-d H:i:s');
    $selectedTime = $_start_time;
    $endTime = strtotime("+{$_duration} minutes", strtotime($selectedTime));
    $completed =  $_start_date .' '. date('h:i:s', $endTime);
    $course_name = get_post($currentEventId)->post_title;

    if( is_array($address) ){
        $table_name = $wpdb->prefix . 'courses_analytic';
        $wpdb->insert(
            $table_name,
            array(
                'firstname' => ($address['first_name']) ? $address['first_name'] : '-',
                'lastname' => ($address['last_name']) ? $address['last_name'] : '-',
                'company' => ($address['company']) ? $address['company'] : '-',
                'address' => ($address['address_1']) ? $address['address_1'] : '-',
                'city' => ($address['city']) ? $address['city'] : '-',
                'state' => ($address['state']) ? $address['state'] : '-',
                'zip' => ($address['postcode']) ? $address['postcode'] : '',
                'course' => ($course_name) ? $course_name : '',
                'registered' => ($register_date) ? $register_date : '-',
                'completed' => ($completed) ? $completed : '-'
            ),
            array( '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s', )
        );

    }

    return;
}

/**
 * @globals $wp_query
 * @desc Check Event price (free or paid), then if price is 0 create a order for current event.
 * @date 28.02.2018 (10:40)
 */
function lms_helper_create_order(){
    global $wp_query;
    $user_id = get_current_user_id();
    if ( !isset($wp_query->post->ID) || empty($wp_query->post->ID) ) return false;

    if ( isset(get_post_meta($wp_query->post->ID,'_event_product_id')[0]) && !empty(get_post_meta($wp_query->post->ID,'_event_product_id')[0]) ){
        $currentEventLandPostId = '';
        $currentEventProdId = '';
        $currentEventId = '';

        $currentEventLandPostId = $wp_query->post->ID;
        $currentEventProdId = get_post_meta($currentEventLandPostId,'_event_product_id')[0];
        $currentEventId = get_post_meta($currentEventProdId,'_related_course')[0];
        $_event_type = get_post_meta($currentEventId,'_event_type')[0];


        if( !isset(get_post_meta($currentEventProdId)['_regular_price'][0]) || get_post_meta($currentEventProdId)['_regular_price'][0] != '0.00' || get_post_meta($currentEventProdId)['_regular_price'][0] != 0.00 || get_post_meta($currentEventProdId)['_regular_price'][0] != 0)return false;

        $address = array(
            'first_name' => ($_POST['input_4_3']) ? $_POST['input_4_3'] : 'Event User Name',
            'last_name'  => ($_POST['input_4_6']) ? $_POST['input_4_6'] : '',
            'company'    => ($_POST['input_7']) ? $_POST['input_7'] : 'Event User Company',
            'email'      => ($_POST['input_6']) ? $_POST['input_6'] : 'event.user@event.com',
            'phone'      => ($_POST['input_5']) ? $_POST['input_5'] : '(000) 000-0000',
            'address_1'  => 'Event Address',
            'address_2'  => '',
            'city'       => 'Event City',
            'state'      => '',
            'postcode'   => 'event0000',
            'country'    => 'EV'
        );

        $order = wc_create_order();

        $order->add_product( get_product( $currentEventProdId ), 1 );


        $order->set_address( $address, 'billing' );
        $order->set_address( $address, 'shipping' );

        $order->calculate_totals();

        update_post_meta( $order->id, '_payment_method', 'cod' );
        update_post_meta( $order->id, '_payment_method_title', 'Cash On Delivery' );

        // Store Order ID in session so it can be re-used after payment failure
        WC()->session->order_awaiting_payment = $order->id;

        // Process Payment
        $available_gateways = WC()->payment_gateways->get_available_payment_gateways();

        $result = $available_gateways[ 'cod' ]->process_payment( $order->id );

        // Redirect to success/confirmation/payment page
        if ( $result['result'] == 'success' ) {

            $result = apply_filters( 'woocommerce_payment_successful_result', $result, $order->id );

            //add_action( 'woocommerce_email', 'unhook_those_pesky_emails' );

            wp_redirect( $result['redirect'] );
            exit;
        }

    }

}

/**
 * @desc Connect user to webex team and room
 * @param $userData
 * @param $sessionKey
 * @return void
 */
function registerMeetingAttendee($userData,$sessionKey,$teacher_id){
    $return = '';
    $access = require_once('webex_config.php');
    $cred = get_user_meta($teacher_id,'webex_cred',true);

    $username = @$userData['first_name'].' '.@$userData['last_name'];

    $xmlData = '<?xml version="1.0" encoding="UTF-8"?>
                    <serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <header>
                            <securityContext>
                                <webExID>'.$cred["id"].'</webExID>
                                <password>'.$cred["pass"].'</password>
                                <siteID>'.$cred["siteID"].'</siteID>
                                <partnerID>'.$cred["partnerID"].'</partnerID>
                            </securityContext>
                        </header>
                        <body>
                            <bodyContent
                                xsi:type="java:com.webex.service.binding.attendee.RegisterMeetingAttendee">
                                <attendees>
                                    <person>
                                        <name>'.$username.'</name>
                                        <company>'.$userData['company'].'</company>
                                        <address>
                                            <addressType>GLOBAL</addressType>
                                            <city>'.$userData['city'].'</city>
                                            <country>'.$userData['country'].'</country>
                                        </address>
                                        <email>'.$userData['email'].'</email>
                                        <type>VISITOR</type>
                                    </person>
                                    <joinStatus>ACCEPT</joinStatus>
                                    <role>ATTENDEE</role>
                                    <emailInvitations>true</emailInvitations>
                                    <sessionKey>'.$sessionKey.'</sessionKey>
                                </attendees>
                            </bodyContent>
                        </body>
                    </serv:message>';
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $access["url"] );
    curl_setopt( $ch, CURLOPT_POST, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $xmlData );
    $result = curl_exec($ch);

    $cleanResult = str_replace(':','',$result);
    $xml = simplexml_load_string($cleanResult);
    $json = json_encode($xml); // convert the XML string to JSON
    $response = json_decode($json,TRUE); // convert the JSON-encoded string to a PHP variable

    curl_close($ch);

    return $return;
}

/**
 * @desc Disable all emails for free events
 * @param $email_class
 * @date 06.03.2018 (14:58)
 */
function unhook_those_pesky_emails($email_class ) {

    /**
     * Hooks for sending emails during store events
     **/
    remove_action( 'woocommerce_low_stock_notification', array( $email_class, 'low_stock' ) );
    remove_action( 'woocommerce_no_stock_notification', array( $email_class, 'no_stock' ) );
    remove_action( 'woocommerce_product_on_backorder_notification', array( $email_class, 'backorder' ) );

    // New order emails
    remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
    remove_action( 'woocommerce_order_status_pending_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
    remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
    remove_action( 'woocommerce_order_status_failed_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
    remove_action( 'woocommerce_order_status_failed_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
    remove_action( 'woocommerce_order_status_failed_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );

    // Processing order emails
    remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
    remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );

    // Completed order emails
    remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );

    // Note emails
    remove_action( 'woocommerce_new_customer_note_notification', array( $email_class->emails['WC_Email_Customer_Note'], 'trigger' ) );
}

/**
 * @globals $woocommerce
 * @desc Connect user to membership plan that the user buy
 * @param $order_id
 * @return bool
 * @date 09.03.2018 (11:42)
 */
function lms_helper_woocommerce_order_status_completed($order_id ) {

    global $woocommerce;
    $current_user = wp_get_current_user();
    $order = new WC_Order( $order_id );
    $start_date = $order->order_date;
    $user = get_user_by('id', $current_user->ID);
    $items = $woocommerce->cart->get_cart();

    if( empty($items) )return false;

    foreach( $items as $cart_item ){
        $product_id = $cart_item['product_id'];

        if ( !isset(get_post_meta($product_id,'_rpwcm_plans')[0]) || empty(get_post_meta($product_id,'_rpwcm_plans')[0]) )return false;

        $planId = get_post_meta($product_id,'_rpwcm_plans')[0];

        $post_type = get_post($planId)->post_type;

        if($post_type != 'membership_plan')return false;
        if( !isset(get_post_meta($planId,'key')[0]) || empty(get_post_meta($planId,'key')[0]) )return false;

        $planKey = get_post_meta($planId,'key')[0];

        $user->add_cap($planKey);
        add_user_meta($current_user->ID,'_rpwcm_' . $planKey . '_since',time());

        add_user_meta($current_user->ID,'user_plan_id',$planId);

        $planEvents = get_post_meta($planId,'plan_events');

        if ( !empty($planEvents) ){
            foreach ($planEvents as $planEvent){

                $functions = new learndash_woocommerce();
                $functions->increment_course_access_counter( $planEvent, $current_user->ID );
                update_user_meta( $current_user->ID, "course_".$planEvent."_access_from", strtotime( $start_date ) );
            }

        }

    }

}

/**
 * @desc add user to default list
 * @date 09.03.2018 (11:42)
 */
function add_user_to_default_list(){

    if (isset($_REQUEST['newsletter']) && $_REQUEST['newsletter'] == "Y" && is_plugin_active('mailster/mailster.php')) {
        $current_user = wp_get_current_user();

        if (isset($current_user->data) && isset($current_user->data->ID) && $current_user->data->ID > 0) {
            $default_list_id = get_default_list_id();

            global $wpdb;
            $isexist = $wpdb->get_results("SELECT * FROM `wp_mailster_lists_subscribers` WHERE list_id = ".$default_list_id." AND subscriber_id = ".$current_user->data->ID);
            if (!isset($isexist[0])) {
                $wpdb->insert( 
                    'wp_mailster_lists_subscribers', 
                    array( 
                        'list_id' => $default_list_id, 
                        'subscriber_id' => $current_user->data->ID, 
                        'added' =>  time()
                    ),  
                    array( 
                        '%d', 
                        '%d',
                        '%d' 
                    ) 
                );


                //autosubscribe
                $wpdb->query("UPDATE `wp_mailster_subscribers` SET `status` = '1' WHERE `wp_mailster_subscribers`.`ID` = ".$current_user->data->ID);

            }
            
                   
        }
 
    }
 
}

/**
 * @desc Check if user role is administrator, then redirect to dashboard
 * @date 15.03.2018 (11:55)
 */
function lms_helper_admin_redirect() {

    if( is_user_logged_in() ) {
        $currentUserData = wp_get_current_user();
        $currentUserRole = $currentUserData->roles[0];
        if ( $currentUserRole == 'administrator' ){

            wp_redirect( get_option('home').'/wp-admin' );
            exit();

        }
    }

}

function get_custom_post_type_template() {

    $functions = new learndash_woocommerce();
    $functions->increment_course_access_counter( 832, get_current_user_id() );
    update_user_meta( get_current_user_id(), "course_832_access_from", 1522245183 );

    //accessed events
   /* $user_id = get_current_user_id();

    if ( get_post_type() != 'page' || !$user_id) return false;

    get_post_type();

    lms_show($user_id);*/

}

/**
 * @desc Change plugins load order
 */
function my_plugin_load_first()
{
    $path = 'wpmaildrill/wpmaildrill.php';

    if ( $plugins = get_option( 'active_plugins' ) ) {
        if ( $key = array_search( $path, $plugins ) ) {
            array_splice( $plugins, $key, 1 );
            array_unshift( $plugins, $path );
            update_option( 'active_plugins', $plugins );
        }
    }
}

/**
 * @desc Create button for redirect
 * @param $order_get_id
 */
function lms_helper_memebership_redirect_button( $order_get_id ) {

    $order = wc_get_order( $order_get_id );
    foreach ($order->get_items() as $item_key => $item_values):
        $product_id = $item_values->get_product_id();
        break;
    endforeach;
    $pageUrl = get_post_meta($product_id,'lms_helper_memb_redirect')[0];
    if ( !empty($pageUrl) ){
    ?>
        <a href="<?= $pageUrl; ?>" class="button">Click here to continue</a>
    <?php
    }
};




