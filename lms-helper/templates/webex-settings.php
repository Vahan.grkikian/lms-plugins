<h2>Webex API Credentials</h2>
<div class="webex_credentials">
    <form id="webex_options_form" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" name="form">
        <p class="font-blue-madison">Enter Account Information below</p>

        <div class="form-group required">
            <label class="control-label" for="teacher_email">Enter teacher email</label>
            <input type="email" class="form-control" id="teacher_email" name="teacher_email" required>
        </div>

        <div class="form-group required">
            <label class="control-label" for="webexId">Enter teacher webexid</label>
            <input type="text" class="form-control" id="webexId" name="webexId" required>
        </div>

        <div class="form-group required">
            <label class="control-label" for="webex_pass">Enter teacher webex password</label>
            <input type="password" class="form-control" id="webex_pass" name="webex_pass" required>
        </div>

        <div class="form-group required">
            <label class="control-label" for="siteID">Enter teacher webex siteID</label>
            <input type="text" class="form-control" id="siteID"  name="siteID" required>
        </div>

        <div class="form-group required">
            <label class="control-label" for="PartnerID">Enter teacher webex PartnerID</label>
            <input type="text" class="form-control" id="PartnerID" name="PartnerID" required>
        </div>

        <input name="save" type="submit" value="Save" class="button edit_design btn dark btn-md sbold uppercase green-steel">
        <input type="hidden" name="action" value="save_webex_credentials">

    </form>
</div>
