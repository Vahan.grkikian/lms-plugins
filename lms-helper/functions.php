<?php
/**
 * @desc Debug function
 * @param $data
 * @date 28.02.2018 (10:43)
 */
function lms_show($data){
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

/**
 * @desc Include plugin css and js files to wp-login page
 * @date 01.03.2018 (11:51)
 */
function lms_helper_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . plugins_url( '/admin/assets/css/lms-helper-login.css?v=7', __FILE__ ) . '" />';
}

/**
 * @desc Include plugin css and js files
 * @date 01.03.2018 (10:54)
 */
function lms_helper_scripts_styles() {
    wp_enqueue_style( 'lms-helper-css', plugins_url(PLUGIN_NAME.'/assets/css/lms-helper.css') );
    wp_enqueue_script( 'lms-helper-js', plugins_url(PLUGIN_NAME.'/assets/js/lms-helper.js'), array( 'jquery' ) );
}

/**
 * @desc Add Forgot Password option to user login page
 * @date 01.03.2018 (11:20)
 */
function lms_helper_forgot_pass(){ ?>
    <a class='lms-f-p' href="<?= wp_lostpassword_url(); ?>" title="Lost Password">Lost Password</a>
<?php }

/**
 * @desc Check Event price (free or paid), then if price is 0 create a order for current event.
 * @date 28.02.2018 (10:40)
 */
function lms_helper_create_order(){
    global $wp_query;
    if ( isset($wp_query->query['post_type']) && $wp_query->query['post_type'] == "sfwd-courses" ){
        $currentEventId = '';
        $currentEventProdId = '';

        if ( !isset($wp_query->post->ID) || empty($wp_query->post->ID) ) return false;
        $currentEventId = $wp_query->post->ID;

        if ( !isset( get_post_meta($currentEventId)['_event_product_id'][0] ) || empty( get_post_meta($currentEventId)['_event_product_id'][0] ) )return false;
        $currentEventProdId = get_post_meta($currentEventId)['_event_product_id'][0];

        if( !isset(get_post_meta($currentEventProdId)['_regular_price'][0]) || get_post_meta($currentEventProdId)['_regular_price'][0] != '0.00' )return false;

        $address = array(
            'first_name' => ($_POST['input_4_3']) ? $_POST['input_4_3'] : 'Event User Name',
            'last_name'  => '',
            'company'    => ($_POST['input_7']) ? $_POST['input_7'] : 'Event User Company',
            'email'      => ($_POST['input_6']) ? $_POST['input_6'] : 'event.user@event.com',
            'phone'      => ($_POST['input_5']) ? $_POST['input_5'] : '(000) 000-0000',
            'address_1'  => 'Event Address',
            'address_2'  => '',
            'city'       => 'Event City',
            'state'      => '',
            'postcode'   => 'event0000',
            'country'    => 'EV'
        );

        $order = wc_create_order();

        $order->add_product( get_product( $currentEventProdId ), 1 );


        $order->set_address( $address, 'billing' );
        $order->set_address( $address, 'shipping' );

        $order->calculate_totals();

        update_post_meta( $order->id, '_payment_method', 'cod' );
        update_post_meta( $order->id, '_payment_method_title', 'Cash On Delivery' );

        // Store Order ID in session so it can be re-used after payment failure
        WC()->session->order_awaiting_payment = $order->id;

        // Process Payment
        $available_gateways = WC()->payment_gateways->get_available_payment_gateways();

        $result = $available_gateways[ 'cod' ]->process_payment( $order->id );

        // Redirect to success/confirmation/payment page
        if ( $result['result'] == 'success' ) {

            $result = apply_filters( 'woocommerce_payment_successful_result', $result, $order->id );

            wp_redirect( $result['redirect'] );
            exit;
        }

    }

}

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string The filtered title.
 */

/*function my_tpl_wp_title($title) {
    $title = 'My new cool title';
    die($title);
    return $title;
}*/
