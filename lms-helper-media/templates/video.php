<?php
$user_id = get_current_user_id();
$videoFolders = get_user_meta($user_id,'secure_folders');
$videoFolders = array_reverse($videoFolders);
?>
<div class="upload_video_section">
    <div class="lms_folders_part">
        <div class="aio-fixed-header">
            <span class="lms_title">Folders</span>
            <button type="button" id="cr_folder" class="btn btn-info btn-lg" data-toggle="modal" data-target="#folderCreate">
                <span class="tooltiptext">Create folder</span>
            </button>
        </div>
        <form action="" method="post" enctype="multipart/form-data" id="lms_media_video_upload">
            <h5>Select folder and upload a video</h5>
            <ul class="folders_list">
                <?php foreach ($videoFolders as $videoFolder): ?>
                    <li>
                        <span class="folders_icon"></span>
                        <input type="radio" class="folder_name" name="folder_name" value="<?= $videoFolder; ?>" style="display: none;">
                        <label><?= $videoFolder; ?></label>
                    </li>
                <?php endforeach; ?>
            </ul>

            <!--Upload Video modal-->
            <div class="modal fade" id="videoUpload" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content videoUpload">
                        <div class="modal-header">
                            <h4 class="modal-title">Upload video</h4>
                        </div>
                        <input type="file" name="video_data" accept="video/mp4,video/x-m4v,video/*">

                        <button type="submit">Upload Video</button>
                        <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </form>
    </div>

    <div class="lms_shortcodes_part">
        <div class="aio-fixed-header">
            <span class="lms_title">Videos</span>
            <div class="hid_cont">
                <button type="button" id="up_video" data-toggle="modal" data-target="#videoUpload">
                    <span class="tooltiptext">Upload video</span>
                </button>
                <button class="copy_shtrct" onclick="copyClipboard()">
                    <span class="tooltiptext">Copy shortcode</span>
                </button>
                <button class="preview_shrtct">
                    <span class="tooltiptext">Preview video</span>
                </button>
                <button class="analytic_shrtct">
                    <span class="tooltiptext">Video analytic</span>
                </button>
            </div>

        </div>
        <span class="" id="success_message"></span>
        <div class="user_shortcodes" id="lms_media_user_shortcodes"></div>
    </div>

    <!--Create folder modal-->
    <div class="modal fade" id="folderCreate" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Folder</h4>
                </div>
                <form action="" method="post" id="lms_media_folder_cr_form">
                    <input type="text" maxlength="20" name="lms_media_folder_name" placeholder="example: secure-folder">
                    <button class="add-new-h2" type="submit" name="secure" value="true">Create</button>
                    <button type="button" class="btn lms_close" data-dismiss="modal">Close</button>
                </form>
            </div>
        </div>
    </div>
    <div class="spinner" style="display: none;"></div>

    <!--Video analytic Modal -->
    <div class="modal fade" id="analytic_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Video Analytic</h4>
                </div>
                <a href="#" class="export">Export CSV</a>
                <div class="modal-body" id="lms_media_video_analytic_modal"></div>
                <div class="modal-footer">
                    <button type="button" class="btn lms_close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!--Video preview Modal -->
    <div class="modal fade" id="preview_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Video Analytic</h4>
                </div>
                <div class="modal-body" id="lms_media_video_preview_modal"></div>
                <div class="modal-footer">
                    <button type="button" class="btn lms_close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



</div>