/*
* Analytics
* */
function getMsg(eid, pid, sid, uid, vid, pos, dur) {
    var d = new Date();
    var s = 'date=' + d.toLocaleDateString();
    s += '\ntime=' + d.toLocaleTimeString();
    s += '\npid=' + pid;
    s += '\nsid=' + sid;
    s += '\nuid=' + uid;
    s += '\nvid=' + vid;
    s += '\neid=' + eid;
    s += '\npos=' + pos;
    s += '\ndur=' + dur;

    var data = {
        'action': 'lms_media_save_analytic',
        'type': 'jwplayer',
        'date': d.toLocaleDateString(),
        'time': d.toLocaleTimeString(),
        'pid': pid,
        'sid': sid,
        'uid': uid,
        'vid': vid,
        'eid': eid,
        'pos': pos,
        'dur': dur
    }

    $.ajax({
        url: lms_media_front_script.ajax_url,
        dataType: 'json',
        type: 'post',
        data: data,
        success: function( data, textStatus, jQxhr ){

        },
        error: function( jqXhr, textStatus, errorThrown ){
            console.log( errorThrown );
        }
    });
        console.log( " data8888", data);
    return s;
}

function onPlay(pid, sid, uid, vid, pos, dur) {
    getMsg('start', pid, sid, uid, vid, pos, dur);
};

function onPause(pid, sid, uid, vid, pos, dur) {
    getMsg('pause', pid, sid, uid, vid, pos, dur);
};

function onComplete(pid, sid, uid, vid, pos, dur) {
    getMsg('complete', pid, sid, uid, vid, pos, dur);
};