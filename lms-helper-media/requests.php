<?php

/**
 * @desc Beauty print
 * @param $data
 */
function print_by($data ){
    echo "<pre>";
    print_r( $data );
    echo "</pre>";
}

/**
 * @desc Include admin assets
 * @return void
 */
function lms_media_load_admin_style() {

    wp_enqueue_style( 'lms_media_css', plugin_dir_url( __FILE__ ) . 'assets/lms_media.css' );
    wp_enqueue_script( 'lms_media_script', plugins_url( 'assets/lms_media_script.js', __FILE__ ), array('jquery'), '1.0', true );
    wp_localize_script( 'lms_media_script', 'lms_media_script', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}


/**
 * @desc Include front assets
 * @return void
 */
function lms_media_load_front_assets(){
    wp_enqueue_script( 'lms_media_front_script', plugins_url( 'front/lms_front.js', __FILE__ ), array('jquery'), '1.0', true );
    wp_localize_script( 'lms_media_front_script', 'lms_media_front_script', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}


/**
 * @desc Add sub menu to the file menu item
 * @return void
 */
function lms_helper_media_admin_menu(){
    add_menu_page( 'Media', 'Media', 'manage_options', 'upload_video','lms_helper_media_upload_video_page','dashicons-video-alt3');
}


/**
 * @desc Create video upload page (UI)
 * @return "html"
 */
function lms_helper_media_upload_video_page(){
    require_once( __DIR__.'/templates/video.php' );
}

/**
 * @desc Create folder via API, in streampublisher
 * @return array|string
 */
function lms_helper_media_create_folder(){
    // POST file data
    $username = USERNAME;
    $password = PASSWORD;
    $user_id = get_current_user_id();
    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/CreateFolder';

    $foldername = esc_sql($_REQUEST['foldername']);
    $secure = esc_sql($_REQUEST['secure']);

    $parameters = array(
        'foldername' => $foldername,
        'secure' => $secure
    );
    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;
    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($curl);

    $response = json_decode($result);
    $success = '';
    if ( !empty($response->Value) && $response->Status != 'False' ){

        if ( $secure == 'true' ){
            add_user_meta($user_id,'secure_folders',$foldername);
        }
        else{
            add_user_meta($user_id,'simple_folders',$foldername);
        }
        $success = array(
            'status' => 'ok',
            'message' => 'The folder is successfully created.'
        );

    }
    else {
        $success = array(
            'status' => 'error',
            'message' => 'Something gone wrong, please try again.'
        );
    }
    curl_close($curl);
    wp_send_json_success( $success );
    wp_die();
}

/**
 * @desc Upload Wp Image to another server via ftp
 * @return mixed
 */
function lms_helper_media_upload_by_ftp(){

    $ftp_server = FTP_HOST;
    $ftp_user_name = FTP_USERNAME;
    $ftp_user_pass = FTP_PASS;

    $html = '';
    $user_id = get_current_user_id();
    
    $fileName = $_FILES['img']['name'];
    $dir = esc_sql($_POST['foldername']);
    $file_path = esc_sql($_FILES['img']['tmp_name']);


    $destination_file = '/'.$dir.'/'.$fileName;
    $source_file = $file_path;


    // set up basic connection
    $conn_id = ftp_connect($ftp_server);
    ftp_pasv($conn_id, true);


    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);


    //create the directory
    if (!@ftp_chdir($conn_id, $dir)){
        ftp_mkdir($conn_id, $dir);
    }


    // check connection
    if ( !$conn_id || !$login_result ){
        $html = array(
            'status' => 'error',
            'message' => 'Lost connection, please try again.'
        );
    }

    // upload the file
    $upload = ftp_put($conn_id, $destination_file, $source_file, FTP_BINARY);



    // check upload status
    if (!$upload || empty($upload)) {
        $html = array(
            'status' => 'error',
            'message' => 'Upload failed, please try again.'
        );

    }
    else {

        add_user_meta($user_id,'video_names_'.$dir,$fileName);
        $html = array(
                'status' => 'ok',
                'filename' => $fileName,
                'dir' => $dir,
                'message' => 'The video is successfully uploaded.'
        );
    }
    // close the FTP stream
    ftp_close($conn_id);

    wp_send_json_success( $html );
    wp_die();
}

/**
 * @param $sourcekeyfile
 * @return string
 */
function lms_helper_media_get_file_url($sourcekeyfile){
    $username = USERNAME;
    $password = PASSWORD;

    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/GetFileUrl';


    $parameters = array(
        'sourcekeyfile' => $sourcekeyfile,
        "linktype" => "standard",
        "ssl" => "false"
    );

    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;

    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($curl);
    curl_close($curl);

    $imageUrl = '';
    //print_by($result);
    if ( !empty(json_decode($result)->Value) ){
        $imageUrl = json_decode($result)->Value;
    }

    return $imageUrl;
}

/**
 * @desc Get cuurent user shortcodes and show in admin page
 * @return void "html"
 */
function get_current_user_shortcodes(){
    $html = '';
    $user_id = get_current_user_id();
    $foldername = esc_sql($_REQUEST['foldername']);
    $videoNames = get_user_meta($user_id,'video_names_'.$foldername);

    if ( empty($videoNames) ){
        $html .= '<h3> No items in this folder </h3>';
        wp_send_json_success( $html );
        wp_die();
    }
    $html .= "<script src=\"//content.jwplatform.com/libraries/IKaW14Sj.js\"></script>";
    $html .= "<ul>";
        foreach ($videoNames as $videoName){
            $shoursekey = $foldername."/".$videoName;
            $inputValue = "[video  name='".$shoursekey."']";
            $imageUrl = getFileThumbnailUrl($shoursekey);
            $html .= '<li>';
            $html .= '<input type="hidden" class="video_data" value="/'.$foldername.'/'.$videoName.'"/>';
                $html .= '<span class="lms_media_video_thumb"><img src="'.$imageUrl.'"></span>';
                $html .= '<span class="btn_shrt_container">';
                    $html .= '<span class="lms_media_video_folder_name">'.$videoName.'</span>';
                    $html .= '<span class="copy"><input value="'.$inputValue.'" class="lms_media_video_shortcode" readonly></span>';
                $html .= '</span>';
                //$html .= '<span data="'.$shoursekey.'" class="video_del_icon"></span>';
                $html .= '</li>';
        }
    $html .= '</ul>';
    wp_send_json_success( $html );
    wp_die();
}

/**
 * @param $atts
 * @return string
 */
function lms_media_video($atts ) {
    $html = '';
    $sourcekeyfile = $atts['name'];


    $playerIncludes = GetPlayerIncludes($sourcekeyfile);
    if ( $playerIncludes == 'error' ) return $html = 'Error';
    $html .= $playerIncludes;

    $callScript = GetFilePreviewScript($sourcekeyfile);
    if ( $callScript == 'error' ) return $html = 'Error';
    $html .= $callScript;


    return $html;
}


/**
 * @desc Delete the video from SP and site DB
 */
function lms_media_delete_video(){
    // POST file data
    $username = USERNAME;
    $password = PASSWORD;

    $user_id = get_current_user_id();
    $sourcekeyfile = esc_sql($_POST['filename']);
    $dirname = explode('/',$sourcekeyfile)[0];
    $filename = explode('/',$sourcekeyfile)[1];

    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/DeleteFile';


    $parameters = array(
        'sourcekeyfile' => $sourcekeyfile,
    );
    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;
    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($curl);
    print_by($result);
    $response = json_decode($result);
    $html = '';
    if ( $response->Status == 'True' ) {
        $html = array(
            'status' => $response->Status,
            'dir' => $dirname,
            'message' => 'The video is successfully deleted'
        );
        delete_user_meta($user_id, 'video_names_'.$dirname,$filename);
    }
    else {
        $html = array(
            'status' => 'error',
            'message' => 'Something gone wrong, please try again'
        );
    }

    wp_send_json_success( $html );
    wp_die();
}
/**
 * @param $sourcekeyfile
 * @param string $linktype
 * @return string
 */
function GetPlayerIncludes($sourcekeyfile, $linktype = 'standart'){

    // POST file data
    $username = USERNAME;
    $password = PASSWORD;

    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/GetPlayerIncludes';

    $parameters = array(
        'playertype' => 'jw',
        'sourcekeyfile' => $sourcekeyfile,
        'ssl' => 'true',
        'linktype' => $linktype
    );
    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;
    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($curl);

    $response = json_decode($result);

    if ( $response->Status == 'True' ) {
        return $response->Value;
    }
    else {
        return 'error';
    }

}

/**
 * @param $sourcekeyfile
 * @return string
 */
function GetFilePreviewScript($sourcekeyfile){

    $user_id = get_current_user_id();
    if(!session_id()) {
        session_start();
    }

    // POST file data
    $username = USERNAME;
    $password = PASSWORD;

    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/GetFilePreviewScript';

    $parameters = array(
        'sourcekeyfile' => $sourcekeyfile,
        "linktype" => "hls",
        "ssl" => "true",
        "playertype" => "jw",
        "playerscrub" => "false",
        "playerautoplay" => "false",
        "playersize" => "640x360",
        "sessionid" => session_id(),
        "userid" => $user_id

    );
    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;
    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($curl);

    $response = json_decode($result);

    if ( $response->Status == 'True' ) {
        return $response->Value;
    }
    else {
        return 'error';
    }
}

/**
 *
 */
function lms_media_save_analytic(){
    global $wpdb;

    $dbName         =   $wpdb->prefix . 'player_analytic';
    $date           =   esc_sql($_POST['date']);
    $time           =   esc_sql($_POST['time']);
    $sessionid      =   esc_sql($_POST['sid']);
    $userid         =   esc_sql($_POST['uid']);
    $videoid        =   esc_sql($_POST['vid']);
    $eventid        =   esc_sql($_POST['eid']);
    $position       =   esc_sql($_POST['pos']);
    $duration       =   esc_sql($_POST['dur']);
    $course_name    = '';
    $total_view_time = '';

    if ($eventid != 'start'){

        $sql = "SELECT `position` FROM {$dbName} WHERE `videoid` = {$videoid} AND `userid` = {$userid} AND `eventid` = 'start' ORDER BY id LIMIT 1";
        $prevStartPosition = $wpdb->get_results($sql,ARRAY_A)[0];
        $total_view_time = $position - $prevStartPosition->position;
    }

    if ( !empty($videoid) ){
        $course_id = $wpdb->get_results( "select post_id from $wpdb->postmeta where meta_value = '{$videoid}'", ARRAY_A );
        $course_name = get_the_title( $course_id );
    }

    $user_data =  lms_helper_get_user_advanced_data($userid);

    $wpdb->insert($dbName, array(
        'date' => $date,
        'time' => $time,
        'sessionid' => $sessionid,
        'userid' => $userid,
        'videoid' => $videoid,
        'eventid' => $eventid,
        'position' => $position,
        'duration' => $duration,
        'total_view_time' => $total_view_time,
        'user_data' => $user_data,
        'course_name' => $course_name
    ));


}

function lms_helper_get_user_advanced_data($user_id){
    $order_data = array(
        'first_name' => get_user_meta( $user_id, 'billing_first_name', true ),
        'last_name' => get_user_meta( $user_id, 'billing_last_name', true ),
        'company' => get_user_meta( $user_id, 'billing_company', true ),
        'address_1' => get_user_meta( $user_id, 'billing_address_1', true ),
        'city' => get_user_meta( $user_id, 'billing_city', true ),
        'state' => get_user_meta( $user_id, 'billing_state', true ),
        'postcode' => get_user_meta( $user_id, 'billing_postcode', true ),
        'country' => get_user_meta( $user_id, 'billing_country', true ),
        'email' => get_user_meta( $user_id, 'billing_email', true ),
    );

    return $order_data;
}

/**
 *
 */
function lms_media_get_analytic(){
    $filename = esc_sql( $_POST['filename'] );

    $html = '';
    global $wpdb;
    $table_name = $wpdb->prefix . 'player_analytic';

    $results = $wpdb->get_results(
        $wpdb->prepare("SELECT * FROM `{$table_name}` WHERE `videoid` = %s", $filename)
    );

    if ( !empty($results) ){

        $html .= '<table id="analytics">';

            $html .= '<tr>';
                $html .= '<th>Id</th>';
                $html .= '<th>Date</th>';
                $html .= '<th>Time</th>';
                $html .= '<th>Session Id</th>';
                $html .= '<th>User Id</th>';
                $html .= '<th>Video Id</th>';
                $html .= '<th>Event Id</th>';
                $html .= '<th>Position</th>';
                $html .= '<th>Duration</th>';
            $html .= '</tr>';
            foreach ($results as $item) {
                $html .= '<tr>';
                $html .= '<td>'.$item->id.'</td>';
                $html .= '<td>'.$item->date.'</td>';
                $html .= '<td>'.$item->time.'</td>';
                $html .= '<td>'.$item->sessionid.'</td>';
                $html .= '<td>'.$item->userid.'</td>';
                $html .= '<td>'.$item->videoid.'</td>';
                $html .= '<td>'.$item->eventid.'</td>';
                $html .= '<td>'.$item->position.'</td>';
                $html .= '<td>'.$item->duration.'</td>';
                $html .= '</tr>';
            }


        $html .= '</table>';
    }
    else {
        $html = '<h5>No available data</h5>';
    }
    wp_send_json_success( $html );
    wp_die();
}

function get_file_preview_html(){
    $user_id = get_current_user_id();
    $filename = '';
    if ( isset($_POST['filename']) && !empty($_POST['filename']) ){
        $filename = esc_sql($_POST['filename']);
    }

    if(!session_id()) {
        session_start();
    }

    // POST file data
    $username = USERNAME;
    $password = PASSWORD;

    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/GetFilePreviewHtml';

    $parameters = array(
        'sourcekeyfile' => $filename,
        "linktype" => "hls",
        "mobile"=>"false",
        "usetoken"=>"false",
        "ssl" => "true",
        "ttl" => "360",
        "playertype" => "jw",
        "playerscrub" => "false",
        "playerautoplay" => "false",
        "playersize" => "600x480",
        "sessionid" => session_id(),
        "userid" => $user_id

    );
    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;
    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($curl);

    $response = json_decode($result);
    $html = '';
    if ( $response->Status == 'True' ) {
        $html = $response->Value;
    }
    else {
        $html =  'Video not found';
    }
    wp_send_json_success( $html );
    wp_die();
}

function setCallbackUrlSP($username,$password){
    // POST file data

    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/SetFileProperty';

    $parameters = array(
        "sourcekeyfile" => "LmsCompleteCallbackUrl",
        "propertyname" => "LmsCompleteCallbackUrl",
        "propertyvalue" => get_site_url()."/onstream-api/callback/"

    );
    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;
    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

    $result = curl_exec($curl);
    $response = json_decode($result);
    return $response;
}
  
function getSpCallback(){
    $url = get_site_url().'/onstream-api/callback/';
    $path = parse_url($url, PHP_URL_PATH);

    $content = trim(file_get_contents("php://input"));
    //Attempt to decode the incoming RAW post data from JSON.
    $decoded = json_decode($content, true);

    if ( $path == '/onstream-api/callback/' && !empty($decoded) ){



    }


}

function getFileThumbnailUrl($shoursekey){
    // POST file data
    $username = USERNAME;
    $password = PASSWORD;

    $baseurl = 'https://ras.onstreammedia.com/LMSRest/api/File/GetFileProperty';

    $parameters = array(
        'sourcekeyfile' => $shoursekey,
        "propertyname" => "PosterUrl"
    );
    // encode as JSON
    $json = json_encode($parameters);

    $curl = curl_init();

    $base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
    $authorization = "Basic ".$base64EncodedAuthorizationInformation;
    $headers = array();
    $headers[] = "Authorization:".$authorization;
    $headers[] = "Content-Type: application/json";

    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL,  $baseurl);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    $result = curl_exec($curl);

    $response = json_decode($result);

    if ( $response->Status == 'True' ) {
        $imgUrl = lms_helper_media_get_file_url($response->Value);
        return $imgUrl;
    }
    else {
        return '/wp-content/plugins/lms-helper-media/assets/img/noimage.png';
    }
}

function wpse_custom_menu_order( $menu_ord ) {
    if ( !$menu_ord ) return true;

    return array(
        'index.php',
        'events',
        'courses',
        'edit.php?post_type=membership_plan',
        'blog',
        'pages',
        'admin.php?page=gf_edit_forms',
        'upload.php',
        'upload_video',
        'admin.php?page=mailster_dashboard',
        'edit.php?post_type=shop_order',
        'users.php',
        'reports_page',
        'boss_options',
        'integrations',
        'separator-last',
    );
}
