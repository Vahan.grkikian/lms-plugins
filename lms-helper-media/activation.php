<?php

// function to create the DB / Options / Defaults
function lms_helper_media_install() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'player_analytic';

    // create the ECPT metabox database table
    if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name)
    {
        $sql = "CREATE TABLE " . $table_name . " (
		`id` int(11) NOT NULL AUTO_INCREMENT,
		`date` date NOT NULL,
		`time` time NOT NULL,
		`sessionid` varchar(50) NOT NULL,
		`userid` varchar(50) NOT NULL,
		`videoid` varchar(50) NOT NULL,
		`eventid` varchar(50) NOT NULL,
		`position` varchar(25) NOT NULL,
		`duration` varchar(25) NOT NULL,
		`total_view_time` varchar(25) NOT NULL,
		`user_data` varchar(255) NOT NULL,
		`course_name` varchar(255) NOT NULL,
		UNIQUE KEY id (id)
		);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        $sql = "CREATE INDEX lms_media ON {$table_name} (date, time, sessionid, userid, videoid)" ;

        $wpdb->query ($sql) ;
    }

}

function lms_helper_media_uninstall()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'player_analytic';
    $sql = "DROP TABLE IF EXISTS $table_name";
    $wpdb->query($sql);
}

