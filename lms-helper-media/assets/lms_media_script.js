jQuery(document).ready(function () {

    jQuery('#video_analytic_table').DataTable();

    /*
    * get user shortcodes
    * */
    jQuery( document ).on( 'change', 'input[name="folder_name"]', function() {
        var folderName = jQuery(this).val();
        get_user_shortcodes_via_ajax(folderName);
    })

    /*
    * Upload image to publisher via ftp
    * */
    jQuery('#lms_media_video_upload button[type="submit"]').on('click', function (e) {
        e.preventDefault();

        var folderName = jQuery('input[name="folder_name"]:checked','#lms_media_video_upload').val();
        if ( !folderName ){
            alert('Pelease choose the folder!');
            return false;
        }
        jQuery('.modal-content button[data-dismiss="modal"]').trigger('click');
        var $input = $("input[type='file']");
        var fd = new FormData;

        fd.append('img', $input.prop('files')[0]);
        fd.append('action', 'lms_helper_media_upload_by_ftp');
        fd.append('foldername', folderName);


        $(".upload_video_section .spinner").show();

        jQuery.ajax({
            url : lms_media_script.ajax_url,
            type : 'post',
            data : fd,
            processData: false,
            contentType: false,
            success : function( response ) {
                if ( response.data.status == 'ok' ){
                    jQuery("#success_message").addClass('green');
                    get_user_shortcodes_via_ajax(response.data.dir);
                }
                else if( response.data.status == 'error' ){
                    jQuery("#success_message").addClass('red');
                }
                jQuery('#success_message').html(response.data.message);
                $(".upload_video_section .spinner").hide();
                setTimeout(function(){ jQuery('#success_message').html(''); }, 2000);
            }
        });

    });


    /*
    * Show analytic
    * */
    jQuery(document).on('click','.analytic_shrtct, .video_analytic_shrtct span', function () {
        $(".upload_video_section .spinner").show();
        var filename = jQuery(this).val();
        jQuery.ajax({
            url : lms_media_script.ajax_url,
            type : 'post',
            data : {
                action : 'lms_media_get_analytic',
                filename : filename
            },
            success : function( response ) {
                jQuery(".upload_video_section .spinner").hide();
                jQuery("#lms_media_video_analytic_modal").html(response.data);
                jQuery('#analytic_modal  .modal-title').text('Video analytic');
                jQuery('#analytic_modal').css('display','block');
                jQuery('#analytic_modal').addClass('in');
            }
        });
        //console.log(response.data);
    });
    jQuery(document).on('click','.video_analytic_shrtct span', function () {
        $(".fm_spinner").show();
        var filename = jQuery(this).attr('id');
        jQuery.ajax({
            url : lms_media_script.ajax_url,
            type : 'post',
            data : {
                action : 'lms_media_get_analytic',
                filename : filename
            },
            success : function( response ) {
                jQuery("#lms_media_video_analytic_modal").html(response.data);
                jQuery('#analytic_modal  .modal-title').text('Video analytic');
                jQuery('#analytic_modal').css('display','block');
                jQuery('#analytic_modal').addClass('in');
                $(".fm_spinner").hide();
                //console.log(response.data);
            }
        });

    });
    /*
    * Video preview
    * */
    jQuery(document).on('click','.preview_shrtct', function () {
        $(".upload_video_section .spinner").show();
        var filename = jQuery(this).val();
        jQuery.ajax({
            url : lms_media_script.ajax_url,
            type : 'post',
            data : {
                action : 'get_file_preview_html',
                filename : filename
            },
            success : function( response ) {
                jQuery(".upload_video_section .spinner").hide();
                jQuery("#lms_media_video_preview_modal").html(response.data);
                jQuery('#analytic_modal .modal-title').text('Video preview');
                jQuery('#preview_modal').css('display','block');
                jQuery('#preview_modal').addClass('in');
            }
        });
    });

    /*
    * Create Folder
    * */
    jQuery(document).on('click','button[name="secure"]', function (event) {
        event.preventDefault();
        jQuery('input[name="lms_media_folder_name"]').removeClass('required');
        var foldername = jQuery('input[name="lms_media_folder_name"]').val();
        var secure = jQuery(this).val();
        if ( !foldername || foldername.indexOf(" ") >= 0 ){
            jQuery('input[name="lms_media_folder_name"]').addClass('required');
            return false;
        }
        jQuery('.modal-content button[data-dismiss="modal"]').trigger('click');
        $(".upload_video_section .spinner").show();
        jQuery.ajax({
            url : lms_media_script.ajax_url,
            type : 'post',
            data : {
                action : 'lms_helper_media_create_folder',
                foldername : foldername,
                secure : secure
            },
            success : function( response ) {
                jQuery(".upload_video_section .spinner").hide();
                jQuery("#success_message").html(response.data.message);
                if ( response.data.status == 'ok' ){
                    jQuery("#success_message").addClass('green');
                    setTimeout(function(){ location.reload(); }, 2000);
                }
                else if( response.data.status == 'error' ){
                    jQuery("#success_message").addClass('red');
                }
            }
        });
    });

    /*
    * Delete Video
    * */
    jQuery(document).on('click','.video_del_icon', function () {
        $(".upload_video_section .spinner").show();
        var filename = jQuery(this).attr('data');
        jQuery.ajax({
            url : lms_media_script.ajax_url,
            type : 'post',
            data : {
                action : 'lms_media_delete_video',
                filename : filename
            },
            success : function( response ) {
                jQuery(".upload_video_section .spinner").hide();
                if ( response.data.status != 'error' ){
                    jQuery("#success_message").addClass('green');
                    get_user_shortcodes_via_ajax(response.data.dir);
                }
                else{
                    jQuery("#success_message").addClass('red');
                }
                jQuery('#success_message').html(response.data.message);
                setTimeout(function(){ jQuery('#success_message').html(''); }, 3000);
            }
        });
    });

    /*
    * Trigger click to radio button
    * */
    jQuery(document).on('click','.folders_list li label', function () {
        jQuery(this).prev().trigger('click');
    });

    /*
    * Close modal
    * */
    jQuery(document).on('click','.lms_close',function () {
        jQuery('#analytic_modal').css('display','none');
        jQuery("#lms_media_video_analytic_modal").html('');
        jQuery('#analytic_modal').removeClass('in');

        jQuery('#preview_modal').css('display','none');
        jQuery("#lms_media_video_preview_modal").html('');
        jQuery('#preview_modal').removeClass('in');
    })

    jQuery(document).on('click','.folders_list li',function () {
        currentLi = jQuery(this);
        jQuery('.folders_list li').each(function () {

           if ( jQuery(this).hasClass('folders_list_li_active') ){

               jQuery(this).removeClass('folders_list_li_active');

           }
        });
        currentLi.addClass('folders_list_li_active');
    });

    jQuery(document).on('click','.user_shortcodes li', function () {
        currentLi = jQuery(this);
        currentVideo = currentLi.children('.video_data').val();
        jQuery('.preview_shrtct').val(currentVideo);
        jQuery('.analytic_shrtct').val(currentVideo);
        jQuery('.user_shortcodes li').each(function () {

            if ( jQuery(this).hasClass('active_video') ){

                jQuery(this).removeClass('active_video');

            }
        });
        currentLi.addClass('active_video');

    });


    /*
        * Export Analytics to csv file
        * */
    function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td),tr:has(th)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function(i, row) {
                var $row = $(row),
                    $cols = $row.find('td,th');

                return $cols.map(function(j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"';

        // Deliberate 'false', see comment below
        if (false && window.navigator.msSaveBlob) {

            var blob = new Blob([decodeURIComponent(csv)], {
                type: 'text/csv;charset=utf8'
            });

            // Crashes in IE 10, IE 11 and Microsoft Edge
            // See MS Edge Issue #10396033
            // Hence, the deliberate 'false'
            // This is here just for completeness
            // Remove the 'false' at your own risk
            window.navigator.msSaveBlob(blob, filename);

        } else if (window.Blob && window.URL) {
            // HTML5 Blob
            var blob = new Blob([csv], {
                type: 'text/csv;charset=utf-8'
            });
            var csvUrl = URL.createObjectURL(blob);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvUrl
                });
        } else {
            // Data URI
            var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }
    }

    // This must be a hyperlink
    $(".export").on('click', function(event) {
        // CSV
        var args = [$('#analytics'), 'analytic.csv'];

        exportTableToCSV.apply(this, args);

        // If CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });

}); //Document Ready End

function copyClipboard() {
    var copyText = jQuery('.active_video .lms_media_video_shortcode');
    copyText.select();
    document.execCommand("Copy");
}
function get_user_shortcodes_via_ajax(folderName) {
    $(".upload_video_section .spinner").show();
    jQuery.ajax({
        url : lms_media_script.ajax_url,
        type : 'post',
        data : {
            action : 'get_current_user_shortcodes',
            foldername : folderName
        },
        success : function( response ) {
            jQuery('#lms_media_user_shortcodes').html( response.data );
            $(".upload_video_section .spinner").hide();
        }
    });

}
