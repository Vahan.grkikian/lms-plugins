<?php
/*
Plugin Name: LMS Helper Media
Plugin URI:
Description: LMS Helper Media
Version: 1.0
Author: LMS Helper Media
*/
define("MEDIA_PLUGIN_NAME","lms-helper-media");
define("USERNAME",get_option('els_streamingpublisher_account'));
define("PASSWORD",get_option('els_streamingpublisher_password'));

/*
 * Define ftp accesses
 * */
define("FTP_HOST","gtw.onstreammedia.com");
define("FTP_USERNAME",get_option('els_streamingpublisher_account'));
define("FTP_PASS",get_option('els_streamingpublisher_password'));

require_once ( __DIR__.'/requests.php' );
require_once ( __DIR__.'/activation.php' );

// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'lms_helper_media_install');
register_uninstall_hook(__FILE__, 'lms_helper_media_uninstall');

add_action( 'admin_enqueue_scripts', 'lms_media_load_admin_style' );

add_action( 'wp_enqueue_scripts', 'lms_media_load_front_assets' );

add_action( 'wp_ajax_get_current_user_shortcodes', 'get_current_user_shortcodes' );

add_action( 'wp_ajax_lms_media_get_analytic', 'lms_media_get_analytic' );

add_action( 'wp_ajax_lms_media_save_analytic', 'lms_media_save_analytic' );

add_action( 'wp_ajax_get_file_preview_html', 'get_file_preview_html' );

add_action( 'wp_ajax_lms_helper_media_create_folder', 'lms_helper_media_create_folder' );

add_action( 'wp_ajax_lms_helper_media_upload_by_ftp', 'lms_helper_media_upload_by_ftp' );

add_action( 'wp_ajax_lms_media_delete_video', 'lms_media_delete_video' );

add_action( 'admin_menu', 'lms_helper_media_admin_menu' );

add_shortcode( 'video', 'lms_media_video' );

add_action('init','getSpCallback');

add_filter( 'custom_menu_order', 'wpse_custom_menu_order', 10, 1 );
add_filter( 'menu_order', 'wpse_custom_menu_order', 10, 1 );
