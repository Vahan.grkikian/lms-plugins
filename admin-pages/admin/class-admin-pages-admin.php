<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    admin_pages
 * @subpackage admin_pages/admin
 * @author     Your Name <email@example.com>
 */
class admin_pages_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $admin_pages    The ID of this plugin.
	 */
	private $admin_pages;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $admin_pages       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $admin_pages, $version ) {

		$this->admin_pages = $admin_pages;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in admin_pages_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The admin_pages_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
   		wp_deregister_style('wp-admin');
		wp_enqueue_style( 'bootstrapnew', plugin_dir_url( __FILE__ ) . 'plugins/bootstrap/css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'roundedcss', plugin_dir_url( __FILE__ ) . 'css/components-rounded.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bluecss', plugin_dir_url( __FILE__ ) . 'css/blue.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'customcss', plugin_dir_url( __FILE__ ) . 'css/custom.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'layoutcss', plugin_dir_url( __FILE__ ) . 'css/layout.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'pluginscss', plugin_dir_url( __FILE__ ) . 'css/plugins.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'simpleline', plugin_dir_url( __FILE__ ) . 'plugins/simple-line-icons/simple-line-icons.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrapswitch', plugin_dir_url( __FILE__ ) . 'plugins/bootstrap-switch/css/bootstrap-switch.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'daterangepicker', plugin_dir_url( __FILE__ ) . 'plugins/bootstrap-daterangepicker/daterangepicker.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'morris', plugin_dir_url( __FILE__ ) . 'plugins/morris/morris.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'fullcalendar', plugin_dir_url( __FILE__ ) . 'plugins/fullcalendar/fullcalendar.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'jqvmap', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/jqvmap.css', array(), $this->version, 'all' );
		wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
		wp_enqueue_style( 'bootstrapswitch', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/select2/css/select2.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrapswitch', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/select2/css/select2-bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrap-wysihtml5', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrap-markdown', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'summernote', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-summernote/summernote.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'daterangepicker', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrap-datepicker3', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrap-timepicker', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'bootstrap-datetimepicker', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'clockface', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/clockface/css/clockface.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'dropzone', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/dropzone/dropzone.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'basic', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/dropzone/basic.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'wp-admin', plugin_dir_url( __FILE__ ) . 'css/admin-pages-admin.css', array(), $this->version, 'all' );

	}
 
	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in admin_pages_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The admin_pages_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( 'bootstrapjs', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'blockuijs', plugin_dir_url( __FILE__ ) . 'js/jquery.blockui.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'bootstrap-switch', plugin_dir_url( __FILE__ ) . 'js/bootstrap-switch/js/bootstrap-switch.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'momentjs', plugin_dir_url( __FILE__ ) . 'js/moment.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'appmin', plugin_dir_url( __FILE__ ) . 'app.js', array( 'jquery' ), '1.0.5', true );
		wp_enqueue_script( 'daterangepicker', plugin_dir_url( __FILE__ ) . 'js/bootstrap-daterangepicker/daterangepicker.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'datepicker', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'timepicker', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', array( 'jquery' ), $this->version, true );
		//wp_enqueue_script( 'datetimepicker', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', array( 'jquery' ), $this->version, true );		
		wp_enqueue_script( 'datetimepicker2', plugin_dir_url( __FILE__ ) . 'assets/pages/scripts/components-date-time-pickers.js', array( 'jquery' ), $this->version, true );			
		wp_enqueue_script( 'easypiechart', plugin_dir_url( __FILE__ ) . 'js/jquery-easypiechart/jquery.easypiechart.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'morris', plugin_dir_url( __FILE__ ) . 'js/morris/morris.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'raphael', plugin_dir_url( __FILE__ ) . 'js/morris/raphael-min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'counterup', plugin_dir_url( __FILE__ ) . 'js/counterup/jquery.waypoints.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'counterupjs', plugin_dir_url( __FILE__ ) . 'js/counterup/jquery.counterup.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amcharts', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/amcharts.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartsserial', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/serial.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartspie', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/pie.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartsserial', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/serial.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartsradar', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/radar.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartslight', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/themes/light.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartspatterns', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/themes/patterns.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartschalk', plugin_dir_url( __FILE__ ) . 'js/amcharts/amcharts/themes/chalk.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartsammap', plugin_dir_url( __FILE__ ) . 'js/amcharts/ammap/ammap.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartsworldLow', plugin_dir_url( __FILE__ ) . 'js/amcharts/ammap/maps/js/worldLow.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'amchartsamstock', plugin_dir_url( __FILE__ ) . 'js/amcharts/amstockcharts/amstock.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'fullcalendar', plugin_dir_url( __FILE__ ) . 'js/fullcalendar/fullcalendar.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'horizontal-timeline', plugin_dir_url( __FILE__ ) . 'js/horizontal-timeline/horizontal-timeline.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'flot', plugin_dir_url( __FILE__ ) . 'js/flot/jquery.flot.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'flotresize', plugin_dir_url( __FILE__ ) . 'js/flot/jquery.flot.resize.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'flotcats', plugin_dir_url( __FILE__ ) . 'js/flot/jquery.flot.categories.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'sparkline', plugin_dir_url( __FILE__ ) . 'js/jquery.sparkline.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'dashjs', plugin_dir_url( __FILE__ ) . 'js/dashboard.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'vmap', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/jquery.vmap.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'vmapru', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'vmapworld', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'vmapeu', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'vmapge', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'vmapusa', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'vmapsampledata', plugin_dir_url( __FILE__ ) . 'plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'wysihtml5', plugin_dir_url( __FILE__ ) . 'plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'wysihtml5boot', plugin_dir_url( __FILE__ ) . 'plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'jqvalidate', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/jquery-validation/js/jquery.validate.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'additional-methods', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/jquery-validation/js/additional-methods.min.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'bootstrapwiz', plugin_dir_url( __FILE__ ) . 'assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js', array( 'jquery' ), $this->version, true );	
		wp_enqueue_script( 'form-wizard', plugin_dir_url( __FILE__ ) . 'assets/pages/scripts/form-wizard.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->admin_pages, plugin_dir_url( __FILE__ ) . 'js/admin-pages-admin.js', array( 'jquery' ), $this->version, true );

	}

}

if(!function_exists('wp_get_current_user')) {
    include(ABSPATH . "wp-includes/pluggable.php"); 
}

if ( !current_user_can( 'be_overlord' ) ) {
function adminpages_admin_head() {
	echo '<style>#toplevel_page_pb_backupbuddy_backup, #toplevel_page_wpseo_dashboard, li#menu-posts-onstream_views, #toplevel_page_gadash_settings, #toplevel_page_the7-dashboard, #toplevel_page_of-options-wizard, #menu-posts, #menu-media, #menu-pages, #menu-comments, #toplevel_page_samurai_db-page, #menu-posts-ex-speaker, #toplevel_page_wpcf7, #toplevel_page_woocommerce, #menu-posts-product, #menu-posts-membership_plan, #menu-appearance, #menu-plugins, #menu-users, #menu-tools, #toplevel_page_vc-general, #menu-settings, #toplevel_page_learndash-lms, #toplevel_page_wooevents, #collapse-menu, li#toplevel_page_AdminPageFrameworkLoader_AdminPage, li#toplevel_page_learndash-reports-page, li#menu-posts-forumli#menu-posts-topic, li#menu-posts-reply, li#toplevel_page_bp-activity, li#toplevel_page_bp-groups, li#toplevel_page_buddydrive-files, li#menu-posts-bp-email, li#toplevel_page_buddyboss-settings, li#toplevel_page_badgeos_badgeos, li#menu-posts-forum, li#menu-posts-topic, li#menu-posts-newsletter, li#toplevel_page_gf_edit_forms, li#toplevel_page_wcx_wcreport_plugin_dashboard-parent-dashboard, li#toplevel_page_about-ultimate  {display: none !important; visibility: hidden !important}.wp-core-ui {clear: both;} </style>';
}
add_action( 'admin_head', 'adminpages_admin_head' );
}

function adminpages_admin_head_all() {
	echo '<style>.dashicons-dashboard:before {content: "\f102";} a#insert_badgeos_shortcodes {display: none !important;} .wpseo-tab-video-container, .wpseo-metabox-buy-premium {display: none;}#dashboard-widgets-wrap .postbox {max-width: 600px;} #dashboard-widgets div#postbox-container-2, .newsletter_page_mailster_dashboard div#addons-panel, div#sidebar-container, .yoast_premium_upsell_admin_block, .hide-if-no-customize, .actions.bulkactions, div#wp-eventdescription-editor-container div#mceu_14, div#wp-eventdescription-editor-container div#mceu_15, .wrap.awr-news-cnt-wrap, nav#ml-menu, .google-analytics_page_gadash_backend_settings div#postbox-container-1 {display: none !important;visibility: hidden;} .wp-admin .awr-content {margin: 0; } .forms_page_gf_export div#gform_tab_container {margin-left: 0;}.forms_page_gf_export .gform_tabs {display: none;visibility: hidden;width: 0;}#dashboard-widgets-wrap div#gadwp-widget, #gadwp-bottomstats-1 .inside {max-width: 100%;width: 100%;}</style>';
}
add_action( 'admin_head', 'adminpages_admin_head_all' );


add_filter('admin_title', 'my_admin_title', 10, 2);

function my_admin_title($admin_title, $title)
{
    return get_bloginfo('name').' &bull; '.$title;
}
// First, create a function that includes the path to your favicon
function add_favicon() {
  	$favicon_url = plugin_dir_url( __FILE__ ) . 'img/Onstream_arrows.png';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}
  
// Now, just make sure that function runs when you're on the login page and admin pages  
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

function custom_admin_js() {
    echo "<script type='text/javascript' > 
document.body.className+=' folded';                 
</script>
<script type='text/javascript' > 
jQuery('body :not(script)').contents().filter(function() {
    return this.nodeType === 3;
  }).replaceWith(function() {
      return this.nodeValue.replace('LearnDash','LMS');
  });
</script>
<script type='text/javascript' > 
jQuery('body :not(script)').contents().filter(function() {
    return this.nodeType === 3;
  }).replaceWith(function() {
      return this.nodeValue.replace('Mailster','Marketing');
  });
</script>";  
}
add_action('admin_footer', 'custom_admin_js');

add_action( 'admin_bar_menu', 'remove_my_account', 999 );
function remove_my_account( $wp_admin_bar ) {
    //$wp_admin_bar->remove_node( 'my-account' );
    $wp_admin_bar->remove_node( 'user-info' );
    $wp_admin_bar->remove_node( 'edit-profile' );
}

add_action( 'admin_bar_menu', 'add_logout', 999 );
function add_logout( $wp_admin_bar ) {
    $args = array(
        'id'     => 'logout',           // id of the existing child node (New > Post)
        'title'  => 'Logout?',   // alter the title of existing node
        'parent' => 'top-secondary',    // set parent
    );
    $wp_admin_bar->add_node( $args );
}

add_action( 'admin_menu', 'stop_access_profile' );
function stop_access_profile() {
    remove_menu_page( 'profile.php' );
    remove_submenu_page( 'users.php', 'profile.php' );
    if(IS_PROFILE_PAGE === true) {
        wp_die( 'You are not permitted to change your own profile information. Please contact a member of Support to have your profile information changed.' );
    }
}

function rkv_remove_columns( $columns ) {
	// remove the Yoast SEO columns
	unset( $columns['wpseo-score'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
	unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-links'] );
	unset( $columns['gadwp_stats'] );
	unset( $columns['comments'] );
	return $columns;
}
add_filter ( 'manage_edit-post_columns', 'rkv_remove_columns' );
add_filter ( 'manage_edit-page_columns', 'rkv_remove_columns' );
add_filter( 'manage_edit-product_columns', 'rkv_remove_columns',10, 1 );
add_filter( 'manage_edit-sfwd-lessons_columns', 'rkv_remove_columns',10, 1 );
add_filter( 'manage_edit-sfwd-topic_columns', 'rkv_remove_columns',10, 1 );
add_filter( 'manage_edit-sfwd-quiz_columns', 'rkv_remove_columns',10, 1 );
add_filter( 'manage_edit-sfwd-certificates_columns', 'rkv_remove_columns',10, 1 );
add_filter( 'manage_edit-sfwd-assignment_columns', 'rkv_remove_columns',10, 1 );

function my_custom_taxonomy_columns( $columns )
{
	$columns['my_term_id'] = __('Term ID');

	return $columns;
}
add_filter('manage_edit-category_columns' , 'my_custom_taxonomy_columns');
add_filter('manage_product_cat_custom_column' , 'my_custom_taxonomy_columns');

function my_remove_wp_seo_meta_box() {
	remove_meta_box('wpseo_meta', 'sfwd-quiz', 'normal');
	remove_meta_box('commentstatusdiv', 'sfwd-quiz', 'normal');
	remove_meta_box('commentsdiv', 'sfwd-quiz', 'normal');
	remove_meta_box('authordiv', 'sfwd-quiz', 'normal');
	remove_meta_box('wpseo_meta', 'sfwd-certificates', 'normal');
	remove_meta_box('commentstatusdiv', 'sfwd-certificates', 'normal');
	remove_meta_box('commentsdiv', 'sfwd-certificates', 'normal');
	remove_meta_box('authordiv', 'sfwd-certificates', 'normal');
	remove_meta_box('revisionsdiv', 'sfwd-certificates', 'normal');
}
add_action('add_meta_boxes', 'my_remove_wp_seo_meta_box', 100);

add_action( 'admin_init', 'wpseo_remove_yoast_seo_posts_filter', 20 );
function wpseo_remove_yoast_seo_posts_filter() {
    global $wpseo_meta_columns;

    if ( $wpseo_meta_columns ) {
    	remove_action( 'restrict_manage_posts', array( $wpseo_meta_columns, 'posts_filter_dropdown_readability' ) );
        remove_action( 'restrict_manage_posts', array( $wpseo_meta_columns, 'posts_filter_dropdown' ) );
    }
}
