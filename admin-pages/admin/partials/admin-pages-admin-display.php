<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/admin/partials
 */

function edit_admin_menus() {
    global $menu;
     
    $menu[2][0] = 'Home'; // Change Dashboard to Home

    remove_menu_page('tools.php'); // Remove the Tools Menu
    remove_menu_page('edit.php'); // Remove the Tools Menu
    remove_submenu_page('index.php','index.php');
    remove_submenu_page('index.php','update-core.php');
}
add_action( 'admin_menu', 'edit_admin_menus' );

add_action( 'admin_menu', 'lms_admin_menu' );

function lms_admin_menu() {
    //add_menu_page( 'Home', 'Home', 'manage_options', 'dashboard', 'courses_home_page', 'dashicons-admin-home' );
    add_menu_page( 'Events', 'Events', 'manage_options', 'events', 'courses_events_page', 'dashicons-tickets' );
    add_submenu_page( 'events', 'Deleted Events', 'Deleted Events','manage_options', 'events_deleted', 'courses_deleted_events_page');
    add_submenu_page( 'events', 'Categories', 'Categories','manage_options', '/edit-tags.php?taxonomy=category');
    add_submenu_page( 'events', 'Tags', 'Tags','manage_options', '/edit-tags.php?taxonomy=post_tag');
    add_menu_page( 'Courses', 'Courses', 'manage_options', 'courses', 'courses_admin_page', 'dashicons-clipboard' );
    add_submenu_page( 'courses', 'Add New', 'Add New','manage_options', '/post-new.php?post_type=sfwd-courses');
    add_submenu_page( 'courses', 'Deleted Courses', 'Deleted Courses','manage_options', 'courses_deleted', 'courses_deleted_courses_page');
    add_submenu_page( 'courses', 'Lessons', 'Lessons','manage_options', 'edit.php?post_type=sfwd-lessons');
    add_submenu_page( 'courses', 'Topics', 'Topics','manage_options', 'edit.php?post_type=sfwd-topic');
    add_submenu_page( 'courses', 'Quizzes', 'Quizzes','manage_options', 'edit.php?post_type=sfwd-quiz');
    add_submenu_page( 'courses', 'Certificates', 'Certificates','manage_options', 'edit.php?post_type=sfwd-certificates');
    add_submenu_page( 'courses', 'Assignments', 'Assignments','manage_options', 'edit.php?post_type=sfwd-assignment');
    add_submenu_page( 'courses', 'Categories', 'Categories','manage_options', '/edit-tags.php?taxonomy=category');
    add_submenu_page( 'courses', 'Tags', 'Tags','manage_options', '/edit-tags.php?taxonomy=post_tag');
    add_menu_page( 'Membership', 'Membership', 'manage_options', 'edit.php?post_type=membership_plan', '', 'dashicons-groups' );
//	add_menu_page( 'Membership', 'Membership', 'manage_options', 'members', 'courses_membership_page', 'dashicons-groups' );
//	add_submenu_page( 'members', 'Membership Plans', 'Membership Plans', 'manage_options', 'edit.php?post_type=membership_plan');
    add_menu_page( 'Blog', 'Blog', 'manage_options', 'blog', 'courses_blog_page', 'dashicons-admin-post' );
    add_submenu_page( 'blog', 'Deleted Posts', 'Deleted Posts','manage_options', 'posts_deleted', 'courses_deleted_posts_page');
    add_submenu_page( 'blog', 'Categories', 'Categories','manage_options', '/edit-tags.php?taxonomy=category');
    add_submenu_page( 'blog', 'Tags', 'Tags','manage_options', '/edit-tags.php?taxonomy=post_tag');
    add_submenu_page( 'blog', 'Comments', 'Comments','manage_options', '/edit-comments.php');
    add_menu_page( 'Pages', 'Pages', 'manage_options', 'pages', 'courses_pages_page', 'dashicons-welcome-widgets-menus' );
    add_submenu_page( 'pages', 'Deleted Pages', 'Deleted Pages','manage_options', 'pages_deleted', 'courses_deleted_pages_page');
    add_menu_page( 'Forms', 'Forms', 'manage_options', 'admin.php?page=gf_edit_forms', '', 'dashicons-format-aside' );
    add_submenu_page( 'admin.php?page=gf_edit_forms', 'Export Form Entries', 'Export Form Entries', 'manage_options', 'admin.php?page=gf_export&view=export_entry');
    add_menu_page( 'Images', 'Images', 'manage_options', '/upload.php', '', 'dashicons-format-image' );
    //add_menu_page( 'Files', 'Files', 'manage_options', 'files', 'courses_files_page', 'dashicons-media-default' );
    //add_submenu_page( 'files', 'Media', 'Media', 'manage_options', '/upload.php');
//	add_submenu_page( '/upload.php', 'Media Upload', 'Media Upload', 'manage_options', '/media-new.php');
    add_menu_page( 'Emails', 'Emails', 'manage_options', 'admin.php?page=mailster_dashboard', '', 'dashicons-testimonial' );
    add_menu_page( 'eCommerce', 'eCommerce', 'manage_options', 'edit.php?post_type=shop_order', '', 'dashicons-cart' );
    add_submenu_page( 'edit.php?post_type=shop_order', 'Products', 'Products','manage_options', 'edit.php?post_type=product');
    //add_submenu_page( 'edit.php?post_type=shop_order', 'Add New Product', 'Add New Product','manage_options', 'post-new.php?post_type=product');
    add_submenu_page( 'edit.php?post_type=shop_order', 'Categories', 'Categories','manage_options', 'edit-tags.php?taxonomy=product_cat&amp;post_type=product');
    add_submenu_page( 'edit.php?post_type=shop_order', 'Tags', 'Tags','manage_options', 'edit-tags.php?taxonomy=product_tag&amp;post_type=product');
    add_submenu_page( 'edit.php?post_type=shop_order', 'Attributes', 'Attributes','manage_options', 'edit.php?post_type=product&amp;page=product_attributes');
    add_submenu_page( 'edit.php?post_type=shop_order', 'Coupons', 'Coupons','manage_options', 'edit.php?post_type=shop_coupon');
    add_submenu_page( 'edit.php?post_type=shop_order', 'Ecommerce Settings', 'Ecommerce Settings','manage_options', 'admin.php?page=wc-settings');
    add_menu_page( 'Users', 'Users', 'manage_options', 'users.php', '', 'dashicons-admin-users');
//	add_menu_page( 'Emails', 'Emails', 'manage_options', 'emails', 'courses_emails_page', 'dashicons-email-alt' );
    add_menu_page( 'Reports', 'Reports', 'manage_options', 'reports_page', 'reports_page', 'dashicons-chart-bar' );
    add_submenu_page( 'reports_page', 'Export Form Entries', 'Export Form Entries', 'manage_options', 'admin.php?page=gf_export&view=export_entry');
//	add_submenu_page( 'reports_page', 'Reports', 'Reports', 'manage_options', 'admin.php?page=wcx_wcreport_plugin_dashboard&parent=dashboard');
    add_submenu_page( 'reports_page', 'Reports Details', 'Reports Details', 'manage_options', 'admin.php?page=wc-reports');
    add_menu_page( 'Website', 'Website', 'manage_options', '/admin.php?page=boss_options', '', 'dashicons-admin-site' );
    add_submenu_page( '/admin.php?page=boss_options', 'Menus', 'Menus','manage_options', 'nav-menus.php');
    add_submenu_page( '/admin.php?page=boss_options', 'Widgets', 'Widgets','manage_options', 'widgets.php');
    add_menu_page( 'Integrations', 'Integrations', 'manage_options', 'integrations', 'courses_integrations_page', 'dashicons-admin-plugins' );
//	add_menu_page( 'Account', 'Account', 'manage_options', 'account', 'courses_account_page', 'dashicons-admin-users' );
//	add_menu_page( 'Settings', 'Settings', 'manage_options', 'settings', 'courses_settings_page', 'dashicons-admin-generic' );

    add_submenu_page( 'hidden', 'Webinar Options', 'Webinar Options','edit_themes', 'plugin-options.php', 'mynewtheme_admin');
    add_submenu_page( 'hidden', 'Video Options', 'Video Options','edit_themes', 'onstream-video-options.php', 'mynewtheme_admin_video');
    add_submenu_page( 'hidden', 'Streaming Publisher Options', 'Streaming Publisher Options','edit_themes', 'streamingpublisher-options.php', 'mynewtheme_admin_sp');
    add_submenu_page( 'hidden', 'Webcaster Options', 'Webcaster Options','edit_themes', 'visualwebcaster-options.php', 'mynewtheme_admin_webcast');
    add_submenu_page( 'hidden', 'New Event', 'New Event','edit_themes', 'newevent', 'courses_addnew_events_page' );
}

require plugin_dir_path( __FILE__ ) . 'custom-post-types.php';
require plugin_dir_path( __FILE__ ) . 'list-posts.php';
require plugin_dir_path( __FILE__ ) . 'list-pages.php';
require plugin_dir_path( __FILE__ ) . 'list-deleted-pages.php';
require plugin_dir_path( __FILE__ ) . 'list-deleted-posts.php';
require plugin_dir_path( __FILE__ ) . 'list-deleted-events.php';
require plugin_dir_path( __FILE__ ) . 'list-deleted-courses.php';
require plugin_dir_path( __FILE__ ) . 'list-events.php';
require plugin_dir_path( __FILE__ ) . 'list-courses.php';
require plugin_dir_path( __FILE__ ) . 'dashboard.php';
require plugin_dir_path( __FILE__ ) . 'member-list.php';
require plugin_dir_path( __FILE__ ) . 'integrations.php';
require plugin_dir_path( __FILE__ ) . 'email_shortcodes.php';
require plugin_dir_path( __FILE__ ) . 'reports-dashboard.php';
require plugin_dir_path( __FILE__ ) . 'visualwebcaster-options.php';
require plugin_dir_path( __FILE__ ) . 'streamingpublisher-options.php';
require plugin_dir_path( __FILE__ ) . 'onstream-video-options.php';

function courses_home_page(){
	?>
	<div class="wrap">
		<?php tt_render_dashboard_page(); ?>
	</div>
	<?php
}
function reports_page(){
	?>
	<div class="wrap">
		<h2>Reports</h2>
		<?php tt_render_reports_page(); ?>
	</div>
	<?php
}
function courses_membership_page(){
	?>
	<div class="wrap">
		<h2>Membership</h2>
		<?php tt_render_member_page(); ?>
	</div>
	<?php
}
function courses_events_page(){
	?>
	<div class="wrap">
		<?php tt_render_events_page(); ?>
	</div>
	<?php
}
function courses_addnew_events_page(){
	?>
	<div class="wrap">
		<?php tt_render_new_event_page(); ?>
	</div>
	<?php
}
function courses_admin_page(){
	?>
	<div class="wrap">
		<?php tt_render_courses_page(); ?>
	</div>
	<?php
}
function courses_blog_page(){
	tt_render_posts_page();
}
function courses_website_page(){
	?>
	<div class="wrap">
		<h2>Website</h2>
	</div>
	<?php
}
function courses_pages_page(){
	tt_render_pages_page(); 
}
function courses_deleted_pages_page(){
	tt_render_deleted_pages_page(); 
}
function courses_deleted_posts_page(){
	tt_render_deleted_posts_page(); 
}
function courses_deleted_events_page(){
	tt_render_deleted_events_page(); 
}
function courses_deleted_courses_page(){
	tt_render_deleted_courses_page(); 
}
function courses_files_page(){
	?>
	<div class="wrap">
		<h2>Files</h2>
	</div>
	<?php
}
function courses_messages_page(){
	?>
	<div class="wrap">
		<h2>Messages</h2>
	</div>
	<?php
}
function courses_ecommerce_page(){
	?>
	<div class="wrap">
		<h2>eCommerce</h2>
	</div>
	<?php
}
function courses_emails_page(){
	?>
	<div class="wrap">
		<h2>Emails</h2>
	</div>
	<?php
}
function courses_integrations_page(){
	?>
	<div class="wrap">
		<?php tt_render_integrations_page(); ?>
	</div>
	<?php
}
function courses_account_page(){
	?>
	<div class="wrap">
		<h2>Account</h2>
	</div>
	<?php
}
function courses_settings_page(){
	?>
	<div class="wrap">
		<h2>Settings</h2>
	</div>
	<?php
}


