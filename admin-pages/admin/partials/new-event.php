<?php /** *************************** RENDER TEST PAGE ******************************** ******************************************************************************* * This function renders the admin page and the example list table. Although it 's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and * it 's the way the list tables are used in the WordPress core.
 */
function tt_render_new_event_page()
{

    ?>
    <div class="row">
        <div class="upload_video_section" style="display: none;">
            <div class="spinner" style="z-index: 99;"></div>
        </div>
        <div class="col-md-12">
            <div class="portlet light " id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                                        <span class="caption-subject uppercase"> New Event -
                                            <span class="step-title"> Step 1 of 5 </span>
                                        </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" action="#" id="submit_form" method="POST">
                        <div class="alert alert-danger lms_alert"></div>
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li class="active">
                                        <a href="#eventtype" data-toggle="tab" class="step active" aria-expanded="true">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Event </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#setup-event" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Info </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#settings" data-toggle="tab" class="step">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                                <i class="fa fa-check"></i> LMS </span>
                                        </a>
                                    </li>
                                    <li class="eventitem" id="webinar">
                                        <a href="#onstream" data-toggle="tab" class="step">
                                            <span class="number"> 4 </span>
                                            <span class="desc eventitem" id="webinar">
                                                                <i class="fa fa-check"></i> Onstream
                                                            </span>
                                            <span class="desc eventitem" id="other" style="display:none;">
                                                                <i class="fa fa-check"></i> Other
                                                            </span>
                                            <span class="desc eventitem" id="webcast" style="display:none;">
                                                                <i class="fa fa-check"></i> Webcast
                                                            </span>
                                            <span class="desc eventitem" id="sp_upload" style="display:none;">
                                                                <i class="fa fa-check"></i> Upload Video
                                                            </span>
                                            <span class="desc eventitem" id="go_to_webinar" style="display:none;">
                                                                <i class="fa fa-check"></i> Go to Webinar
                                                            </span>
                                            <span class="desc eventitem"  id="webex" style="display:none;">
                                                                <i class="fa fa-check"></i> Webex
                                                            </span>
                                            <span class="desc eventitem"  id="livestream" style="display:none;">
                                                                <i class="fa fa-check"></i> Livestream
                                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#publish" data-toggle="tab" class="step">
                                            <span class="number"> 5 </span>
                                            <span class="desc">
                                                                <i class="fa fa-check"></i> Publish </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-info" role="progressbar">
                                    <div class="progress-bar progress-bar-info" style="width: 20%;"></div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button>
                                        You have some form errors. Please check below.
                                    </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button>
                                        Your form validation is successful!
                                    </div>

                                    <!-- Set Event Type -->
                                    <div class="tab-pane active" id="eventtype">

                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">

                                                <div class="portlet light">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <div class="form-body mt-element-overlay">
                                                            <h3 class="form-section">Choose Your Event Type</h3>
                                                            <div class="row">
                                                                <div class="col-md-6 text-center">
                                                                    <a href="#"
                                                                       class="eventchoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                       onclick="return false;">Onstream Webinars</a>
                                                                    <input type="radio" name="eventtype" id="webinar"
                                                                           value="webinar" checked=""
                                                                           style="display:none;">
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6 text-center">
                                                                    <a href="#"
                                                                       class="eventchoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                       data-container="body" data-placement="top"
                                                                       data-original-title="Streaming Publisher"
                                                                       onclick="return false;">Upload Content</a>
                                                                    <input type="radio" name="eventtype" id="sp_upload"
                                                                           value="sp_upload" checked=""
                                                                           style="display:none;">
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            <!--/row-->
                                                            <div class="row">
                                                                <div class="col-md-6 text-center">
                                                                    <a href="#"
                                                                       class="eventchoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                       onclick="return false;">VisualWebcaster</a>
                                                                    <input type="radio" name="eventtype" id="webcast"
                                                                           value="webcast" checked=""
                                                                           style="display:none;">
                                                                </div>
                                                                <!--/span-->
                                                                <div class="col-md-6 text-center">
                                                                    <a href="#"
                                                                       class="eventchoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                       onclick="return false;">Link to External
                                                                        Content</a>
                                                                    <input type="radio" name="eventtype" id="other"
                                                                           value="other" checked=""
                                                                           style="display:none;">
                                                                </div>
                                                                <!--/span-->
                                                            </div>
                                                            <!--/row-->
                                                            <div class="row" style="text-align: center">
                                                                <div class="col-md-6 text-center" style="float:left;">
                                                                    <a href="#"
                                                                       class="eventchoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                       onclick="return false;">Go to Webinar
                                                                        </a>
                                                                    <input type="radio" name="eventtype" id="go_to_webinar"
                                                                           value="go_to_webinar" checked=""
                                                                           style="display:none;">
                                                                </div>
                                                                <div class="col-md-6 text-center" style="float:right;">
                                                                    <a href="#"
                                                                       class="eventchoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                       onclick="return false;">Webex
                                                                    </a>
                                                                    <input type="radio" name="eventtype" id="webex"
                                                                           value="webex" checked=""
                                                                           style="display:none;">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 text-center" style="float:left;">
                                                                    <a href="#"
                                                                       class="eventchoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                       onclick="return false;">Livestream
                                                                    </a>
                                                                    <input type="radio" name="eventtype" id="livestream"
                                                                           value="livestream" checked=""
                                                                           style="display:none;">
                                                                </div>
                                                            </div>
                                                            <!--/row-->
                                                        </div>
                                                        <script type="text/javascript">
                                                            jQuery("a.eventchoice").click(function ($) {
                                                                jQuery("a.eventchoice").removeClass("active");
                                                                jQuery("a.eventchoice").next().prop("checked", false);
                                                                jQuery(this).addClass("active").next().prop("checked", "checked");
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="setup-event">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <div class="form-body">
                                                            <!-- START FORM CONTENT-->
                                                            <h3 class="form-section">Set Up Your Event</h3>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Enter Your Title
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <input type="text" class="form-control lms_required"
                                                                               name="topic">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Upload Featured
                                                                            Image
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <br>
                                                                        <button type="button" class="btn green"
                                                                                id="featured_image_upload">Upload
                                                                        </button>
                                                                        <br/><br/>
                                                                        <img src="" width="250px" height="250px"
                                                                             id="featured_image_thumb"
                                                                             style="display:none;max-width:250px;"/>
                                                                        <input type="hidden" name="featured_image_id"
                                                                               id="featured_image_id" class="lms_required" value="" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Date
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <div class="input-icon">
                                                                            <i class="fa fa-calendar-o"></i>
                                                                            <input type="text" size="12"
                                                                                   class="form-control form-control-inline input-medium date-picker lms_required"
                                                                                   name="start_date">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Time
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <div class="input-icon">
                                                                            <i class="fa fa-clock-o"></i>
                                                                            <input type="text"
                                                                                   class="form-control timepicker timepicker-24 lms_required"
                                                                                   name="start_time">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Duration
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <select name="duration" class="form-control lms_required">
                                                                            <option value="">
                                                                                - Select -
                                                                            </option>
                                                                            <option value="15">
                                                                                15 min
                                                                            </option>
                                                                            <option value="30">
                                                                                30 min
                                                                            </option>
                                                                            <option value="45">
                                                                                45 min
                                                                            </option>
                                                                            <option value="60">
                                                                                60 min
                                                                            </option>
                                                                            <option value="75">
                                                                                75 min
                                                                            </option>
                                                                            <option value="90">
                                                                                90 min
                                                                            </option>
                                                                            <option value="105">
                                                                                105 min
                                                                            </option>
                                                                            <option value="120">
                                                                                120 min
                                                                            </option>
                                                                            <option value="135">
                                                                                135 min
                                                                            </option>
                                                                            <option value="150">
                                                                                150 min
                                                                            </option>
                                                                            <option value="165">
                                                                                165 min
                                                                            </option>
                                                                            <option value="180">
                                                                                180 min
                                                                            </option>
                                                                            <option value="195">
                                                                                195 min
                                                                            </option>
                                                                            <option value="210">
                                                                                210 min
                                                                            </option>
                                                                            <option value="225">
                                                                                225 min
                                                                            </option>
                                                                            <option value="240">
                                                                                240 min
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Timezone
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <select class="form-control lms_required" name="timezone">
                                                                            <option value="UM5" selected>Eastern
                                                                                Standard Time (US & Canada)
                                                                            </option>
                                                                            <option value="UM12"> International Date
                                                                                Line West
                                                                            </option>
                                                                            <option value="UM11"> Midway Island, Samoa
                                                                            </option>
                                                                            <option value="UM10"> Hawaii-Aleutian
                                                                                Standard Time (US & Canada)
                                                                            </option>
                                                                            <option value="UM95"> Taiohae, Marquesas
                                                                                Islands
                                                                            <option value="UM9"> Alaskan Standard Time
                                                                                (US & Canada)
                                                                            </option>
                                                                            <option value="UM8"> Pacific Standard Time
                                                                                (US & Canada)
                                                                            </option>
                                                                            <option value="UM7"> Mountain Standard Time
                                                                                (US & Canada)
                                                                            </option>
                                                                            <option value="UM6"> Central Standard Time
                                                                                (US & Canada)
                                                                            </option>
                                                                            <option value="UM5"> Eastern Standard Time
                                                                                (US & Canada)
                                                                            </option>
                                                                            <option value="UM45"> Venezuelan Standard
                                                                                Tim
                                                                            </option>
                                                                            <option value="UM4"> Atlantic Standard Time
                                                                                (Canada)
                                                                            </option>
                                                                            <option value="UM35"> St. John`s,
                                                                                Newfoundland and Labrador
                                                                            </option>
                                                                            <option value="UM3"> Brazil, Buenos Aires,
                                                                                Georgetown
                                                                            </option>
                                                                            <option value="UM2"> Mid-Atlantic</option>
                                                                            <option value="UM1"> Azores, Cape Verde
                                                                                Islands
                                                                            </option>
                                                                            <option value="UTC"> Western Europe Time,
                                                                                London, Lisbon, Casablanca
                                                                            </option>
                                                                            <option value="UP1"> Berlin, Brussels,
                                                                                Copenhagen, Madrid, Paris
                                                                            </option>
                                                                            <option value="UP2"> South Africa</option>
                                                                            <option value="UP3"> Baghdad, Riyadh,
                                                                                Kaliningrad
                                                                            </option>
                                                                            <option value="UP35"> Tehran</option>
                                                                            <option value="UP4"> Moscow, St. Petersburg,
                                                                                Abu Dhabi, Muscat, Baku, Tbilisi
                                                                            </option>
                                                                            <option value="UP45"> Kabul</option>
                                                                            <option value="UP5"> Islamabad, Karachi,
                                                                                Tashkent
                                                                            </option>
                                                                            <option value="UP55"> Bombay, Calcutta,
                                                                                Madras, New Delhi
                                                                            </option>
                                                                            <option value="UP575"> Kathmandu</option>
                                                                            <option value="UP6"> Ekaterinburg, Almaty,
                                                                                Dhaka, Colombo
                                                                            </option>
                                                                            <option value="UP65"> Yagoon</option>
                                                                            <option value="UP7"> Omsk, Bangkok, Hanoi,
                                                                                Jakarta
                                                                            </option>
                                                                            <option value="UP8"> Krasnoyarsk, Beijing,
                                                                                Perth, Singapore, Hong Kong
                                                                            </option>
                                                                            <option value="UP875"> Western Australia,
                                                                                Caiguna, Eucla
                                                                            </option>
                                                                            <option value="UP9"> Irkutsk, Tokyo, Seoul,
                                                                                Osaka, Sapporo
                                                                            </option>
                                                                            <option value="UP95"> Adelaide, Darwin
                                                                            </option>
                                                                            <option value="UP10"> Yakutsk, Eastern
                                                                                Australia, Guam
                                                                            </option>
                                                                            <option value="UP105"> Lord Howe Island
                                                                                (Australia)
                                                                            </option>
                                                                            <option value="UP11"> Vladivostok, Solomon
                                                                                Islands, New Caledonia
                                                                            </option>
                                                                            <option value="UP115"> Norfolk Island
                                                                            </option>
                                                                            <option value="UP12"> Magadan, Auckland,
                                                                                Wellington, Fiji
                                                                            </option>
                                                                            <option value="UP1275"> Chatham Island
                                                                            </option>
                                                                            <option value="UP13"> Tonga</option>
                                                                            <option value="UP14"> Kiribati</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row col-lg-4">
                                                                <div class="rpwcm_post_membership_field">
                                                                    <span class="title"><?php _e('Access Restriction Method', 'woocommerce-membership'); ?></span>
                                                                    <select id="_rpwcm_post_restriction_method_quick" name="_rpwcm_post_restriction_method" class="rpwcm_post_restriction_method">
                                                                        <optgroup label="<?php _e('No Restriction', 'woocommerce-membership'); ?>">
                                                                            <option value="none"><?php _e('No Restriction', 'woocommerce-membership'); ?></option>
                                                                        </optgroup>
                                                                        <optgroup label="<?php _e('Members Only', 'woocommerce-membership'); ?>">
                                                                            <option value="all_members"><?php _e('All Members', 'woocommerce-membership'); ?></option>
                                                                            <option value="members_with_plans" ><?php _e('Members With Specific Plans', 'woocommerce-membership'); ?></option>
                                                                        </optgroup>
                                                                        <optgroup label="<?php _e('Non-Members Only', 'woocommerce-membership'); ?>">
                                                                            <option value="non_members"><?php _e('All Non-Members', 'woocommerce-membership'); ?></option>
                                                                            <option value="users_without_plans" ><?php _e('Users Without Specific Plans', 'woocommerce-membership'); ?></option>
                                                                        </optgroup>
                                                                    </select>
                                                                </div>
                                                                <div class="rpwcm_show_if_restrict_access_by_plan">
                                                                    <div class="rpwcm_post_membership_field">
                                                                        <?php WooCommerce_Membership::render_field_multiselect(array('name' => '_rpwcm_only_caps', 'class' => '_rpwcm_only_plans', 'values' => WooCommerce_Membership_Plan::get_list_of_all_plan_keys(), 'selected' => $selected)); ?>
                                                                    </div>
                                                                    <p style="margin-bottom: 0;">
                                                                        <?php printf(__('Control your membership plans %shere%s.', 'woocommerce-membership'), '<a href="' . admin_url('edit.php?post_type=membership_plan') . '">', '</a>'); ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Description
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <?php $settings = array('media_buttons' => false);
                                                                        wp_editor('Please add the Event Description here ', 'eventdescription', $settings); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-pane" id="settings">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <div class="form-body">
                                                            <!-- START FORM CONTENT-->

                                                            <h3 class="form-section">Settings</h3>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Instructor
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <select class="form-control lms_required" name="Instructor">
                                                                            <option value="">Select</option>
                                                                            <?php
                                                                            $args2 = array(
                                                                                'role__in ' => ['author', 'editor', 'administrator'],
                                                                                'orderby ' => 'user_nicename ',
                                                                                'order ' => 'ASC '
                                                                            );
                                                                            $authors = get_users($args2);
                                                                            foreach ($authors as $user) {
                                                                                echo '<option value="' . $user->id . '">' . $user->display_name . ' [' . $user->user_email . ']</option>';
                                                                            } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Regular Price
                                                                            <span class="required" aria-required="true">*</span>
                                                                        </label>
                                                                        <input type="text" class="form-control lms_required"
                                                                               name="regularprice">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Categories
                                                                            <span class="required" aria-required="true">*</span>
                                                                        </label>
                                                                        <ul class="categories list-group">
                                                                            <?php wp_category_checklist(); ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1"></div>
                                                                <!--    <div class="col-md-5">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Discount Price
                                                                                <span class="required" aria-required="true">*</span>
                                                                            </label>
                                                                            <input type="text" class="form-control" name="saleprice">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                -->
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Maximum Attendees
                                                                            <span class="required" aria-required="true">*</span>
                                                                        </label>
                                                                        <input type="text" class="form-control lms_required"
                                                                               name="maxattendees">
                                                                    </div>
                                                                </div>
                                                                <!--    <div class="col-md-1"></div>
                                                                  <div class="col-md-5">
                                                                       <div class="form-group">
                                                                           <label class="control-label">Membership Access
                                                                           <span class="required" aria-required="true"> * </span>
                                                                           </label>
                                                                           <input type="text" class="form-control">
                                                                       </div>
                                                                 </div>
                                                                </div>

                                                               <div class="row">
                                                                   <div class="col-md-5">
                                                                       <div class="form-group">
                                                                           <label class="control-label">Require Member Approval
                                                                           <span class="required" aria-required="true"> * </span>
                                                                           </label>
                                                                           <input type="text" class="form-control" name="memberapproval">
                                                                       </div>
                                                                   </div>
                                                                   <div class="col-md-1"></div>
                                                                   <div class="col-md-5">
                                                                   </div>-->
                                                            </div>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Onstream ID -->
                                    <div class="tab-pane" id="onstream">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <div class="form-body eventtypes_panel" id="webinar-id">
                                                            <!-- START FORM CONTENT-->

                                                            <h3 class="form-section">Enter Onstream ID</h3>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <h4>If you already have an Onstream ID please
                                                                            enter it.</h4>
                                                                        <label class="control-label">Onstream ID
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <input type="input" class="form-control lms_required"
                                                                               name="onstreamid" id="onstreamid">
                                                                        <span class="help-block"> Please enter you onstream event ID if you have one. Otherwise please click the "Create Webinar" button below.</span>

                                                                        <a class="btn success blue" id="getonstreamid">Create
                                                                            Webinar</a>
                                                                        <span class="ajax_error"></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <div class="form-body eventtypes_panel" id="webcast-id"
                                                             style="display:none;">
                                                            <!-- START FORM CONTENT-->

                                                            <h3 class="form-section">Enter Webcast ID</h3>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <h4>If you already have an Webcast ID please
                                                                            enter it.</h4>
                                                                        <label class="control-label">Webcast ID
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <input type="input" class="form-control lms_required"
                                                                               name="webcastid" id="webcastid">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <div class="form-body eventtypes_panel" id="sp_upload-id"
                                                             style="display:none;">
                                                            <!-- START FORM CONTENT-->

                                                            <h3 class="form-section">Upload Video</h3>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group sp-video">
                                                                        <h4>Please Upload your Video here</h4>
                                                                        <button type="button" class="btn green btn-lg"
                                                                                id="sp_video_upload">Choose Upload
                                                                        </button>
                                                                        <br/><br/>
                                                                        <input type="hidden" name="sp_video_upload_file"
                                                                               id="sp_video_upload_file" value=""
                                                                               readonly>
                                                                        <div id="uploading-video" style="display:none;">
                                                                            <img src="<?php echo plugin_dir_url(__FILE__) . '../assets/global/img/loading-spinner-grey.gif'; ?>"
                                                                                 alt="" width="22" height="22"
                                                                                 class="loading">
                                                                            <span>Your Video is uploading...  This will disappear once completed and then you may continue your Event creation. </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <div class="form-body eventtypes_panel" id="other-id"
                                                             style="display:none;">
                                                            <!-- START FORM CONTENT-->

                                                            <h3 class="form-section">Enter Other Media URL</h3>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Media URL
                                                                            <span class="required" aria-required="true"> * </span>
                                                                        </label>
                                                                        <input type="input" class="form-control lms_required"
                                                                               name="otherid" id="otherid">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <div class="form-body eventtypes_panel" id="webex-id"
                                                             style="display:none;">
                                                            <!-- START FORM CONTENT-->
                                                            <div id="myProgress">
                                                                <div id="myBar"></div>
                                                            </div>
                                                            <span class="help-block"> Please set your Webex meeting passsword.</span>
                                                            <input class="form-control lms_required" type="password" name="webex_pass" maxlength="8">
                                                            <span class="webex_button" onclick="move()">Create meeting in webex</span>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <div class="form-body eventtypes_panel" id="livestream-id"
                                                             style="display:none;">
                                                            <!-- START FORM CONTENT-->
                                                            <h4>If you already have an Livestream ID please
                                                                enter it.</h4>
                                                            <label class="control-label">Livestream ID
                                                                <span class="required" aria-required="true"> * </span>
                                                            </label>
                                                            <input type="input" class="form-control lms_required"
                                                                   name="livestream_id" id="livestream_id">
                                                            <span class="help-block"> Please enter your Livestream ID if you have one.</span>
                                                            <a class="btn success blue" id="getlivestreamshortcode">Get
                                                                Shortcode</a>
                                                            <span class="livestreamshortcode"></span>
                                                            <input type="hidden" name="livestreamshortcode" value="" />
                                                            <span class="ajax_error"></span>
                                                            <!-- END FORM CONTENT-->
                                                        </div>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="publish">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="portlet light">
                                                    <div class="portlet-body form">
                                                        <!-- BEGIN FORM-->
                                                        <h3 class="form-section">Create Your Event!</h3>
                                                        <br/>
                                                        <h4>Choose a Template:</h4>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <a href="#"
                                                                   class="templatechoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                   onclick="return false;">
                                                                    <img class=""
                                                                         src="/wp-content/plugins/admin-pages/admin/assets/global/img/Blank.png"/>
                                                                </a>
                                                                <input type="radio" name="templatechoice" id="blank"
                                                                       value="blank" checked="" style="display:none;">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <a href="#"
                                                                   class="templatechoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                   onclick="return false;">
                                                                    <img class=""
                                                                         src="/wp-content/plugins/admin-pages/admin/assets/global/img/Simple-Registration.png"/>
                                                                </a>
                                                                <input type="radio" name="templatechoice"
                                                                       id="simpleregistration"
                                                                       value="simpleregistration" checked=""
                                                                       style="display:none;">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <a href="#"
                                                                   class="templatechoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                   onclick="return false;">
                                                                    <img class=""
                                                                         src="/wp-content/plugins/admin-pages/admin/assets/global/img/Webinar-Registration.png"/>
                                                                </a>
                                                                <input type="radio" name="templatechoice"
                                                                       id="webinarregistration"
                                                                       value="webinarregistration" checked=""
                                                                       style="display:none;">
                                                            </div>
                                                        </div>
                                                        <!-- BEGIN FORM-->
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <a href="#"
                                                                   class="templatechoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                   onclick="return false;">
                                                                    <img class=""
                                                                         src="/wp-content/plugins/admin-pages/admin/assets/global/img/Corporate-Compliance.png"/>
                                                                </a>
                                                                <input type="radio" name="templatechoice"
                                                                       id="corporatecompliance"
                                                                       value="corporatecompliance" checked=""
                                                                       style="display:none;">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <a href="#"
                                                                   class="templatechoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                   onclick="return false;">
                                                                    <img class=""
                                                                         src="/wp-content/plugins/admin-pages/admin/assets/global/img/Leadership-Single-Event.png"/>
                                                                </a>
                                                                <input type="radio" name="templatechoice"
                                                                       id="leadershipsingle" value="leadershipsingle"
                                                                       checked="" style="display:none;">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <a href="#"
                                                                   class="templatechoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                   onclick="return false;">
                                                                    <img class=""
                                                                         src="/wp-content/plugins/admin-pages/admin/assets/global/img/Multi-Event-Course.png"/>
                                                                </a>
                                                                <input type="radio" name="templatechoice"
                                                                       id="multievent" value="multievent" checked=""
                                                                       style="display:none;">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <a href="#"
                                                                   class="templatechoice btn btn-lg blue m-icon-big btn-block btn-outline"
                                                                   onclick="return false;">
                                                                    <img class=""
                                                                         src="/wp-content/plugins/admin-pages/admin/assets/global/img/One-Day-Conference.png"/>
                                                                </a>
                                                                <input type="radio" name="templatechoice"
                                                                       id="onedayconference" value="onedayconference"
                                                                       checked="" style="display:none;">
                                                            </div>
                                                        </div>
                                                        <!-- END FORM-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="col-md-2"></div>
                                <div class="col-md-8 m-grid-col-middle m-grid-col-center">
                                    <div class="row" style="text-align: center;">
                                        <a href="javascript:;" class="btn default button-previous disabled"
                                           style="display: none;">
                                            <i class="fa fa-angle-left"></i> Back</a>
                                        <a href="javascript:;" class="btn blue-steel button-next"> Continue
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <a href="javascript:;" class="btn green button-submit" style="display: none;">
                                            Create
                                            <i class="fa fa-check"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                </div>
                </form>

            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">

        jQuery(document).ready(function () {
           jQuery(document).on('click','#getlivestreamshortcode',function () {
               var code = jQuery('#livestream_id').val();
               if (code == '' || code == undefined){
                   jQuery('.ajax_error').text('Livestream ID can`t be empty');
                   jQuery('#livestream_id').css('border','1px solid red');
                   return;
               }
               jQuery('input[name="livestreamshortcode"]').val(code);
               jQuery('.livestreamshortcode').text('[video name="'+code+'"]');
               jQuery('.ajax_error').text('');
               jQuery('#livestream_id').css('border','1px solid #c2cad8');
           });
        });


        jQuery('#getonstreamid').click(function (e) {
            e.preventDefault(); // Prevent Default Submission
            jQuery.ajax({
                url: '<?php echo plugin_dir_url(__FILE__); ?>new-event-submit.php',
                type: 'POST',
                data: jQuery('#submit_form').serialize(), // it will serialize the form data
                dataType: 'html'
            })
                .done(function (data) {
                    var obj = jQuery.parseJSON(data);
                    if( obj.status && obj.status == 'failed' ){
                        jQuery('.ajax_error').text(obj.message);
                        return false;
                    }
                    jQuery('#onstreamid').val(obj.id);
                })
                .fail(function () {
                    alert('Ajax Submit Failed ...');
                });
        });
    </script>

    <script type="text/javascript">
        jQuery('.button-submit').click(function (e) {
            e.preventDefault(); // Prevent Default Submission
            $(".upload_video_section").show();
            jQuery.ajax({
                url: '<?php echo plugin_dir_url(__FILE__); ?>new-event-create_post.php',
                type: 'POST',
                data: jQuery('#submit_form').serialize(), // it will serialize the form data
                dataType: 'html'
            })
                .done(function (data) {
                    var obj = jQuery.parseJSON(data);
                    if( obj.status && obj.status == 'failed' ){
                        jQuery('.lms_alert').text('Please fill the following fields '+obj.message);
                        jQuery('.lms_alert').css('display','block');
                        $('html, body').animate({scrollTop:$('.lms_alert').position().top}, 'slow');
                        $(".upload_video_section").hide();
                        return false;
                    }
                    $(".upload_video_section").hide();
                    jQuery('#submit_form').append(obj.comment);
                    jQuery(location).attr('href', '<?php echo admin_url('admin.php?page=events'); ?>');

                })
                .fail(function () {
                    $(".upload_video_section").hide();
                    alert('Ajax Submit Failed ...');
                });
        });
    </script>
    <script type="text/javascript">
        jQuery('#featured_image_upload').click(function (e) {
            open_media_uploader_image();
        });
        var media_uploader = null;

        function open_media_uploader_image() {
            media_uploader = wp.media({
                frame: "post",
                state: "insert",
                multiple: false
            });

            media_uploader.on("insert", function () {
                var json = media_uploader.state().get("selection").first().toJSON();
                var image_url = json.url;
                var image_caption = json.caption;
                var image_title = json.title;
                var image_id = json.id;
                jQuery('#featured_image_id').val(image_id);
                jQuery('#featured_image_thumb').attr("src", image_url).show();
            });

            media_uploader.open();
        }
    </script>
    <script type="text/javascript">
        jQuery('#sp_video_upload').click(function (e) {
            open_media_uploader_video();
        });
        var media_uploader = null;

        function open_media_uploader_video() {
            media_uploader = wp.media({
                frame: "post",
                state: "insert",
                multiple: false
            });

            media_uploader.on("insert", function () {
                var json = media_uploader.state().get("selection").first().toJSON();
                var video_url = json.url;
                var video_title = json.title;
                var video_id = json.id;
                jQuery('#uploading-video').show();
                jQuery.ajax({
                    url: '<?php echo plugin_dir_url(__FILE__); ?>new-sp-event-submit.php',
                    type: 'POST',
                    data: {video_url: video_url, video_title: video_title, video_id: video_id}
                })
                    .done(function (data) {
                        var obj = jQuery.parseJSON(data);
                        console.log(obj.sourcekeyfile);
                        jQuery('#sp_video_upload_file').val(obj.sourcekeyfile);
                        jQuery('#uploading-video').hide();
                        jQuery('.form-group.sp-video').append("<h4>Video Uploaded! Please continue..</h4>");
                    })
                    .fail(function () {
                        console.log('Ajax Submit Failed ...');
                    });
            });

            media_uploader.open();
        }
    </script>
    <script>
        jQuery(function ($) {

            var $items = jQuery('span.eventitem');
            jQuery("a.eventchoice").click(function ($) {
                $items.hide();
                jQuery("a.eventchoice").removeClass("active");
                jQuery("a.eventchoice").next().prop("checked", false);
                jQuery('div.form-body.eventtypes_panel').hide();
                jQuery(this).addClass("active").next().prop("checked", "checked");
                var id = jQuery(this).next().attr('id');

                jQuery('.eventtypes_panel').each(function () {
                    var parent = jQuery(this).attr('id');
                    if ( parent != id + '-id'){
                        jQuery('#'+parent+' input').removeClass('lms_required');
                    }
                    else{
                        jQuery('#'+parent+' input').addClass('lms_required');
                    }

                });

                jQuery('span.eventitem#' + id).show();
                jQuery('div.form-body#' + id + '-id').show();
            });
        });
    </script>
    <script type="text/javascript">
        jQuery("a.templatechoice").click(function ($) {
            jQuery("a.templatechoice").removeClass("active");
            jQuery("a.templatechoice").next().prop("checked", false);
            jQuery(this).addClass("active").next().prop("checked", "checked");
        });
        function move() {
            var elem = document.getElementById("myBar");
            var width = 1;
            var id = setInterval(frame, 40);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                    jQuery('#myBar').css('background','#36c6d3');
                } else {
                    width++;
                    elem.style.width = width + '%';
                }
            }
        }
    </script>
    <style type="text/css">
        .livestreamshortcode {
            display: block;
            margin-top: 20px;
            font-weight: bold;
        }
        form .row img.loading {
            width: auto;
        }

        div#uploading-video {
            display: none;
        }
        .ajax_error{
            display: block;
            font-size: 18px;
            color: red;
            margin-top: 30px;
        }
        .lms_alert{display: none;}
        .admin_page_newevent .upload_video_section{
            position: absolute;
            width: 100%;
            height: 100%;
            background-color:rgba(255, 255, 255, 0.3);
            z-index: 9;
        }
        .admin_page_newevent .upload_video_section .spinner {
            top: 70%;}
        #myProgress {
            width: 100%;
            background-color: #ddd;
            border-radius: 10px;
        }

        #myBar {
            width: 1%;
            height: 30px;
            background-color: #659be0;
            border-radius: 10px;
        }
        .webex_button {
            display: inline-block;
            margin-top: 15px;
            cursor: pointer;
            border: 1px solid;
            padding: 10px;
            border-radius: 5px;
            background: #4B77BE;
            color: #fff;
        }
    </style>
<?php }
