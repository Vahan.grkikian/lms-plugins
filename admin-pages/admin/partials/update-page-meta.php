<?php

require_once(dirname(__FILE__).'/../../../../../wp-config.php');

//Global WordPress
global $wpdb;

		//variables submitted 
		$featured_image_id = $_POST['featured_image_id'];
        $id = $_POST['postid'];
        $postID = $_POST['postid'];
        $post_title = $_POST['post_title'];
        $post_name = $_POST['post_name'];
        $PostVisibility = $_POST['publish-visibility'];
        $d = $_POST['start_date'];
//        $PostDate = $_POST['publish-date'];
//        $PostHour = $_POST['publish-hour'];
//        $PostMin = $_POST['publish-min'];
//        $d = "$PostDate $PostHour:$PostMin:00";
        $date = date_create($d);
        $PostFullDate = date_format($date, 'Y-m-d H:i:s');
        $author_id = $_POST['post_author'];
        $post_type = $_POST['post_type'];
        $post_password = $_POST['post_password'];
        $post_parent = $_POST['post_parent'];
        $menu_order = $_POST['menu_order'];
        $page_template = $_POST['page_template'];
        $comment_status = $_POST['comment_status'];
        $post301 = $_POST['redirect'];
        $sitemap_include = $_POST['sitemap_include'];
        $yoast_wpseo_opengraph_title = $_POST['yoast_wpseo_opengraph-title'];
        $yoast_wpseo_opengraph_description = $_POST['yoast_wpseo_opengraph-description'];
        $yoast_wpseo_opengraph_image = $_POST['yoast_wpseo_opengraph-image'];
        $yoast_wpseo_twitter_title = $_POST['yoast_wpseo_twitter-title'];
        $yoast_wpseo_twitter_description = $_POST['yoast_wpseo_twitter-description'];
        $yoast_wpseo_twitter_image = $_POST['yoast_wpseo_twitter-image'];
        $sitemap_priority = $_POST['sitemap_priority'];
        $_rpwcm_post_restriction_method = $_POST['_rpwcm_post_restriction_method'];
        $_rpwcm_only_caps = $_POST['_rpwcm_only_caps'];

        //$category_selection = $_POST['post_category'];
       // $category_selections = implode(',', $category_selection);

        $seotitle = $_POST['seotitle'];
        $metadesc = $_POST['metadesc'];
        //$metakeywords = $_POST['metakeywords'];
        $metavalidation = $_POST['validation_select'];
        $metafollow = $_POST['metafollow'];
        $metasettings = implode(", ", $_POST['yoast_wpseo_meta-robots-adv']);
        $metacanonical = $_POST['yoast_wpseo_canonical'];

            $post = array();
            $post['ID'] = $postID;
            $post['post_type'] = $post_type;
            $post['post_title'] = $post_title;
            $post['post_author'] = $author_id;
            $post['post_name'] = $post_name;
            $post['menu_order'] = $menu_order;
            $post['post_parent'] = $post_parent;
            $post['post_date'] = $PostFullDate;
            $post['comment_status'] = $comment_status;
            $post['post_status'] = $PostVisibility;

            switch(true) {
    case $_POST['publish-visibility'] = 'public':
      $post['post_password'] = $post_password;
    break;
    case $_POST['publish-visibility'] = 'draft':
      unset( $post['sticky'] );
    break;
    case $_POST['publish-visibility'] = 'password':
      unset( $post['sticky'] );
    break;
    case $_POST['publish-visibility'] = 'private' :
        $post['post_password'] = $post_password;
        unset( $post['sticky'] );
    break;
}


            // Insert the post into the database
            $result = wp_update_post( $post, $wp_error );

        update_post_meta($result, '_thumbnail_id', $featured_image_id);
        update_post_meta($result, '_wp_page_template', $page_template);
        update_post_meta($result, '_yoast_wpseo_title', $seotitle);
        update_post_meta($result, '_yoast_wpseo_metadesc', $metadesc);
        //update_post_meta($result, '_yoast_wpseo_metakeywords', $metakeywords);
        update_post_meta($result, '_yoast_wpseo_meta-robots-noindex', $metavalidation);
        update_post_meta($result, '_yoast_wpseo_meta-robots-nofollow', $metafollow);
        update_post_meta($result, '_yoast_wpseo_meta-robots-adv', $metasettings);
        update_post_meta($result, '_yoast_wpseo_canonical', $metacanonical);
        update_post_meta($result, '_yoast_wpseo_opengraph-title', $yoast_wpseo_opengraph_title);
        update_post_meta($result, '_yoast_wpseo_opengraph-description', $yoast_wpseo_opengraph_description);
        update_post_meta($result, '_yoast_wpseo_opengraph-image', $yoast_wpseo_opengraph_image);
        update_post_meta($result, '_yoast_wpseo_twitter-title', $yoast_wpseo_twitter_title);
        update_post_meta($result, '_yoast_wpseo_twitter-descriptio', $yoast_wpseo_twitter_description);
        update_post_meta($result, '_yoast_wpseo_twitter-image', $yoast_wpseo_twitter_image);
        update_post_meta($result, '_rpwcm_post_restriction_method', $_rpwcm_post_restriction_method);
        foreach ( $_rpwcm_only_caps as $_rpwcm_only_cap ){
            add_post_meta($result, '_rpwcm_only_caps', $_rpwcm_only_cap);
        }

            //wp_set_post_terms( $result, $category_selection, 'category', false);


        $error = array("response" => "error", "comment" => "Event Not Created");
        $success = array("response" => "success", "comment" => "Event Created");

		if(is_wp_error($result)) {
		echo json_encode($error);
		} else {
		echo json_encode($success);
		}
		//RebuildLandingList($catgroup);
