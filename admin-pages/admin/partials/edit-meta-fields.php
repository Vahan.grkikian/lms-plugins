<?php /** *************************** RENDER TEST PAGE ******************************** ******************************************************************************* * This function renders the admin page and the example list table. Although it 's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and * it 's the way the list tables are used in the WordPress core.
 */
 require_once(dirname(__FILE__).'/../../../../../wp-config.php');

?>
<div class="modal-body">
    <div id="quick-edit" class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 400px;">
        <div class="scroller" style="height: 400px; overflow-y: scroll; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
            <form id="submit_form">
                <table>
                    <tr class="inline-edit-row inline-edit-row-page inline-edit-page quick-edit-row quick-edit-row-page inline-edit-page inline-editor" style="">
                        <td colspan="10" class="colspanchange">
                            <legend class="inline-edit-legend">Quick Edit</legend>
                            <?php
                            $postdata = get_post($_GET['post_id']);
                            $post_id=$_GET['post_id']; //print_r($postdata);
                            $method = get_post_meta($post_id,'_rpwcm_post_restriction_method')[0];
                            $selected = get_post_meta($post_id,'_rpwcm_only_caps')[0];
                            if ( !is_array($selected) ){
                                $selected = array($selected);
                            }

                            ?>
                            <input type="hidden" name="postid" id="postid" value="<?php echo $post_id; ?>">
                            <input type="hidden" name="post_type" id="post_type" value="<?php echo $postdata->post_type; ?>" />
                            <fieldset class="inline-edit-col-left">
                                <div class="inline-edit-col">

                                    <label>
                                        <span class="title">Title</span>
                                        <span class="input-text-wrap">
                                            <input type="text" name="post_title" class="ptitle" value="<?php echo $postdata->post_title; ?>">
                                        </span>
                                    </label>

                                    <label class="inline-edit-rpwcm-quick">
                                        <span class="title"><?php _e('Access Restriction Method', 'woocommerce-membership'); ?></span>
                                        <select id="_rpwcm_post_restriction_method_quick" name="_rpwcm_post_restriction_method" class="rpwcm_post_restriction_method_quick">
                                            <optgroup label="<?php _e('No Restriction', 'woocommerce-membership'); ?>">
                                                <option value="none" <?php echo ($method == 'none' ? 'selected="selected"' : ''); ?>><?php _e('No Restriction', 'woocommerce-membership'); ?></option>
                                            </optgroup>
                                            <optgroup label="<?php _e('Members Only', 'woocommerce-membership'); ?>">
                                                <option value="all_members" <?php echo ($method == 'all_members' ? 'selected="selected"' : ''); ?>><?php _e('All Members', 'woocommerce-membership'); ?></option>
                                                <option value="members_with_plans" <?php echo ($method == 'members_with_plans' ? 'selected="selected"' : ''); ?>><?php _e('Members With Specific Plans', 'woocommerce-membership'); ?></option>
                                            </optgroup>
                                            <optgroup label="<?php _e('Non-Members Only', 'woocommerce-membership'); ?>">
                                                <option value="non_members" <?php echo ($method == 'non_members' ? 'selected="selected"' : ''); ?>><?php _e('All Non-Members', 'woocommerce-membership'); ?></option>
                                                <option value="users_without_plans" <?php echo ($method == 'users_without_plans' ? 'selected="selected"' : ''); ?>><?php _e('Users Without Specific Plans', 'woocommerce-membership'); ?></option>
                                            </optgroup>
                                        </select>
                                    </label>
                                    <?php if ( $method == 'members_with_plans' || $method == 'users_without_plans' ): ?>
                                    <div class="rpwcm_show_if_restrict_access_by_plan" style="display: block;">
                                    <?php else: ?>
                                        <div class="rpwcm_show_if_restrict_access_by_plan" style="display: none;">
                                    <?php endif; ?>
                                        <div class="rpwcm_post_membership_field">
                                            <?php WooCommerce_Membership::render_field_multiselect(array('name' => '_rpwcm_only_caps', 'class' => 'rpwcm_only_plans', 'values' => WooCommerce_Membership_Plan::get_list_of_all_plan_keys(), 'selected' => $selected)); ?>
                                        </div>
                                        <p style="margin-bottom: 0;">
                                            <?php printf(__('Control your membership plans %shere%s.', 'woocommerce-membership'), '<a href="' . admin_url('edit.php?post_type=membership_plan') . '">', '</a>'); ?>
                                        </p>
                                    </div>

                                    <label>
                                        <span class="title">Slug</span>
                                        <span class="input-text-wrap">
                                            <input type="text" name="post_name" value="<?php echo $postdata->post_name; ?>">
                                        </span>
                                    </label>


                                    <label class="control-label">
                                        <span class="title">Date</span>
                                        <span class="input-text-wrap">
                                            <div class="input-icon">
                                                <i class="fa fa-calendar-o"></i> 
                                                <input type="text" size="18" class="form-control form-control-inline date-picker" name="start_date" value="<?php echo $postdata->post_date; ?>">
                                        </span>
                                        </div>
                                    </label>

                                    <label class="inline-edit-author">
                                        <span class="title">Author</span>
                                        <span class="input-text-wrap">
                                            <select class="form-control" name="post_author" class="authors">
                                                <option value="">Select</option>
                                                <?php $args2=array( 'role__in '=>['author', 'editor', 'administrator'], 'orderby ' => 'user_nicename ', 'order ' => 'ASC ' ); $authors = get_users($args2); foreach ($authors as $user) { echo '
                                                <option value="'.$user->id .'" '.selected( $postdata->post_author, $user->id ).'>' . $user->display_name.' ['.$user->user_email . ']</option>'; } ?>
                                            </select>
                                        </span>
                                    </label>

                                    <div class="inline-edit-group wp-clearfix">
                                        <label class="inline-edit-status alignleft" style="width: 100%;">
                                            <span class="title" style="width: 50%;">Featured Image</span>
                                            <button type="button" class="btn blue-steel right" id="featured_image_upload">Upload Image</button>
                                        </label>
                                        <br />
                                        <br />
                                        <?php $image=wp_get_attachment_image_url( get_post_thumbnail_id( $post_id ), 'thumbnail' ); ?>
                                        <?php if (!empty($image)) { echo '<img src="'.$image. '" width="125px" height="125px" id="featured_image_thumb" style="max-width:125px;"/>'; } else { echo '<img src="" width="125px" height="125px" id="featured_image_thumb" style="display:none;max-width:125px;"/>'; } ?>
                                        <br>
                                        <br>
                                        <input type="hidden" name="featured_image_id" id="featured_image_id" value="<?php echo get_post_thumbnail_id( $post_id ); ?>" readonly>
                                    </div>
                                </div>
                            </fieldset>


                            <fieldset class="inline-edit-col-right">
                                <div class="inline-edit-col">

                                    <label>
                                        <span class="title">Parent</span>
                                        <?php $dropdown_args=array( 'post_type'=>$postdata->post_type, 'exclude_tree' => $postdata->ID, 'selected' => $postdata->post_parent, 'name' => 'post_parent', 'show_option_none' => __('(Main Page no parent)'), 'sort_column' => 'menu_order, post_title', 'echo' => 0, ); $pages = wp_dropdown_pages( $dropdown_args ); echo $pages; ?>
                                    </label>


                                    <label>
                                        <span class="title">Order</span>
                                        <span class="input-text-wrap">
                                            <input type="text" name="menu_order" class="inline-edit-menu-order-input" value="<?php echo $postdata->menu_order; ?>">
                                        </span>
                                    </label>


                                    <label>
                                        <span class="title">Template</span>
                                        <select name="page_template">
                                            <?php $template=get_page_template_slug( $postdata->ID ); ?>
                                            <option value="default" <?php selected( $template, 'default' ); ?>>Default Template</option>
                                            <option value="page-boxed.php" <?php selected( $template, 'page-boxed.php' ); ?>>Boxed Template</option>
                                            <option value="front-page.php" <?php selected( $template, 'front-page.php' ); ?>>Front Page Template</option>
                                            <option value="page-fullcalendar.php" <?php selected( $template, 'page-fullcalendar.php' ); ?>>Full Calendar Template</option>
                                            <option value="page-fullwidth.php" <?php selected( $template, 'page-fullwidth.php' ); ?>>Full Width Content</option>
                                            <option value="page-no-buddypanel.php" <?php selected( $template, 'page-no-buddypanel.php' ); ?>>No BuddyPanel</option>
                                            <option value="page-right-sidebar.php" <?php selected( $template, 'page-right-sidebar.php' ); ?>>Right Sidebar</option>
                                        </select>
                                    </label>

                                    <div class="inline-edit-group wp-clearfix">
                                        <label class="alignleft">
                                            <input type="checkbox" name="comment_status" value="open" <?php checked( $postdata->comment_status, 'open' ); ?>>
                                            <span class="checkbox-title">Allow Comments</span>
                                        </label>
                                    </div>


                                    <div class="inline-edit-group wp-clearfix">
                                        <label class="inline-edit-status alignleft">
                                            <span class="title">Status</span>
                                            <select name="publish-visibility">
                                                <option value="publish" <?php selected( $postdata->post_status, 'publish' ); ?>>Published</option>

                                                <option value="pending" <?php selected( $postdata->post_status, 'pending' ); ?>>Pending Review</option>
                                                <option value="draft" <?php selected( $postdata->post_status, 'draft' ); ?>>Draft</option>
                                                <option value="private" <?php selected( $postdata->post_status, 'private' ); ?>>Private</option>
                                            </select>
                                        </label>


                                    </div>
                                    <div class="inline-edit-group wp-clearfix">
                                        <label class="alignleft">
                                            <span class="title">Password</span>
                                            <span class="input-text-wrap">
                                                <input type="password" name="post_password" class="inline-edit-password-input" value="<?php echo $postdata->post_password; ?>">
                                            </span>
                                        </label>
                                    </div>

                                </div>
        </div>
        </fieldset>
        </td>
        </tr>

        <tr class="inline-edit-row inline-edit-row-page inline-edit-page quick-edit-row quick-edit-row-page inline-edit-page inline-editor" style="">
            <td colspan="10" class="colspanchange">
                <fieldset class="inline-edit">
                    <legend class="inline-edit-legend">SEO Settings</legend>
                    <div class="inline-edit-group wp-clearfix">
                        <label class="alignleft">
                            <span class="title">SEO title</span>
                            <span class="input-text-wrap">
                                <input type="text" class="input-large" id="snippet-editor-title" name="seotitle" value="<?php echo get_post_meta( $post_id, '_yoast_wpseo_title', true ); ?>" placeholder="%%title%% %%page%% %%sep%% %%sitename%%">
                            </span>
                        </label>
                    </div>
                    <br class="clear" />
                    <div class="inline-edit-group wp-clearfix">
                        <label class="alignleft">
                            <span class="title">Meta description</span>
                            <span class="input-text-wrap">
                                <textarea class="input-large" id="snippet-editor-meta-description" name="metadesc" placeholder="Modify your meta description by editing it right here">
                                    <?php echo get_post_meta( $post_id, '_yoast_wpseo_metadesc', true ); ?>
                                </textarea>
                            </span>
                        </label>
                    </div>

                    <div id="wpseo_facebook" class="wpseotab facebook active">
                        <label for="yoast_wpseo_opengraph-title"><span class="title">Facebook Title</span>
                        <span class="input-text-wrap"><input type="text" id="yoast_wpseo_opengraph-title" name="yoast_wpseo_opengraph-title" value="<?php echo get_post_meta( $post_id, '_yoast_wpseo_opengraph-title', true ); ?>" class="large-text" aria-describedby="yoast_wpseo_opengraph-title-desc"></span>
                        <span class="help-block">If you don't want to use the post title for sharing the post on Facebook but instead want another title there, write it here.</span></label>
                        <label for="yoast_wpseo_opengraph-description"><span class="title">Facebook Description</span>
                        <span class="input-text-wrap"><textarea class="large-text" rows="3" id="yoast_wpseo_opengraph-description" name="yoast_wpseo_opengraph-description" aria-describedby="yoast_wpseo_opengraph-description-desc"><?php echo get_post_meta( $post_id, '_yoast_wpseo_opengraph-description', true ); ?></textarea></span>
                        <span class="help-block">If you don't want to use the meta description for sharing the post on Facebook but want another description there, write it here.</span></label>
                        <label for="yoast_wpseo_opengraph-image"><span class="title">Facebook Image</span></label>
                        <input id="yoast_wpseo_opengraph-image" type="text" size="36" class="" name="yoast_wpseo_opengraph-image" value="<?php echo get_post_meta( $post_id, '_yoast_wpseo_opengraph-image', true ); ?>" aria-describedby="yoast_wpseo_opengraph-image-desc">
                        <input id="yoast_wpseo_opengraph-image_button" class="btn blue-steel right" type="button" value="Upload Image">
                        <span class="help-block">If you want to override the image used on Facebook for this post, upload / choose an image or add the URL here. The recommended image size for Facebook is 1200 by 630 pixels.</span>
                    </div>

                    <div id="wpseo_twitter" class="wpseotab twitter active">
                        <label for="yoast_wpseo_twitter-title"><span class="title">Twitter Title</span>
                        <span class="input-text-wrap"><input type="text" id="yoast_wpseo_twitter-title" name="yoast_wpseo_twitter-title" value="<?php echo get_post_meta( $post_id, '_yoast_wpseo_twitter-title', true ); ?>" class="large-text" aria-describedby="yoast_wpseo_twitter-title-desc"></span>
                        <span class="help-block">If you don't want to use the post title for sharing the post on Twitter but instead want another title there, write it here.</span></label>
                        <label for="yoast_wpseo_twitter-description"><span class="title">Twitter Description</span>
                        <span class="input-text-wrap"><textarea class="large-text" rows="3" id="yoast_wpseo_twitter-description" name="yoast_wpseo_twitter-description" aria-describedby="yoast_wpseo_twitter-description-desc"><?php echo get_post_meta( $post_id, '_yoast_wpseo_twitter-description', true ); ?></textarea></span>
                        <span class="help-block">If you don't want to use the meta description for sharing the post on Twitter but want another description there, write it here.</span></label>
                        <label for="yoast_wpseo_twitter-image"><span class="title">Twitter Image</span></label>
                        <input id="yoast_wpseo_twitter-image" type="text" size="36" class="" name="yoast_wpseo_twitter-image" value="<?php echo get_post_meta( $post_id, '_yoast_wpseo_twitter-image', true ); ?>" aria-describedby="yoast_wpseo_twitter-image-desc">
                        <input id="yoast_wpseo_twitter-image_button" class="btn blue-steel right" type="button" value="Upload Image">
                        <span class="help-block">If you want to override the image used on Twitter for this post, upload / choose an image or add the URL here. The recommended image size for Twitter is 1024 by 512 pixels.</span>
                    </div>

                    <div id="wpseo_advanced" class="wpseotab advanced active">
                        <label for="yoast_wpseo_meta-robots-noindex"><span class="title">Meta robots index</span></label>
                        <span class="input-text-wrap">
                            <select name="yoast_wpseo_meta-robots-noindex" id="yoast_wpseo_meta-robots-noindex" class="select">
                                <?php $noindex=get_post_meta( $post_id, '_yoast_wpseo_meta-robots-noindex', true ); ?>
                                <option <?php selected( $noindex, '0' ); ?> value="0">Default for this post type, currently: index</option>
                                <option <?php selected( $noindex, '2' ); ?>value="2">index</option>
                                <option <?php selected( $noindex, '1' ); ?>value="1">noindex</option>
                            </select>
                        </span>
                        <fieldset>
                            <div class="mt-radio-inline">
                            <legend><span class="title">Meta robots follow</span></legend>
                            <?php $nofollow=get_post_meta( $post_id, '_yoast_wpseo_meta-robots-nofollow', true ); ?>
                            <label class="mt-radio">
                            <input type="radio" checked id="yoast_wpseo_meta-robots-nofollow_0" name="yoast_wpseo_meta-robots-nofollow" value="0"> follow
                            <span></span>
                            </label>
                            <label class="mt-radio">
                            <input type="radio" <?php checked( $nofollow, '1' ); ?> id="yoast_wpseo_meta-robots-nofollow_1" name="yoast_wpseo_meta-robots-nofollow" value="1"> nofollow
                        <span></span>
                    </label>
                        </fieldset>
                        <label for="yoast_wpseo_meta-robots-adv"><span class="title">Meta robots advanced</span></label>
                        <?php $adv=get_post_meta( $post_id, '_yoast_wpseo_meta-robots-adv', true ); print_r($adv); $adv1 = explode(',',$adv); ?>
                        <select multiple name="yoast_wpseo_meta-robots-adv[]" id="yoast_wpseo_meta-robots-adv" class="">
                            <option value="" <?php if (empty($adv)) { echo 'selected="selected"'; } ?>>Site-wide default: None</option>
                            <option <?php if (in_array("none", $adv1)) { echo 'selected="selected"'; } ?> value="none">None</option>
                            <option <?php if (in_array("noimageindex", $adv1)) { echo 'selected="selected"'; }  ?> value="noimageindex">No Image Index</option>
                            <option <?php if (in_array("noarchive", $adv1)) { echo 'selected="selected"'; }  ?> value="noarchive">No Archive</option>
                            <option <?php if (in_array("nosnippet", $adv1)) { echo 'selected="selected"'; }  ?> value="nosnippet">No Snippet</option>
                        </select>
                        <p id="yoast_wpseo_meta-robots-adv-desc" class="yoast-metabox__description"><span class="title">Advanced</span>
                            <code>meta</code>robots settings for this page.</p>
                        <label for="yoast_wpseo_canonical">Canonical URL</label>
                        <span class="input-text-wrap"><input type="text" id="yoast_wpseo_canonical" name="yoast_wpseo_canonical" value="<?php echo get_post_meta( $post_id, '_yoast_wpseo_canonical', true ); ?>" class="large-text" aria-describedby="yoast_wpseo_canonical-desc"></span>
                        <span class="help-block">The canonical URL that this page should point to, leave empty to default to permalink. <a target="_blank" href="http://googlewebmastercentral.blogspot.com/2009/12/handling-legitimate-cross-domain.html">Cross domain canonical</a> supported too.</span>
                    </div>
                </fieldset>
            </td>
        </tr>

        </table>
    </div>

</div>

<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn red btn-outline dark">Close</button>
    <button type="button" class="btn green button-submit">Save changes</button>
</div>
</form>
</div>

<script type="text/javascript">
jQuery('.button-submit').click(function(e) {
    e.preventDefault(); // Prevent Default Submission
    jQuery.ajax({
            url: '<?php echo plugin_dir_url( __FILE__ ); ?>update-page-meta.php',
            type: 'POST',
            data: jQuery('#submit_form').serialize(), // it will serialize the form data
            dataType: 'html'
        })
        .done(function(data) {
            var obj = jQuery.parseJSON(data);
            jQuery('#submit_form').append('<div class="alert alert-info fade in">' +
                'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '</div>');
        })
        .fail(function() {
            alert('Ajax Submit Failed ...');
        });
});
</script>
<script type="text/javascript">
jQuery('#featured_image_upload').click(function(e) {
    open_media_uploader_image();
});
var media_uploader = null;

function open_media_uploader_image() {
    media_uploader = wp.media({
        frame: "post",
        state: "insert",
        multiple: false
    });

    media_uploader.on("insert", function() {
        var json = media_uploader.state().get("selection").first().toJSON();
        var image_url = json.url;
        var image_caption = json.caption;
        var image_title = json.title;
        var image_id = json.id;
        jQuery('#featured_image_id').val(image_id);
        jQuery('#featured_image_thumb').attr("src", image_url).show();
    });

    media_uploader.open();
}
</script>
<script type="text/javascript">
jQuery('#yoast_wpseo_twitter-image_button').click(function(e) {
    open_twitter_media_uploader_image();
});
var media_uploader = null;

function open_twitter_media_uploader_image() {
    media_uploader = wp.media({
        frame: "post",
        state: "insert",
        multiple: false
    });

    media_uploader.on("insert", function() {
        var json = media_uploader.state().get("selection").first().toJSON();
        var image_url = json.url;
        var image_caption = json.caption;
        var image_title = json.title;
        var image_id = json.id;
        jQuery('#yoast_wpseo_twitter-image').val(image_url);
    });

    media_uploader.open();
}
</script>
<script type="text/javascript">
jQuery('#yoast_wpseo_opengraph-image_button').click(function(e) {
    open_opengraph_media_uploader_image();
});
var media_uploader = null;

function open_opengraph_media_uploader_image() {
    media_uploader = wp.media({
        frame: "post",
        state: "insert",
        multiple: false
    });

    media_uploader.on("insert", function() {
        var json = media_uploader.state().get("selection").first().toJSON();
        var image_url = json.url;
        var image_caption = json.caption;
        var image_title = json.title;
        var image_id = json.id;
        jQuery('#yoast_wpseo_opengraph-image').val(image_url);
    });

    media_uploader.open();
}
</script>
