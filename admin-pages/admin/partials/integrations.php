<?php

/** *************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function tt_render_integrations_page()
{

    ?>
    <style>
        .cbp-caption-defaultWrap img {
            width: 100%;
        }

        .cbp-l-caption-body {
            text-align: center;
            margin: 10px 0;
        }
    </style>
    <div class="integrations">
        <div class="row widget-row">

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="admin.php?page=plugin-options.php" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/3-01.png" alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="admin.php?page=plugin-options.php" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                                       rel="nofollow">configure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="admin.php?page=visualwebcaster-options.php" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/visual-webcaster.jpg"
                                     alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="admin.php?page=visualwebcaster-options.php" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                                       rel="nofollow">configure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="admin.php?page=streamingpublisher-options.php" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/streamingpublisher-integration.jpg"
                                     alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="admin.php?page=streamingpublisher-options.php" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                                       rel="nofollow">configure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="#" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/2-01-01.png"
                                     alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="#" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                                       rel="nofollow">coming soon!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <div class="row widget-row">
            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="#" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/1-01.png" alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="#" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                                       rel="nofollow">coming soon!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->

            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="#" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/5-01.png" alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="#" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                                       rel="nofollow">coming soon!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="options-general.php?page=postman%2Fconfiguration" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/6-01.png" alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="options-general.php?page=postman%2Fconfiguration" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                                       rel="nofollow">configure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="#" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/7-01.png" alt=""></a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="#" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase disabled"
                                       rel="nofollow">coming soon!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="admin.php?page=gadash_settings" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/4-01.png" alt="">
                            </a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="admin.php?page=gadash_settings" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                                       rel="nofollow">configure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="admin.php?page=wp-gotowebinar" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                               rel="nofollow">
                                <img src="/wp-content/plugins/admin-pages/admin/assets/global/img/lms_default.jpg" alt="">
                            </a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="admin.php?page=wp-gotowebinar" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                                       rel="nofollow">configure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row widget-row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                <div class="cbp-item graphic">
                    <div class="cbp-caption">
                        <div class="cbp-caption-defaultWrap">
                            <a href="admin.php?page=webex-settings" target="_blank"
                               class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                               rel="nofollow">
                                <img src="/wp-content/plugins/lms-helper/admin/assets/images/webex.png" style="padding: 40px;" alt="">
                            </a>
                        </div>
                        <div class="cbp-caption-activeWrap">
                            <div class="cbp-l-caption-alignCenter">
                                <div class="cbp-l-caption-body">
                                    <a href="admin.php?page=webex-settings" target="_blank"
                                       class="cbp-singlePage cbp-l-caption-buttonLeft btn uppercase btn grey uppercase"
                                       rel="nofollow">configure</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
