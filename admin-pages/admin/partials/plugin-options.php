<?php

/** *************************** RENDER OPTIONS PAGE ********************************
 *******************************************************************************
 * This function renders the options page 
 */

    $themename = "Encompass LMS";
    $shortname = "els";

    /* functions to andale the options array  */

function create_form($options) { 
    $user_id = get_current_user_id();
    $els_onstream_account = get_user_meta($user_id, 'els_onstream_account', true);
    $els_onstream_password = get_user_meta($user_id, 'els_onstream_password', true);
?>

<form id="options_form" method="post" name="form">
<img src="/wp-content/plugins/admin-pages/admin/assets/global/img/Webinar-Tile.png" width="264" height="83" border="0">
<p class="font-blue-madison">Enter Account Information below</p>
<div class="suf-section fix">
<input type="text" id="els_onstream_account" placeholder="enter your username" name="els_onstream_account" value="<?php echo $els_onstream_account; ?>">
</div>
<div class="suf-section fix">
<input type="password" id="els_onstream_password" placeholder="enter your password" name="els_onstream_password" value="<?php echo $els_onstream_password; ?>">
</div>
 
        <input name="save" type="button" value="Save" onclick="submit_form(this, document.forms['form'])" class="button edit_design btn dark btn-md sbold uppercase green-steel">
        <input type="hidden" name="formaction" value="default">
    
     <script> function submit_form(element, form){ 
                 form['formaction'].value = element.name;
                 form.submit();
             } </script>
    
        </form>
<?php }

    $options = array( 
        array("name" => "Onstream Account",
                "desc" => " ",
                "id" => $shortname."_onstream_account",
                "type" => "text",
                "parent" => "onstream",
                "std" => ""),
        array("name" => "Onstream Password",
                "desc" => " ",
                "id" => $shortname."_onstream_password",
                "type" => "text",
                "parent" => "onstream",
                "std" => ""),
      );


add_action('admin_menu', 'mynewtheme_add_admin');   
function mynewtheme_add_admin() { 
    global $themename, $shortname, $options;
     $user_id = get_current_user_id();

    if ( $_GET['page'] == basename(__FILE__) ) {
        if ( 'save' == $_POST['formaction'] ) {
             update_user_meta($user_id, 'els_onstream_account', $_REQUEST['els_onstream_account']);
             update_user_meta($user_id, 'els_onstream_password', $_REQUEST['els_onstream_password']);
             header("Location: admin.php?page=plugin-options.php&saved=true");
            die;
        }
  }

}

function mynewtheme_admin() { 
    global $themename, $shortname, $options, $theme_name;
 
    if ($_REQUEST['saved']) {
        echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved for this page.</strong></p></div>';
    }
?>
<div class="wrap">
<style>
    h3.suf-header-3, .suf-section.fix h3 {
        font-size: 20px;
        font-weight: 400;
        margin: 20px 0 0;
        line-height: 29px;
        float: left;
        padding: 0;
        width: 100%;
    }        
    .suf-section.fix {
        margin-bottom: 20px;
    }
</style>
<div class="mnt-options">
<?php
    create_form($options);
?>
    </div><!-- mnt-options -->
</div><!-- wrap -->
<?php } // end function mynewtheme_admin()
?>
