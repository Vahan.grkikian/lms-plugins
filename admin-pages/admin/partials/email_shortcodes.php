<?php

/** *************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */

if ( function_exists( 'mailster_add_tag' ) ) {
  function authorfirstname_function($option, $fallback, $campaignID, $subscriberID){
    $post_author_id = get_post_field( 'post_author', $campaignID );
    $author_id = the_author_meta('first_name', '14');
    return $author_id;
  }
 mailster_add_tag('user_first_name', 'authorfirstname_function');
}
