<?php

/** *************************** RENDER OPTIONS PAGE ********************************
 *******************************************************************************
 * This function renders the options page 
 */

    $themename = "Encompass LMS";
    $shortname = "els";

    /* functions to andale the options array  */

function create_form_webcaster($options) { 
    $user_id = get_current_user_id();
    $els_webcaster_account = get_user_meta($user_id, 'els_webcaster_account', true);
    //$els_onstream_password = get_user_meta($user_id, 'els_onstream_password', true);
?>

<form id="options_form" method="post" name="form">
<img src="/wp-content/plugins/admin-pages/admin/assets/global/img/webcaster-integration.png" width="237" height="110" border="0">
<p class="font-blue-madison">Enter Account Information below</p>
<div class="suf-section fix">
<input type="text" id="els_webcaster_account" placeholder="enter your account ID" name="els_webcaster_account" value="<?php echo $els_webcaster_account; ?>">
</div>

 
        <input name="save" type="button" value="Save" onclick="submit_form(this, document.forms['form'])" class="button edit_design btn dark btn-md sbold uppercase green-steel">
        <input type="hidden" name="formaction" value="default">
    
     <script> function submit_form(element, form){ 
                 form['formaction'].value = element.name;
                 form.submit();
             } </script>
    
        </form>
<?php }

    $options = array( 
        array("name" => "Visual Webcaster Account",
                "desc" => " ",
                "id" => "els_webcaster_account",
                "type" => "text",
                "parent" => "onstream",
                "std" => ""),
      );


add_action('admin_menu', 'mynewtheme_add_admin_webcast');   
function mynewtheme_add_admin_webcast() { 
    global $themename, $shortname, $options;
     $user_id = get_current_user_id();

    if ( $_GET['page'] == basename(__FILE__) ) {
        if ( 'save' == $_POST['formaction'] ) {
             update_user_meta($user_id, 'els_webcaster_account', $_REQUEST['els_webcaster_account']);
             //update_user_meta($user_id, 'els_onstream_password', $_REQUEST['els_onstream_password']);
             header("Location: admin.php?page=visualwebcaster-options.php&saved=true");
            die;
        }
  }

}

function mynewtheme_admin_webcast() { 
    global $themename, $shortname, $options, $theme_name;
 
    if ($_REQUEST['saved']) {
        echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved for this page.</strong></p></div>';
    }
?>
<div class="wrap">
<style>
    h3.suf-header-3, .suf-section.fix h3 {
        font-size: 20px;
        font-weight: 400;
        margin: 20px 0 0;
        line-height: 29px;
        float: left;
        padding: 0;
        width: 100%;
    }        
    .suf-section.fix {
        margin-bottom: 20px;
    }
</style>
<div class="mnt-options">
<?php
    create_form_webcaster($options);
?>
    </div><!-- mnt-options -->
</div><!-- wrap -->
<?php } // end function mynewtheme_admin()
?>
