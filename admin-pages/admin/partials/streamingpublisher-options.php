<?php

/** *************************** RENDER OPTIONS PAGE ********************************
 *******************************************************************************
 * This function renders the options page 
 */

    $themename = "Encompass LMS";
    $shortname = "els";

    /* functions to andale the options array  */
require_once( ABSPATH . '/wp-content/plugins/lms-helper-media/requests.php');

function create_form_sp($options) { 
    $user_id = get_current_user_id();
    $els_streamingpublisher_account = get_option('els_streamingpublisher_account');
    $els_streamingpublisher_password = get_option('els_streamingpublisher_password');
?>

<form id="options_form" method="post" name="form">
<img src="/wp-content/plugins/admin-pages/admin/assets/global/img/Onstream_SP_logo.png" width="191" height="44" border="0">
<p class="font-blue-madison">Enter Account Information below</p>
<div class="suf-section fix">
<input type="text" id="els_streamingpublisher_account" placeholder="enter your account ID" name="els_streamingpublisher_account" value="<?php echo $els_streamingpublisher_account; ?>">
<br />
<br />
<input type="password" id="els_streamingpublisher_password" placeholder="enter your account ID" name="els_streamingpublisher_password" value="<?php echo $els_streamingpublisher_password; ?>">
</div>

 
        <input name="save" type="button" value="Save" onclick="submit_form(this, document.forms['form'])" class="button edit_design btn dark btn-md sbold uppercase green-steel">
        <input type="hidden" name="formaction" value="default">
    
     <script> function submit_form(element, form){ 
                 form['formaction'].value = element.name;
                 form.submit();
             } </script>
    
        </form>
<?php }

    $options = array( 
        array("name" => "Streaming Publisher Account",
                "desc" => " ",
                "id" => "els_streamingpublisher_account",
                "type" => "text",
                "parent" => "onstream",
                "std" => ""),
       array("name" => "Streaming Publisher Password",
                "desc" => " ",
                "id" => "els_streamingpublisher_password",
                "type" => "text",
                "parent" => "onstream",
                "std" => ""),        
      );


add_action('admin_menu', 'mynewtheme_add_admin_sp');   
function mynewtheme_add_admin_sp() { 
    global $themename, $shortname, $options;
     $user_id = get_current_user_id();

    if ( $_GET['page'] == basename(__FILE__) ) {
        if ( 'save' == $_POST['formaction'] ) {
             update_option('els_streamingpublisher_account', $_REQUEST['els_streamingpublisher_account']);
             update_option('els_streamingpublisher_password', $_REQUEST['els_streamingpublisher_password']);

             setCallbackUrlSP($_REQUEST['els_streamingpublisher_account'],$_REQUEST['els_streamingpublisher_password']);

             header("Location: admin.php?page=streamingpublisher-options.php&saved=true");
            die;
        }
  }

}

function mynewtheme_admin_sp() { 
    global $themename, $shortname, $options, $theme_name;
 
    if ($_REQUEST['saved']) {
        echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved for this page.</strong></p></div>';
    }
?>
<div class="wrap">
<style>
    h3.suf-header-3, .suf-section.fix h3 {
        font-size: 20px;
        font-weight: 400;
        margin: 20px 0 0;
        line-height: 29px;
        float: left;
        padding: 0;
        width: 100%;
    }        
    .suf-section.fix {
        margin-bottom: 20px;
    }
</style>
<div class="mnt-options">
<?php
    create_form_sp($options);
?>
    </div><!-- mnt-options -->
</div><!-- wrap -->
<?php } // end function mynewtheme_admin()
?>
