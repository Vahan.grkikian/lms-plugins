<?php

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
  
function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
 
 add_meta_box('custom_home_widget1', 'Home Edit', 'custom_home_edit', 'dashboard', 'normal', 'high');
 add_meta_box('custom_home_widget2', 'Event Add', 'custom_home_events', 'dashboard', 'normal', 'high');
 add_meta_box('custom_home_widget3', 'Course Add', 'custom_home_courses', 'dashboard', 'normal', 'high');
 add_meta_box('custom_home_widget4', 'Reports View', 'custom_home_reports', 'dashboard', 'normal', 'high');
} 
function custom_home_edit() {
    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="dashboard-stat dashboard-stat-v2 blue-steel" target="_blank" href="/wp-admin/post.php?vc_action=vc_inline&post_id='. get_option('page_on_front').'&post_type=page"><div class="visual"><i class="fa fa-dashboard"></i></div><div class="details"><div class="number">Home</div><div class="desc"> <i class="fa fa-plus"></i> Edit </div></div></a></div>';
}
function custom_home_events() {
    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="dashboard-stat dashboard-stat-v2 green-steel" href="'. get_admin_url().'admin.php?page=newevent'.'"><div class="visual"><i class="fa fa-calendar"></i></div><div class="details"><div class="number"> Events </div><div class="desc"> <i class="fa fa-plus"></i> Create New </div></div></a></div>';
}
function custom_home_courses() {
    echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="dashboard-stat dashboard-stat-v2 yellow-gold" href="'. get_admin_url().'post-new.php?post_type=sfwd-courses'.'"><div class="visual"><i class="fa fa-graduation-cap"></i></div><div class="details"><div class="number"> Courses </div><div class="desc"> <i class="fa fa-plus"></i> Create New </div></div></a></div>';
}
function custom_home_reports() {
    echo ' <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="dashboard-stat dashboard-stat-v2 purple" href="/wp-admin/admin.php?page=wc-reports"><div class="visual"><i class="fa fa-bar-chart "></i></div><div class="details"><div class="number"> Reports </div><div class="desc"> <i class="fa fa-plus"></i> View All </div></div></a></div>';
}




add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

function remove_dashboard_widgets () {

      //Completely remove various dashboard widgets (remember they can also be HIDDEN from admin)
      remove_meta_box( 'dashboard_quick_press',   'dashboard', 'side' );      //Quick Press widget
      remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );      //Recent Drafts
      remove_meta_box( 'dashboard_primary',       'dashboard', 'side' );      //WordPress.com Blog
      remove_meta_box( 'dashboard_secondary',     'dashboard', 'side' );      //Other WordPress News
      remove_meta_box( 'dashboard_incoming_links','dashboard', 'normal' );    //Incoming Links
      remove_meta_box( 'dashboard_plugins',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'dashboard_right_now',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'dashboard_activity',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'wpseo-dashboard-overview',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'bbp-dashboard-right-now',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'learndash-propanel-overview',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'learndash-propanel-reporting',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'learndash-propanel-activity',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'learndash-propanel-progress-chart',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'woocommerce_dashboard_recent_reviews',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'woocommerce_dashboard_status',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'example_dashboard_widget',       'dashboard', 'normal' );    //Plugins
      remove_meta_box( 'rg_forms_dashboard',       'dashboard', 'normal' );    //Plugins

}

// force one-column dashboard
function shapeSpace_screen_layout_columns($columns) {
    $columns['dashboard'] = 1;
    return $columns;
}
add_filter('screen_layout_columns', 'shapeSpace_screen_layout_columns');

function shapeSpace_screen_layout_dashboard() { return 1; }
add_filter('get_user_option_screen_layout_dashboard', 'shapeSpace_screen_layout_dashboard');

// list active dashboard widgets
function list_active_dashboard_widgets() {
  global $wp_meta_boxes;
  foreach (array_keys($wp_meta_boxes['dashboard']['normal']['core']) as $name) {
    echo '<div>' . $name . '</div>';
  }
}
//add_action('wp_dashboard_setup', 'list_active_dashboard_widgets');
