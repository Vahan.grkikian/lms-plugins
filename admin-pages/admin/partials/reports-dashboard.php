<?php

/** *************************** RENDER TEST PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function tt_render_reports_page()
{

    ?>
    <div class="row widget-row">
        <h3>Events</h3>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white" href="<?php echo admin_url('admin.php?page=events'); ?>">
                <div class="visual">
                    <i class="fa fa-calendar"></i>
                </div>

                <div class="details">
                    <div class="number">
                        <?php $args = array(
                            'post_type' => array('sfwd-courses'),
                            'post_status' => array('publish'),
                            'meta_query' => array(
                                'relation' => 'AND',
                                array(
                                    'key' => 'course_event',
                                    'value' => '1',
                                    'compare' => '=',
                                ),
                            ),
                        );
                        $the_query = new WP_Query($args); ?>
                        <span data-counter="counterup" data-value="<?php echo $the_query->found_posts; ?>">0</span>
                    </div>
                    <div class="desc"> Total Events</div>
                </div>

            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white"
               href="<?php echo admin_url('admin.php?page=gf_export&view=export_entry'); ?>">
                <div class="visual">
                    <i class="fa fa-user-plus"></i>
                </div>

                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php lms_get_users_count('month'); ?>">0</span>
                    </div>
                    <div class="desc"> New Event Registrations</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white" href="#">
                <div class="visual">
                    <i class="fa fa-dollar"></i>
                </div>

                <div class="details">
                    <div class="number">
                        $<span data-counter="counterup"
                               data-value="<?php getSaleAmountbyCategoryID('event'); ?>">0</span>
                    </div>
                    <div class="desc"> Event Total Sales</div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white" href="#">
                <div class="visual">
                    <i class="fa fa-bar-chart"></i>
                </div>

                <div class="details">
                    <div class="number">
                        $<span data-counter="counterup"
                               data-value="<?php getSaleAmountbyCategoryID('event', 'month'); ?>">0</span>
                    </div>
                    <div class="desc"> Event MTD sales</div>
                </div>
            </a>
        </div>
    </div>

    <div class="row widget-row">
        <h3>Courses</h3>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white" href="<?php echo admin_url('admin.php?page=courses'); ?>">
                <div class="visual">
                    <i class="fa fa-graduation-cap"></i>
                </div>

                <div class="details">
                    <div class="number">
                        <span data-counter="counterup"
                              data-value="<?php $count_courses = wp_count_posts('sfwd-courses');
                              echo $count_courses->publish; ?>">0</span>
                    </div>
                    <div class="desc"> Total Courses</div>
                </div>

            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white"
               href="<?php echo admin_url('users.php?role=customer'); ?>">
                <div class="visual">
                    <i class="fa fa-user-plus"></i>
                </div>

                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php lms_get_users_count('month'); ?>">0</span>
                    </div>
                    <div class="desc"> New Students</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white" href="#">
                <div class="visual">
                    <i class="fa fa-dollar"></i>
                </div>

                <div class="details">
                    <div class="number">
                        $<span data-counter="counterup"
                               data-value="<?php getSaleAmountbyCategoryID('course'); ?>">0</span>
                    </div>
                    <div class="desc"> Course Total Sales</div>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 white" href="#">
                <div class="visual">
                    <i class="fa fa-bar-chart"></i>
                </div>

                <div class="details">
                    <div class="number">
                        $<span data-counter="counterup"
                               data-value="<?php getSaleAmountbyCategoryID('course', 'month'); ?>">0</span>
                    </div>
                    <div class="desc"> Course MTD sales</div>
                </div>
            </a>
        </div>
    </div>

    <div class="row widget-row lms_tab_container">
        <h2>Export</h2>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <form action="" method="post" id="lms_export_form">
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">
                            <label for="lms_export_form">Select A Report</label>
                        </th>
                        <td>
                            <select id="lms_export_form_select" name="lms_export_form" onchange="LmsSelectExportForm(jQuery(this).val());">
                                <option value="">Select a report</option>
                                <option value="course">Course Enrollment</option>
                                <option value="video">Video Engagement</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <button id="lms_submit_button" class="button button-large button-primary" style="display: inline-block;">Generate Export File</button>
            </form>
            <div class="spinner" style="display: none;"></div>
            <div class="lms_analityc_report">
                <table id="lms_analityc_report" class="display">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Company</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip</th>
                        <th>Course Name</th>
                        <th>Date Registered</th>
                        <th>Date Completed</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="lms_analityc_report_video">
                <table id="lms_analityc_report_video" class="display">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Company</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip</th>
                            <th>Lesson Name</th>
                            <th>First Viewed</th>
                            <th>Last Viewed</th>
                            <th>Times Viewed</th>
                            <th>Total View Time</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="clear"></div>
        <div id="Livestream_analitcs_back">
            <?php
            global $wpdb;
            $fm_video_ids = $wpdb->get_results( "SELECT distinct videoid FROM encompass_alpha.wp_player_analytic where videoid LIKE '@%'", OBJECT );

            ?>
            <h2>Livstream Analytics </h2>
            <table id="video_analytic_table" class="display" >
                <thead>
                <tr>
                    <th>Video Id</th>
                    <th>Analytics</th>
                </tr>
                </thead>
                <?php foreach ($fm_video_ids as $fm_video_id): ?>
                    <tr>
                        <td class="video_analytic_shrtct_Id" >  <?php  echo $fm_video_id->videoid;?></td>
                        <td class="video_analytic_shrtct"><div class="video_analytic_shrtct_bg"><span id="<?php  echo $fm_video_id->videoid;?>"></span></div></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
    <div class="modal fade" id="analytic_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Video Analytic</h4>
                    <a href="#" class="export">Export CSV</a>
                </div>
                <div class="modal-body" id="lms_media_video_analytic_modal"></div>
                <div class="modal-footer">
                    <button type="button" class="btn lms_close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="fm_spinner" style="display: none;"></div>
    <?php
}
