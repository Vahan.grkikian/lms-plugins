<?php

require_once(dirname(__FILE__).'/../../../../../wp-config.php');

if( $_POST ){
 
$user_id = get_current_user_id();
$username = get_user_meta($user_id, 'els_onstream_account', true);
$password = get_user_meta($user_id, 'els_onstream_password', true);
if ( !isset($_POST['Instructor']) || empty($_POST['Instructor']) ){
    $response = array(
      'status' => 'failed',
      'message' => "Please select the Instructor on previous step"
    );
    wp_send_json( $response );
    wp_die();
}

$instructor = $_POST['Instructor'];
$user_info = get_userdata($instructor);
$meta = get_user_meta( $instructor );



$baseurl = 'https://join.onstreammedia.com/api/1/';
$parameters = array(
 'topic' => $_POST['topic'],
 'duration' => $_POST['duration'],
 'start_time' => $_POST['start_date'].' '.$_POST['start_time'].':00',
 'timezone' => $_POST['timezone'],
 'invited_participants' => array(
		array(
			'email'=> $user_info->user_email, 
			'first_name'=> $user_info->first_name, 
			'last_name'=> $user_info->last_name, 
			'role'=> 1
		)
	)
);

// encode as JSON
$json = json_encode($parameters);
$postArgs = 'input_type=json&rest_data=' . $json;
$curl = curl_init();
$base64EncodedAuthorizationInformation = base64_encode($username.":".$password);
$authorization = "Basic ".$base64EncodedAuthorizationInformation;
$headers = array();
$headers[] = "Authorization:".$authorization;
curl_setopt($curl, CURLOPT_URL, $baseurl.$username.'/session/format/json');
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
curl_setopt($curl, CURLOPT_POSTFIELDS, $postArgs);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
$result = curl_exec($curl);
curl_close($curl);
echo $result;

}
