<?php

require_once(dirname(__FILE__).'/../../../../../wp-config.php');

if( $_POST ){
 
$user_id = get_current_user_id();
$els_streamingpublisher_account = get_option('els_streamingpublisher_account');
$els_streamingpublisher_password = get_option('els_streamingpublisher_password');
$username = $els_streamingpublisher_account;
$password = $els_streamingpublisher_password;
$userauth = base64_encode($username.':'.$password);
   
	$url_path_str = $_POST['video_url'];
	$file_path_str = $_POST['video_title']; 
	$video_file = basename($url_path_str);


$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://ras1.onstreammedia.com/LMSRest/api/File/CreateFolder",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"foldername\":\"$file_path_str\", \"secure\":\"true\"}",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic $userauth",
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));

$result = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
 $msg = "cURL Error #:" . $err;
} else {

$FTP_SERVER = 'gtw.onstreammedia.com';
$FTP_USERNAME = $els_streamingpublisher_account;
$FTP_USERPASS = $els_streamingpublisher_password;
$filesize = strlen(file_get_contents($url_path_str));

	$ftp_conn = ftp_connect($FTP_SERVER) or die("Could not connect to ".$FTP_SERVER);
	$login = ftp_login($ftp_conn, $username, $password);
	
	$folder_exists = is_dir('ftp://'.$username.':'.$password.'@'.$FTP_SERVER.'/'.$file_path_str);
	if(!$folder_exists)	{  
		//Created
		ftp_mkdir($ftp_conn, $file_path_str); 
	}

	// close connection
	ftp_close($ftp_conn);  

	$ch = curl_init(); 
	$fp = fopen($url_path_str, 'r');  
	curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
	curl_setopt($ch, CURLOPT_URL, 'ftp://gtw.onstreammedia.com/'.$file_path_str.'/'.$video_file);
	curl_setopt($ch, CURLOPT_UPLOAD, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 86400); // 1 Day Timeout
	curl_setopt($ch, CURLOPT_INFILE, $fp);
	curl_setopt($ch, CURLOPT_NOPROGRESS, false);
	curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, 'CURL_callback');
	curl_setopt($ch, CURLOPT_BUFFERSIZE, 128);
	curl_setopt($ch, CURLOPT_INFILESIZE, $filesize);
	$result = curl_exec($ch);
	$error = curl_error($ch);
	if ($error) {	
		$msg = "cURL Error #:" . $error;
	}
	else {
	
		$msg = $result;
	}
	curl_close ($ch);
	
	               $postvideo = array( // Set up the basic post data to insert for our product
                    'post_author'  => $user_id,
                    'post_status'  => 'publish',
                    'post_title'   => $file_path_str.'/'.$video_file,
                    'post_parent'  => '',
                    'post_type'    => 'sp_video_cpt'
                );

    $post_id = wp_insert_post($postvideo); // Insert the post returning the new post id
}
	$return = array('msg' => $msg, 'sourcekeyfile' => $file_path_str.'/'.$video_file, 'post_id' => $post_id);
	
	echo json_encode($return);   

}
