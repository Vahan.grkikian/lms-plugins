<?php

/** *************************** RENDER OPTIONS PAGE ********************************
 *******************************************************************************
 * This function renders the options page 
 */

    $themename = "Encompass LMS";
    $shortname = "els";
    
    /* functions to andale the options array  */
    
    $options2 = array( 
        array("name" => "Onstream Webcaster Integration",
                "type" => "sub-section-3",
                "category" => "onstream",
        ),
        array("name" => "Onstream Account",
                "desc" => "Onstream Username",
                "id" => $shortname."_onstream_webcast_account",
                "type" => "text",
                "parent" => "onstream",
                "std" => ""),
      );
    
  
add_action('admin_menu', 'mynewtheme_add_admin_webcast');   
function mynewtheme_add_admin_webcast() { 
    global $themename, $shortname, $options2, $spawned_options;
 
    if ( $_GET['page'] == basename(__FILE__) ) {
        if ( 'save' == $_REQUEST['formaction'] ) {
            foreach ($options as $value) {
                if( isset( $_REQUEST[ $value['id'] ] ) ) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
                }
                else {
                    delete_option( $value['id'] );
                }
            }
 
            foreach ($spawned_options as $value) {
                if( isset( $_REQUEST[ $value['id'] ] ) ) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
                }
                else {
                    delete_option( $value['id'] );
                }
            }
            header("Location: admin.php?page=onstream-webcast-options.php&saved=true");
            die;
        }
        else if('reset_all' == $_REQUEST['formaction']) {
            foreach ($options as $value) {
                delete_option( $value['id'] );
            }
 
            foreach ($spawned_options as $value) {
                delete_option( $value['id'] );
            }
            header("Location: admin.php?page=onstream-webcast-options.php&".$_REQUEST['formaction']."=true");
            die;
        }
  }

}

function mynewtheme_admin_webcast() { 
    global $themename, $shortname, $options2, $spawned_options, $theme_name;
 
    if ($_REQUEST['saved']) {
        echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved for this page.</strong></p></div>';
    }
    if ($_REQUEST['reset_all']) {
        echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
    }
    ?>
<div class="wrap">
    <style>
h3.suf-header-3, .suf-section.fix h3 {
    font-size: 20px;
    font-weight: 400;
    margin: 20px 0 0;
    line-height: 29px;
    float: left;
    padding: 0;
    width: 100%;
}        
.suf-section.fix {
    margin-bottom: 20px;
}
    </style>
    <div class="mnt-options">
<?php
    create_form($options2);
?>
    </div><!-- mnt-options -->
</div><!-- wrap -->
<?php } // end function mynewtheme_admin()
?>
