<?php

/*************************** LOAD THE BASE CLASS *******************************
 *******************************************************************************
 * The WP_List_Table class isn't automatically available to plugins, so we need
 * to check if it's available and load it if necessary. In this tutorial, we are
 * going to use the WP_List_Table class directly from WordPress core.
 *
 * IMPORTANT:
 * Please note that the WP_List_Table class technically isn't an official API,
 * and it could change at some point in the distant future. Should that happen,
 * I will update this plugin with the most current techniques for your reference
 * immediately.
 *
 * If you are really worried about future compatibility, you can make a copy of
 * the WP_List_Table class (file path is shown just below) to use and distribute
 * with your plugins. If you do that, just remember to change the name of the
 * class to avoid conflicts with core.
 *
 * Since I will be keeping this tutorial up-to-date for the foreseeable future,
 * I am going to work with the copy of the class provided in WordPress core.
 */
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}




/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 * 
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 * 
 * Our theme for this list table is going to be movies.
 */
class Pages_List_Table extends WP_List_Table {
    
    /** ************************************************************************
     * Normally we would be querying data from a database and manipulating that
     * for use in your list table. For this example, we're going to simplify it
     * slightly and create a pre-built array. Think of this as the data that might
     * be returned by $wpdb->query()
     * 
     * In a real-world scenario, you would make your own custom query inside
     * this class' prepare_items() method.
     * 
     * @var array 
     **************************************************************************/
    //var $results = $wpdb->get_results( 'SELECT * FROM wpqd_posts WHERE post_type = post', OBJECT );

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We 
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct(){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'page',     //singular name of the listed records
            'plural'    => 'pages',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }


    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title() 
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as 
     * possible. 
     * 
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     * 
     * For more detailed insight into how columns are handled, take a look at 
     * WP_List_Table::single_row_columns()
     * 
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name){
        switch($column_name){
            case 'page_post_title':
            case 'page_post_url':
            case 'page_post_date':
            case 'page_ID':
            case 'page_Del':
            case 'page_status':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }


    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named 
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     * 
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     * 
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_page_ID($item){
        
        //Build row actions post.php?post=1&action=edit
        if($item['page_post_status'] == 'draft') {
            $actions = '<div class="m-grid m-grid-responsive-xs"><div class="m-grid-row"><div class="col-md-4"><a target="_blank" class="button edit_design btn dark btn-md sbold uppercase blue-steel" href="/wp-admin/post.php?vc_action=vc_inline&post_id='.$item['page_ID'].'&post_type=page"><i class="fa fa-paint-brush"></i>Design</a></div><div class="col-md-3"> <a class="button btn sbold uppercase blue-steel quickedit" href="'.plugin_dir_url( __FILE__ ).'edit-meta-fields.php?post_id='.$item['page_ID'].'" data-target="#ajax" data-toggle="modal" data-url="'.plugin_dir_url( __FILE__ ).'edit-meta-fields.php?post_id='.$item['page_ID'].'" rel="'.$item['page_ID'].'"><i class="icon-wrench"></i>Edit</a></div> <div class="col-md-5"><a target="_blank" class="button view btn dark btn-md sbold uppercase blue-steel" href="/?p='.$item['page_ID'].'&preview=true"><i class="fa fa-eye"></i>Preview</a></div></div>';
        } else {            
            $actions = '<div class="m-grid m-grid-responsive-xs"><div class="m-grid-row"><div class="col-md-4"><a target="_blank" class="button edit_design btn dark btn-md sbold uppercase blue-steel" href="/wp-admin/post.php?vc_action=vc_inline&post_id='.$item['page_ID'].'&post_type=page"><i class="fa fa-paint-brush"></i>Design</a></div><div class="col-md-3"> <a class="button btn sbold uppercase blue-steel quickedit" href="'.plugin_dir_url( __FILE__ ).'edit-meta-fields.php?post_id='.$item['page_ID'].'" data-target="#ajax" data-toggle="modal" data-url="'.plugin_dir_url( __FILE__ ).'edit-meta-fields.php?post_id='.$item['page_ID'].'&post_type=page" rel="'.$item['page_ID'].'"><i class="icon-wrench"></i>Edit</a></div> <div class="col-md-3"><a target="_blank" class="button view btn dark btn-md sbold uppercase blue-steel" href="/?p='.$item['page_ID'].'"><i class="fa fa-eye"></i>View</a></div></div>'; 
        }
        //Return the title contents
        echo $actions;
        //print_r($actions);
    }

    function column_page_status($item){
        echo ucfirst($item['page_post_status']);
    }

    function column_page_Del($item){
        
        //Build row actions post.php?post=1&action=edit
        if($item['page_post_status'] == 'draft') {
            $actions = '<div class="m-grid m-grid-responsive-xs"><div class="m-grid-row"><div class="col-md-2"><a class="button btn red-thunderbird delete_post" href=" '.get_delete_post_link($item['page_ID']).'"><i class="fa fa-times"></i></a></div></div>';
        } else {            
            $actions = '<div class="m-grid m-grid-responsive-xs"><div class="m-grid-row"><div class="col-md-2"><a class="button btn red-thunderbird delete_post" href=" '.get_delete_post_link($item['page_ID']).'"><i class="fa fa-times"></i></a></div></div>'; 
        }
        //Return the title contents
        echo $actions;
        //print_r($actions);
    }

function column_page_post_url($item){
    //Return the url - permalink
       echo '<a target="_blank" class="blue-steel" href="'.esc_url( get_permalink($item['page_ID']) ).'">'.esc_url( get_permalink($item['page_ID']) ).'</a>';
}

function column_page_post_title($item){
        
        //Return the title contents
    if($item['page_post_status'] == 'trash') {
           return '<a target="_blank" class="font-blue-chambray" href="/wp-admin/post.php?vc_action=vc_inline&post_id='.$item['page_ID'].'&post_type=page">'.$item['page_post_title'].'</a> - Deleted';
    } else {
           return '<a target="_blank" class="font-blue-chambray" href="/wp-admin/post.php?vc_action=vc_inline&post_id='.$item['page_ID'].'&post_type=page">'.$item['page_post_title'].'</a>';
    }
    }

function column_page_post_date($item){
        
        //Return the title contents
    $time = strtotime($item['page_post_date']);
    $myFormatForView = date("Y/m/d", $time);
        return sprintf('%1$s',
            /*$1%s*/ $myFormatForView
        );
    }

    /** ************************************************************************
     * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
     * is given special treatment when columns are processed. It ALWAYS needs to
     * have it's own method.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_cb($item){
        return sprintf(
          //  '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['page_ID']                //The value of the checkbox should be the record's id
        );
    }


    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value 
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     * 
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     * 
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
            //'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'page_post_title'     => 'Title',
            'page_post_url'     => 'URL',
            'page_post_date'    => 'Date Created',
            'page_status'    => 'Status',
            'page_ID'  => ' ',
            'page_Del'  => ' '
        );
        return $columns;
    }


    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle), 
     * you will need to register it here. This should return an array where the 
     * key is the column that needs to be sortable, and the value is db column to 
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     * 
     * This method merely defines which columns should be sortable and makes them
     * clickable - it does not handle the actual sorting. You still need to detect
     * the ORDERBY and ORDER querystring variables within prepare_items() and sort
     * your data accordingly (usually by modifying your query).
     * 
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
    function get_sortable_columns() {
        $sortable_columns = array(
            'page_post_title'     => array('page_post_title',false),     //true means it's already sorted
            'page_post_date'    => array('page_post_date', true)
             );
        return $sortable_columns;
    }


    /** ************************************************************************
     * Optional. If you need to include bulk actions in your list table, this is
     * the place to define them. Bulk actions are an associative array in the format
     * 'slug'=>'Visible Title'
     * 
     * If this method returns an empty value, no bulk action will be rendered. If
     * you specify any bulk actions, the bulk actions box will be rendered with
     * the table automatically on display().
     * 
     * Also note that list tables are not automatically wrapped in <form> elements,
     * so you will need to create those manually in order for bulk actions to function.
     * 
     * @return array An associative array containing all the bulk actions: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete'
        );
        //return $actions;
    }


    /** ************************************************************************
     * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
     * For this example package, we will handle it in the class to keep things
     * clean and organized.
     * 
     * @see $this->prepare_items()
     **************************************************************************/
    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }


    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     * 
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 25;
        
        
        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        
        
        /**
         * REQUIRED. Finally, we build an array to be used by the class for column 
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        
        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */
        $this->process_bulk_action();
        
        
        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example 
         * package slightly different than one you might build on your own. In 
         * this example, we'll be using array manipulation to sort and paginate 
         * our data. In a real-world implementation, you will probably want to 
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */
        global $wpdb;

$sql = "SELECT posts_page.ID AS page_ID,
       posts_page.post_date AS page_post_date,
       posts_page.post_date_gmt AS page_post_date_gmt,
       posts_page.post_title AS page_post_title,
       posts_page.post_status AS page_post_status,
       posts_page.post_author AS page_post_author
FROM {$wpdb->prefix}posts AS posts_page
WHERE 1=1 
   AND posts_page.post_status IN ('publish','draft', 'pending', 'future', 'private')
   AND posts_page.post_type = 'page'";

        $data = $wpdb->get_results( $sql, 'ARRAY_A' );

        if( current_user_can( 'show_only_own_posts' ) ){
            $user = wp_get_current_user();
            $current_user_id = $user->ID;
            foreach ($data as $key => $post){
                if ( $post['page_post_author'] != $current_user_id ){
                    unset($data[$key]);
                }
            }
        }

        //$data = $wpdb->get_results( 'SELECT * FROM wpqd_posts WHERE post_type = post', OBJECT );
                
        
        /**
         * This checks for sorting input and sorts the data in our array accordingly.
         * 
         * In a real-world situation involving a database, you would probably want 
         * to handle sorting by passing the 'orderby' and 'order' values directly 
         * to a custom query. The returned data will be pre-sorted, and this array
         * sorting technique would be unnecessary.
         */
        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'page_post_date'; //If no sort, default to title
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'DESC'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');
        
        
        /***********************************************************************
         * ---------------------------------------------------------------------
         * vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
         * 
         * In a real-world situation, this is where you would place your query.
         *
         * For information on making queries in WordPress, see this Codex entry:
         * http://codex.wordpress.org/Class_Reference/wpdb
         * 
         * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         * ---------------------------------------------------------------------
         **********************************************************************/
        
                
        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently 
         * looking at. We'll need this later, so you should always include it in 
         * your own package classes.
         */
        $current_page = $this->get_pagenum();
        
        /**
         * REQUIRED for pagination. Let's check how many items are in our data array. 
         * In real-world use, this would be the total number of items in your database, 
         * without filtering. We'll need this later, so you should always include it 
         * in your own package classes.
         */
        $total_items = count($data);
        
        
        /**
         * The WP_List_Table class does not handle pagination for us, so we need
         * to ensure that the data is trimmed to only the current page. We can use
         * array_slice() to 
         */
        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);
        
        
        
        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where 
         * it can be used by the rest of the class.
         */
        $this->items = $data;
        
        
        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }


}

/** *************************** RENDER PAGE ********************************
 *******************************************************************************
 * This function renders the admin page and the example list table. Although it's
 * possible to call prepare_items() and display() from the constructor, there
 * are often times where you may need to include logic here between those steps,
 * so we've instead called those methods explicitly. It keeps things flexible, and
 * it's the way the list tables are used in the WordPress core.
 */
function tt_render_pages_page(){

function createpage()
{
   $my_post = array(
    'post_title'    => 'New Page',
    'post_content'  => '',
    'post_type'=>'page',
    'post_status'   => 'draft'
);
 
// Insert the post into the database.
$post_id = wp_insert_post( $my_post, $wp_error );
if(!is_wp_error($post_id)){
  //the post is valid
    $url = "/wp-admin/post.php?vc_action=vc_inline&post_id='$post_id'&post_type=page";
    wp_redirect( $url );
}else{
  //there was an error in the post insertion, 
  echo $post_id->get_error_message();
}

}

if(array_key_exists('go',$_POST)){
   createpage();
}


    //Create an instance of our package class...
    $testListTable = new Pages_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $testListTable->prepare_items();
    
    ?>
    <div class="wrap">
        
        <div id="icon-users" class="icon32"><br/></div>
                <h2 class="page-titles">Pages</h2>
                <form method="post" class="new-item"><input class="button edit_design btn dark btn-md sbold uppercase green-steel right" value="Add New" type="submit" name='go'/></form>

        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="posts-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="post" value="<?php echo $_REQUEST['page_ID'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $testListTable->display(); 
              //print_r($data);
?>
        </form>
                                    <div class="modal fade" id="ajax" role="basic" aria-hidden="true"  data-backdrop="static" data-keyboard="false" >
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <img src="<?php echo plugin_dir_url( __FILE__ ).'../assets/global/img/loading-spinner-grey.gif'; ?>" alt="" class="loading">
                                                    <span> &nbsp;&nbsp;Loading... </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>        
<?php
}
