<?php

require_once(dirname(__FILE__) . '/../../../../../wp-config.php');

//Global WordPress
global $wpdb;

$lms_error = array();
$lms_error['message'] = '';
/*
 * Validation
 * */
if (!isset($_POST['eventtype']) || empty($_POST['eventtype'])) $lms_error['message'] = 'Event type,';
if (!isset($_POST['topic']) || empty($_POST['topic'])) $lms_error['message'] .= ' Event title,';
if (!isset($_POST['featured_image_id']) || empty($_POST['featured_image_id'])) $lms_error['message'] .= ' Featured image,';
if (!isset($_POST['start_date']) || empty($_POST['start_date'])) $lms_error['message'] .= ' Start date,';
if (!isset($_POST['start_time']) || empty($_POST['start_time'])) $lms_error['message'] .= ' Start time,';
if (!isset($_POST['duration']) || empty($_POST['duration'])) $lms_error['message'] .= ' Duration,';
if (!isset($_POST['timezone']) || empty($_POST['timezone'])) $lms_error['message'] .= ' Timezone,';
if (!isset($_POST['eventdescription']) || empty($_POST['eventdescription'])) $lms_error['message'] .= ' Event description,';
if (!isset($_POST['Instructor']) || empty($_POST['Instructor'])) $lms_error['message'] .= ' Instructor,';
if (!isset($_POST['regularprice']) || $_POST['regularprice'] == '') $lms_error['message'] .= ' Regular price,';
if (!isset($_POST['maxattendees']) || empty($_POST['maxattendees'])) $lms_error['message'] .= ' Max attendees,';


if ($_POST['eventtype'] == 'webinar' && (!isset($_POST['onstreamid']) || empty($_POST['onstreamid']))) {
    $lms_error['message'] .= ' Webinar id';
} elseif ($_POST['eventtype'] == 'webcast' && (!isset($_POST['webcastid']) || empty($_POST['webcastid']))) {
    $lms_error['message'] .= ' Webcast id';
} elseif ($_POST['eventtype'] == 'sp_upload' && (!isset($_POST['sp_video_upload_file']) || empty($_POST['sp_video_upload_file']))) {
    $lms_error['message'] .= ' Sp uploaded video source key';
}

if (!empty($lms_error['message'])) {
    $lms_error['status'] = 'failed';
    wp_send_json($lms_error);
    wp_die();
}

//lms_show($_POST);die;

//variables submitted
$user_id = get_current_user_id();

//   	$catgroup = $_POST['catgroup'];
$pagename = $_POST['topic'];
$eventtype = $_POST['eventtype'];
$eventdescription = $_POST['eventdescription'];
$featured_image_id = $_POST['featured_image_id'];
$template_selection = $_POST['templatechoice'];
$onstreamid = $_POST['onstreamid'];
$webcastid = $_POST['webcastid'];
$otherid = $_POST['otherid'];
$instructor = $_POST['Instructor'];
$sourcekeyfile = $_POST['sp_video_upload_file'];
$redLocation = '';
$redLocationAction = '';
$_rpwcm_post_restriction_method = $_POST['_rpwcm_post_restriction_method'];
$_rpwcm_only_caps = $_POST['_rpwcm_only_caps'];
$membershipPlanId = '';
if (!empty($_rpwcm_only_caps)) {
    $membershipPlanId = $wpdb->get_results("select post_id from $wpdb->postmeta where meta_value = '" . $_rpwcm_only_caps . "'", ARRAY_A);
}

/*TB*/
$formtable = $wpdb->prefix . "rg_form";
$formid = $wpdb->get_var(
    "
                          SELECT id 
                          FROM $formtable
                          ORDER BY ID DESC limit 0,1
                        "
);
$tablename = $wpdb->prefix . "gf_addon_feed";
$wpdb->insert(
    $tablename,
    array(
        'form_id' => $formid,
        'is_active' => '1',
        'feed_order' => '0',
        'meta' => '{"feedName":"User Registration","feedType":"create","username":"9","first_name":"4.3","last_name":"4.6","nickname":"4.3","displayname":"nickname","email":"6","password":"8","role":"subscriber","userMeta":[{"key":"billing_first_name","value":"4.3","custom_key":""},{"key":"billing_last_name","value":"4.6","custom_key":""},{"key":"billing_email","value":"6","custom_key":""}],"bpMeta":"","sendEmail":"0","userActivationEnable":"0","userActivationValue":"email","feed_condition_conditional_logic":"1","feed_condition_conditional_logic_object":{"conditionalLogic":{"actionType":"show","logicType":"all","rules":[{"fieldId":"10","operator":"is","value":""}]}}}',
        'addon_slug' => 'gravityformsuserregistration'
    )
);


$siteurl = plugin_dir_url(__FILE__) . 'templates/img/';
$template_content = file_get_contents('templates/' . $template_selection . '.txt');
$template_content = str_replace("#formid#", $formid, $template_content);
$template_content = str_replace("#url#", $siteurl, $template_content);

$category_selection = $_POST['post_category'];

$post = array();
$post['post_title'] = $pagename;
$post['post_author'] = $instructor;
$post['post_content'] = $template_content;
$post['post_excerpt'] = $eventdescription;
$post['post_status'] = 'publish';
$post['post_type'] = 'sfwd-courses';
$post['comment_status'] = 'closed';
$post['post_category'] = $category_selection;

// Insert the post into the database
$result = wp_insert_post($post);

update_post_meta($result, 'course_event', '1');
update_post_meta($result, '_thumbnail_id', $featured_image_id);
update_post_meta($result, '_event_type', $eventtype);
update_post_meta($result, '_sourcekeyfile', $sourcekeyfile);
update_post_meta($result, '_onstreamid', $onstreamid);
update_post_meta($result, '_webcastid', $webcastid);
update_post_meta($result, '_otherid', $otherid);
wp_set_post_terms( $result, $category_selection, 'category', false);
update_post_meta($result, '_duration', $_POST['duration']);
update_post_meta($result, '_start_date', $_POST['start_date']);
update_post_meta($result, '_start_time', $_POST['start_time']);
update_post_meta($result, '_timezone', $_POST['timezone']);
if (!empty($membershipPlanId)) {
    add_post_meta($membershipPlanId, '_related_course', $result);
}
add_post_meta($result, '_rpwcm_post_restriction_method', $_rpwcm_post_restriction_method);
foreach ($_rpwcm_only_caps as $_rpwcm_only_cap) {
    add_post_meta($result, '_rpwcm_only_caps', $_rpwcm_only_cap);
}


$postpages2 = array();
$postpages2['post_title'] = 'Thank You Page for ' . $pagename;
$postpages2['post_author'] = $instructor;
if ($eventtype == 'webinar') {
    $postpages2['post_content'] = '[vc_row][vc_column][vc_column_text]Thank You Page for ' . $pagename . '[/vc_column_text][/vc_column][vc_column][vcOnstreamWebinar webcastid="' . $onstreamid . '" buttontext="Join Now"][/vc_column][/vc_row]';
} elseif ($eventtype == 'webcast') {
    $postpages2['post_content'] = '[vc_row][vc_column][vc_column_text]Thank You Page for ' . $pagename . '[/vc_column_text][/vc_column][vc_column][vcOnstreamWebcast webcastid="' . $webcastid . '" buttontext="Join Now"][/vc_column][/vc_row]';
} elseif ($eventtype == 'sp_upload') {
    $postpages2['post_content'] = '[vc_row][vc_column][vc_column_text]Thank You Page for ' . $pagename . '[/vc_column_text][/vc_column][vc_column][vcOnstreamVideo sourcekeyfile="' . $sourcekeyfile . '" playertype="flowplayer" linktype="hls"][/vc_column][/vc_row]';
} elseif ($eventtype == 'webex') {

    $webexData = array(
        'start' => @$_POST['start_date'] . ' ' . $_POST['start_time'],
        'duration' => @$_POST['duration'],
        'timezone' => @$_POST['timezone'],
        'instructor' => @$instructor,
        'event_id' => @$result,
        'title' => @$pagename,
        'members' => @$_POST['maxattendees'],
        'desc' => @$eventdescription
    );
    $url = create_webex_event($webexData,$_POST['webex_pass']);
    if ($url != 'failed') {
        $postpages2['post_content'] = '[vc_row][vc_column][vc_column_text]Thank You Page for ' . $pagename . '[/vc_column_text][/vc_column][vc_column][vc_column_text]Save the Webex Meeting key: ' . $url['key'] . '[/vc_column_text][/vc_column][vc_column][vc_column_text]Save the Webex Meeting password: ' . $url['pass'] . '[/vc_column_text][/vc_column][vc_column][button size="default" url="' . $url['url'] . '" open_new_tab="true" text="Join Now" icon="" type="default" bottom_margin=""][/vc_column][/vc_row]';
    } else {
        $lms_error['status'] = 'failed';
        $lms_error['message'] = 'Your event is not created, please contact with Administrator';
        wp_delete_post($result, true);
        wp_send_json($lms_error);
        wp_die();
    }
} else {
    $postpages2['post_content'] = '[vc_row][vc_column][vc_column_text]Thank You Page for ' . $pagename . '[/vc_column_text][/vc_column][vc_column][vc_video link="' . $otherid . '" width="300" height="250" align="center"][/vc_column][/vc_row]';
}

$postpages2['post_status'] = 'publish';
$postpages2['post_type'] = 'page';
$postpages2['comment_status'] = 'closed';
$postpages2result = wp_insert_post($postpages2, $wp_error);

/*TB*/
if (!$postpages2result) {
    return false;
}

if (!empty($_rpwcm_only_caps)) {
    add_post_meta($postpages2result, '_rpwcm_post_restriction_method', $_rpwcm_post_restriction_method);
    foreach ($_rpwcm_only_caps as $_rpwcm_only_cap) {
        add_post_meta($postpages2result, '_rpwcm_only_caps', $_rpwcm_only_cap);
    }
}

if (isset($_POST['regularprice']) && !empty($_POST['regularprice']) && $_POST['regularprice'] != 0) {
    $redLocation = "add-to-cart={Event Product ID (Hidden Field):11}";
    $redLocationAction = "checkout";
} else {
    $redLocation = '';
    $redLocationAction = get_post($postpages2result)->post_name;
}
$returnValue = json_decode('{"0":{"title":"' . $pagename . '","fields":[{"type":"name","id":4,"label":"Name","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","nameFormat":"advanced","inputs":[{"id":"4.2","label":"Prefix","name":"","choices":[{"text":"Mr.","value":"Mr.","isSelected":false,"price":""},{"text":"Mrs.","value":"Mrs.","isSelected":false,"price":""},{"text":"Miss","value":"Miss","isSelected":false,"price":""},{"text":"Ms.","value":"Ms.","isSelected":false,"price":""},{"text":"Dr.","value":"Dr.","isSelected":false,"price":""},{"text":"Prof.","value":"Prof.","isSelected":false,"price":""},{"text":"Rev.","value":"Rev.","isSelected":false,"price":""}],"isHidden":true,"inputType":"radio"},{"id":"4.3","label":"First","name":""},{"id":"4.4","label":"Middle","name":"","isHidden":true},{"id":"4.6","label":"Last","name":""},{"id":"4.8","label":"Suffix","name":"","isHidden":true}],"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"text","id":7,"label":"Company","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"phone","id":5,"label":"Phone","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"phoneFormat":"standard","formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","form_id":"","productField":"","displayOnly":""},{"type":"email","id":6,"label":"Email","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","emailConfirmEnabled":"","displayOnly":""},{"type":"hidden","id":10,"label":"Is User logged in? (Hidden Field)","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"{user:user_login}","choices":"","conditionalLogic":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"username","id":9,"label":"Username","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":{"actionType":"show","logicType":"all","rules":[{"fieldId":"10","operator":"is","value":""}]},"productField":"","enablePasswordInput":false,"maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""},{"type":"password","id":8,"label":"Password","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":[{"id":"8","label":"Enter Password","name":""},{"id":"8.2","label":"Confirm Password","name":""}],"displayOnly":true,"formId":25,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":{"actionType":"show","logicType":"all","rules":[{"fieldId":"10","operator":"is","value":""}]},"productField":"","passwordStrengthEnabled":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false},{"type":"hidden","id":11,"label":"Event Product ID (Hidden Field)","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":25,"description":"","allowsPrepopulate":true,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"event_product_id","visibility":"visible","noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"displayOnly":""}],"id":25,"useCurrentUserAsAuthor":true,"postContentTemplateEnabled":false,"postTitleTemplateEnabled":false,"postTitleTemplate":"","postContentTemplate":"","lastPageButton":null,"pagination":null,"firstPageCssClass":null,"confirmations":[{"id":"59fa1bc389e40","name":"Default Confirmation - Do not alter","isDefault":true,"type":"redirect","message":"","url":"' . get_site_url() . '\/' . $redLocationAction . '\/","pageId":0,"queryString":"' . $redLocation . '","disableAutoformat":false,"conditionalLogic":[]}],"notifications":[]},"version":"2.2.5"}', true);

$formresult = GFAPI::add_forms($returnValue);


$postproduct = array( // Set up the basic post data to insert for our product
    'post_author' => $instructor,
    'post_excerpt' => $eventdescription,
    'post_status' => 'publish',
    'post_title' => $pagename,
    'post_parent' => '',
    'post_type' => 'product'
);

$post_id = wp_insert_post($postproduct); // Insert the post returning the new post id

if (!$post_id) // If there is no post id something has gone wrong so don't proceed
{
    return false;
}
$num = $_POST['regularprice'];
if (($num == '0') || ($num == '')) {
    $num = '0.00';
}
$formattedNum = number_format($num, 2);
wp_set_object_terms($post_id, 'simple', 'product_type');
update_post_meta($post_id, '_visibility', 'hidden');
update_post_meta($post_id, '_stock_status', 'instock');
update_post_meta($post_id, 'total_sales', '0');
update_post_meta($post_id, '_downloadable', 'no');
update_post_meta($post_id, '_virtual', 'yes');
update_post_meta($post_id, '_regular_price', $formattedNum);
update_post_meta($post_id, '_sale_price', $_POST['saleprice']);
update_post_meta($post_id, '_purchase_note', '');
update_post_meta($post_id, '_featured', 'no');
update_post_meta($post_id, '_weight', '');
update_post_meta($post_id, '_length', '');
update_post_meta($post_id, '_width', '');
update_post_meta($post_id, '_height', '');
update_post_meta($post_id, '_sku', '');
update_post_meta($post_id, '_product_attributes', array());
update_post_meta($post_id, '_sale_price_dates_from', '');
update_post_meta($post_id, '_sale_price_dates_to', '');
update_post_meta($post_id, '_price', $formattedNum);
update_post_meta($post_id, '_sold_individually', '');
update_post_meta($post_id, '_manage_stock', 'yes');
update_post_meta($post_id, '_backorders', 'no');
update_post_meta($post_id, '_stock', $_POST['maxattendees']);

update_post_meta($post_id, '_event_type', $eventtype);
update_post_meta($post_id, '_onstreamid', $onstreamid);
update_post_meta($post_id, '_webcastid', $webcastid);
update_post_meta($post_id, '_otherid', $otherid);
update_post_meta($post_id, '_product_author', $instructor);
update_post_meta($post_id, '_related_course', $result);
update_post_meta($post_id, '_duration', $_POST['duration']);
update_post_meta($post_id, '_start_date', $_POST['start_date']);
update_post_meta($post_id, '_start_time', $_POST['start_time']);
update_post_meta($post_id, '_timezone', $_POST['timezone']);
update_post_meta($post_id, '_sourcekeyfile', $sourcekeyfile);

wp_set_object_terms($post_id, array('event', 'course'), 'product_tag');
//Set product hidden:
$terms = array('exclude-from-catalog', 'exclude-from-search');
wp_set_object_terms($post_id, $terms, 'product_visibility');

update_post_meta($result, '_event_product_id', $post_id);


$postpages = array();
$postpages['post_title'] = 'Landing Page for ' . $pagename;
$postpages['post_author'] = $instructor;
$postpages['post_content'] = $template_content;
$postpages['post_status'] = 'publish';
$postpages['post_type'] = 'page';
$postpages['comment_status'] = 'closed';


// Insert the post into the database
$postpagesresult = wp_insert_post($postpages, $wp_error);

update_post_meta($postpagesresult, '_wp_page_template', 'page-fullwidth.php');
update_post_meta($postpagesresult, '_yoast_wpseo_meta-robots-noindex', '1');
update_post_meta($postpagesresult, '_event_type', $eventtype);
update_post_meta($postpagesresult, '_onstreamid', $onstreamid);
update_post_meta($postpagesresult, '_webcastid', $webcastid);
update_post_meta($postpagesresult, '_otherid', $otherid);
update_post_meta($postpagesresult, '_sourcekeyfile', $sourcekeyfile);
update_post_meta($postpages2result, '_wp_page_template', 'page-fullwidth.php');
update_post_meta($postpages2result, '_yoast_wpseo_meta-robots-noindex', '1');
update_post_meta($postpages2result, '_event_type', $eventtype);
update_post_meta($postpages2result, '_onstreamid', $onstreamid);
update_post_meta($postpages2result, '_webcastid', $webcastid);
update_post_meta($postpages2result, '_otherid', $otherid);
update_post_meta($postpages2result, '_sourcekeyfile', $sourcekeyfile);
update_post_meta($post_id, 'custom-thank-you-page', $postpages2result);
update_post_meta($postpagesresult, '_event_product_id', $post_id);
update_post_meta($postpages2result, '_event_product_id', $post_id);

update_post_meta($result, 'postpagesresult', $postpagesresult);
update_post_meta($result, 'postpages2result', $postpages2result);
update_post_meta($result, 'associatedformID', $formid);
if (!empty($_rpwcm_only_caps)) {
    add_post_meta($postpagesresult, '_rpwcm_post_restriction_method', $_rpwcm_post_restriction_method);
    foreach ($_rpwcm_only_caps as $_rpwcm_only_cap) {
        add_post_meta($postpagesresult, '_rpwcm_only_caps', $_rpwcm_only_cap);
    }
    add_post_meta($membershipPlanId[0], 'plan_events', $result);
}


$news1 = array( // Set up the basic post data to insert for our product
    'post_author' => $instructor,
    'post_content' => 'Reminder 1 for ' . $pagename,
    'post_status' => 'draft',
    'post_title' => 'Reminder 1 for ' . $pagename,
    'post_parent' => '',
    'post_type' => 'newsletter'
);
$news2 = array( // Set up the basic post data to insert for our product
    'post_author' => $instructor,
    'post_content' => 'Reminder 2 for  ' . $pagename,
    'post_status' => 'draft',
    'post_title' => 'Reminder 2 for  ' . $pagename,
    'post_parent' => '',
    'post_type' => 'newsletter'
);

// Insert the post into the database
$news1result = wp_insert_post($news1);
$news2result = wp_insert_post($news2);

update_post_meta($result, 'email1', $news1result);
update_post_meta($result, 'email2', $news2result);
update_post_meta($news1result, '_event_type', $eventtype);
update_post_meta($news1result, '_onstreamid', $onstreamid);
update_post_meta($news1result, '_webcastid', $webcastid);
update_post_meta($news1result, '_otherid', $otherid);
update_post_meta($news2result, '_event_type', $eventtype);
update_post_meta($news2result, '_onstreamid', $onstreamid);
update_post_meta($news2result, '_webcastid', $webcastid);
update_post_meta($news2result, '_otherid', $otherid);
update_post_meta($news1result, '_duration', $_POST['duration']);
update_post_meta($news1result, '_start_date', $_POST['start_date']);
update_post_meta($news1result, '_start_time', $_POST['start_time']);
update_post_meta($news1result, '_timezone', $_POST['timezone']);
update_post_meta($news2result, '_duration', $_POST['duration']);
update_post_meta($news2result, '_start_date', $_POST['start_date']);
update_post_meta($news2result, '_start_time', $_POST['start_time']);
update_post_meta($news2result, '_timezone', $_POST['timezone']);

//        $new_pagename = sanitize_title($pagename);


$error = array("response" => "error", "comment" => "Event Not Created");
$success = array("response" => "success", "comment" => "Event Created");

if (is_wp_error($result)) {
    echo json_encode($error);
} else {
    $listSlug = str_replace(' ', '-', $pagename);
    $date = date('m/d/Y h:i:s a', time());
    $timestamp = strtotime($date);
    $table = $wpdb->prefix . 'mailster_lists';
    $data = array('parent_id' => 0, 'name' => $pagename, 'slug' => $listSlug, 'description' => $pagename, 'added' => $timestamp, 'updated' => $timestamp);
    $format = array('%d', '%s', '%s', '%s', '%d', '%d');
    $wpdb->insert($table, $data, $format);
    $my_id = $wpdb->insert_id;
    if (!is_wp_error($my_id)) {
        add_post_meta($news1result, '_mailster_lists', array($my_id, 1));
        add_post_meta($news2result, '_mailster_lists', array($my_id, 1));

        /*$operator = false;
        $conditions = false;
        mailster( 'campaigns' )->get_totals_by_lists( $my_id, array( 'operator' => $operator, 'conditions' => $conditions ) );*/

        update_post_meta($result, 'event_mail_list', $my_id);
    }

    echo json_encode($formresult);
}
//RebuildLandingList($catgroup);


function create_webex_event($data,$webex_pass)
{

    $return = '';
    $access = require_once ABSPATH . '/wp-content/plugins/lms-helper/webex_config.php';
    $cred = get_user_meta($data['instructor'], 'webex_cred', true);
    $instructorData = get_userdata($data['instructor']);
    $username = @get_user_meta($instructorData->ID, 'first_name', true) . ' ' . @get_user_meta($instructorData->ID, 'last_name', true);
    $address = @get_user_meta($instructorData->ID, 'billing_address_1', true);
    $city = @get_user_meta($instructorData->ID, 'billing_city', true);
    $state = @get_user_meta($instructorData->ID, 'billing_state', true);
    $zip = @get_user_meta($instructorData->ID, 'billing_postcode', true);
    $country = @get_user_meta($instructorData->ID, 'billing_country', true);
    $phone = @get_user_meta($instructorData->ID, 'billing_phone', true);

    $date = date_format(date_create($data["start"]), "m/d/Y H:m:s");
    $zones = array(
        'UM12' => 0,
        'UM11' => 1,
        'UM10' => 2,
        'UM95' => 3,
        'UM9' => 3,
        'UM8' => 4,
        'UM7' => 5,
        'UM6' => 9,
        'UM5' => 12,
        'UM45' => 14,
        'UM4' => 14,
        'UM35' => 15,
        'UM3' => 17,
        'UM2' => 18,
        'UM1' => 19,
        'UTC' => 21,
        'UP1' => 25,
        'UP2' => 31,
        'UP3' => 34,
        'UP35' => 35,
        'UP4' => 37,
        'UP45' => 38,
        'UP5' => 40,
        'UP55' => 41,
        'UP575' => 41,
        'UP6' => 43,
        'UP65' => 43,
        'UP7' => 44,
        'UP8' => 48,
        'UP875' => 48,
        'UP9' => 51,
        'UP95' => 53,
        'UP10' => 55,
        'UP105' => 58,
        'UP11' => 59,
        'UP115' => 60
    );

    $xmlData = '<?xml version="1.0" encoding="UTF-8"?>
                <serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <header>
                        <securityContext>
                            <webExID>'.$instructorData->data->user_email.'</webExID>
                            <password>' . $cred["pass"] . '</password>
                            <siteID>' . $cred["siteID"] . '</siteID>
                            <partnerID>' . $cred["partnerID"] . '</partnerID>
                        </securityContext>
                    </header>
                    <body>
                        <bodyContent
                            xsi:type="java:com.webex.service.binding.meeting.CreateMeeting">
                            <accessControl>
                                <meetingPassword>'.$webex_pass.'</meetingPassword>
                            </accessControl>
                            <schedule>
                                <startDate>' . $date . '</startDate>
                                <duration>' . $data["duration"] . '</duration>
                                <timeZoneID>' . $zones[$data["timezone"]] . '</timeZoneID>
                            </schedule>
                            <metaData>
                                <confName>' . $data["title"] . '</confName>
                                <meetingType>123</meetingType>
                            </metaData>
                            <telephony>
                                <telephonySupport>CALLIN</telephonySupport>
                            </telephony>
                            <participants>
                            <maxUserNumber>' . $data["members"] . '</maxUserNumber>
                                <attendees>
                                    <attendee>
                                        <person>
                                            <name>' . $username . '</name>
                                            <email>'.$instructorData->data->user_email.'</email>
                                        </person>
                                    </attendee>
                                </attendees>
                            </participants>
                        </bodyContent>
                    </body>
                </serv:message>';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $access["url"]);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
    $result = curl_exec($ch);
    $cleanResult = str_replace(':', '', $result);
    $xml = simplexml_load_string($cleanResult);
    $json = json_encode($xml); // convert the XML string to JSON
    $response = json_decode($json, TRUE); // convert the JSON-encoded string to a PHP variable

    if ($response['servheader']['servresponse']['servresult'] == 'SUCCESS') {
        update_post_meta($data['event_id'], 'webex_sessionKey', $response['servbody']['servbodyContent']['meetmeetingkey']);
        update_post_meta($data['event_id'], 'webex_guestToken', $response['servbody']['servbodyContent']['meetguestToken']);
        $return['key'] = $response['servbody']['servbodyContent']['meetmeetingkey'];
        $return['url'] = getJoinUrlMeeting($data['instructor'], $access, $return['key']);
        $return['pass'] = $webex_pass;
    } else {
        $return = 'failed';
    }

    curl_close($ch);

    return $return;
}

function getJoinUrlMeeting($instructor, $access, $key)
{
    $return = '';
    $instructorData = get_userdata($instructor);
    $cred = get_user_meta($instructor, 'webex_cred', true);

    $username = @get_user_meta($instructorData->ID, 'first_name', true) . ' ' . @get_user_meta($instructorData->ID, 'last_name', true);

    $xmlData = '<?xml version="1.0" encoding="UTF-8"?>
                        <serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                            <header>
                                <securityContext>
                                    <webExID>' . $cred["id"] . '</webExID>
                                    <password>' . $cred["pass"] . '</password>
                                    <siteID>' . $cred["siteID"] . '</siteID>
                                    <partnerID>' . $cred["partnerID"] . '</partnerID>
                                    <email>'.$instructorData->data->user_email.'</email>
                                </securityContext>
                            </header>
                            <body>
                                <bodyContent
                                    xsi:type="java:com.webex.service.binding.meeting.GetjoinurlMeeting">
                                    <sessionKey>' . $key . '</sessionKey>
                                    <attendeeName>' . $username . '</attendeeName>
                                </bodyContent>
                            </body>
                        </serv:message>';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $access["url"]);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
    $result = curl_exec($ch);
    $cleanResult = str_replace(':', '', $result);
    $xml = simplexml_load_string($cleanResult);
    $json = json_encode($xml); // convert the XML string to JSON
    $response = json_decode($json, TRUE); // convert the JSON-encoded string to a PHP variable

    if ($response['servheader']['servresponse']['servresult'] == 'SUCCESS') {
        $return = str_replace("//", "://", $response['servbody']['servbodyContent']['meetjoinMeetingURL']);
    } else {
        $return = 'failed';
    }
    curl_close($ch);

    return $return;
}
