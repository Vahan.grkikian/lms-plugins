(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	/*
	* Front Validation
	* */
	$(document).ready(function () {
		$(document).on('click','#submit_form .button-next',function (e) {
			var checker = true;

			if ( $('.tab-pane.active').prev().attr('id') == 'eventtype' ){
				$('.eventchoice').each(function () {
					console.log($(this).hasClass('active'));
					if( $(this).hasClass('active') ){
                        $('.eventchoice').css('border-color','#3598dc');
                        checker = true;
                        return false;
					}
					else{
                        $(this).css('border-color','red');
                        checker = false;
					}
                });
			}

            $('.tab-pane.active').prev().find('.lms_required').each(function () {
				if( !$(this).val() ){
                    $(this).css('border','1px solid red');
                    if ( $(this).attr('name') == 'featured_image_id' ){
						$(this).parent('.form-group').css('border','1px solid red');
					}
                    checker = false;
				}else{
                    $(this).css('border','1px solid #c2cad8');
                    if ( $(this).attr('name') == 'featured_image_id' ){
                        $(this).parent('.form-group').css('border','1px solid #c2cad8');
                    }
				}
            });
			if ( checker == false ){
                e.preventDefault();
                $('.button-previous').trigger('click');
                $('.tab-content .alert-danger').removeClass('display-none');
                $('.tab-content .alert-danger').css('display','block');
                $('html, body').animate({scrollTop:$('.tab-content .alert-danger').position().top}, 'slow');
                return false;
			}
			else{
                $('.tab-content .alert-danger').addClass('display-none');
                $('.tab-content .alert-danger').css('display','none');
			}
        });
    });

})( jQuery );
