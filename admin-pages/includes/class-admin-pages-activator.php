<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    admin_pages
 * @subpackage admin_pages/includes
 * @author     Your Name <email@example.com>
 */
class admin_pages_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
//add_action('init', 'cloneUserRole');
function cloneUserRole()
{
 global $wp_roles;
 if (!isset($wp_roles))
 $wp_roles = new WP_Roles();
 $adm = $wp_roles->get_role('administrator');
 // Adding a new role with all admin caps.
 $wp_roles->add_role('be_overlord', 'Overlord', $adm->capabilities);
}
	}

}
