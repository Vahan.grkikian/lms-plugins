<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/public/partials
 */


// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions_webinar' );
 
function vc_before_init_actions_webinar() {
     
/*
Element Description: Onstream Webcast
*/
 
// Element Class 
class vcOnstreamWebinarLink extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vcOnstreamWebinar_mapping' ) );
        add_shortcode( 'vcOnstreamWebinar', array( $this, 'vcOnstreamWebinar_html' ) );
    }
     
    // Element Mapping
    public function vcOnstreamWebinar_mapping() {
    global $post;
    $user_id = get_current_user_id();
    $webcastid = get_post_meta( $post->ID, '_onstreamid', true );
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
         
        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Onstream Webinar Link', 'text-domain'),
                'base' => 'vcOnstreamWebinar',
                'description' => __('Adding Webinar Link ', 'text-domain'), 
                'category' => __('Onstream', 'text-domain'),   
                'icon' => esc_url( plugins_url( 'images/Onstream_arrows.png', dirname(__FILE__) ) ),            
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Webinar ID', 'text-domain' ),
                        'param_name' => 'webcastid',
                        'value' => __( $webcastid, 'text-domain' ),
                        'description' => __( '', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),  
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Button text', 'text-domain' ),
                        'param_name' => 'buttontext',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'such as "Join Now"', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),                      
                ),
            )
        );                                
        
    }
     
     
    // Element HTML
    public function vcOnstreamWebinar_html( $atts ) {

    $user_id = get_current_user_id();
    global $current_user; get_currentuserinfo();
    global $post;

        // Params extraction
        extract(
            shortcode_atts(
                array(
			'webcastid' => '',
            'buttontext' => '',
                ), 
                $atts
            )
        );

            $webcastid = $atts['webcastid'];
            if (empty($webcastid)) {
                $webcastid = get_post_meta( $post->ID, '_onstreamid', true );
            }

    // GET user_id

        $userID = $post->post_author;
        $current_user_id = get_current_user_id();
        $username = get_user_meta($userID, 'els_onstream_account', true);
        $password = get_user_meta($userID, 'els_onstream_password', true);
 
            $baseurl = 'https://join.onstreammedia.com/api/1/';
            $curl = curl_init(); curl_setopt($curl, CURLOPT_URL, $baseurl.$username.'/user_id/email/'.$current_user->user_email.'/format/json'); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); curl_setopt($curl, CURLOPT_USERPWD, 'apiadmin:apiadm1n');
            $current_user_onstream = json_decode(curl_exec($curl)); curl_close($curl);

        // GET invitees
            $curl = curl_init(); curl_setopt($curl, CURLOPT_URL, $baseurl.$username.'/invitee/id/'.$current_user_onstream[0]->id.'/session_id/'.$webcastid.'/format/json'); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
            $result = json_decode(curl_exec($curl)); curl_close($curl);

            update_user_meta($current_user_id, $webinarid, $result->personal_session_link);

            return '<a target="_blank" class="webinar button" href="'.$result->personal_session_link.'">'.$atts['buttontext'].'</a>';
   }
} // End Element Class
 
 
// Element Class Init
new vcOnstreamWebinarLink();    
     
}

