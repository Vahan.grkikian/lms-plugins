<?php

/* =====================================================================================================================
  Element Description: VC Onstream Webinar Button For Post Grid
  ======================================================================================================================= */

$attributes = array();

add_filter('vc_grid_item_shortcodes', 'onstream_webinar_button_add_grid_shortcodes');

// Initialize Function
function onstream_webinar_button_add_grid_shortcodes($shortcodes) {

    $params = array_merge(array(
        array(
            'type' => 'textfield',
            'heading' => __('Register Text', 'js_composer'),
            'param_name' => 'registertext',
            'value' => '',
            'description' => __('Text On Button For Logged Out User', 'js_composer'),
            "group" => "Webinar Button"
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Buy Text', 'js_composer'),
            'param_name' => 'buytext',
            'value' => '',
            'description' => __('Text On Button For Logged In User Who Has Not Purchased The Product Yet', 'js_composer'),
            "group" => "Webinar Button"
        ),
        array(
            'type' => 'textfield',
            'heading' => __('View Text', 'js_composer'),
            'param_name' => 'viewtext',
            'value' => '',
            'description' => __('Text On Button For Logged In User Who Has Purchased The Product', 'js_composer'),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("Button Alignment", "js_composer"),
            "param_name" => "btn_align",
            "value" => array(
                "Left Align" => "elearning-left",
                "Center Align" => "elearning-center",
                "Right Align" => "elearning-right",
                "Inline" => "elearning-inline",
            ),
            "description" => "",
            "group" => "Webinar Button"
        ),
        array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("Button Size", "js_composer"),
            "param_name" => "btn_size",
            "value" => array(
                __("Normal Button", "js_composer") => "elearning-normal",
                __("Mini Button", "js_composer") => "elearning-mini",
                __("Small Button", "js_composer") => "elearning-small",
                __("Large Button", "js_composer") => "elearning-large",
                __("Button Block", "js_composer") => "elearning-block",
                __("Custom Size", "js_composer") => "elearning-custom",
            ),
            "description" => "",
            "group" => "Webinar Button"
        ),
        array(
            "type" => "number",
            "class" => "",
            "heading" => __("Button Width", "js_composer"),
            "param_name" => "btn_width",
            "value" => "",
            "min" => 10,
            "max" => 1000,
            "suffix" => "px",
            "description" => "",
            "dependency" => array("element" => "btn_size", "value" => "elearning-custom"),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "number",
            "class" => "",
            "heading" => __("Button Height", "js_composer"),
            "param_name" => "btn_height",
            "value" => "",
            "min" => 10,
            "max" => 1000,
            "suffix" => "px",
            "description" => "",
            "dependency" => array("element" => "btn_size", "value" => "elearning-custom"),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "number",
            "class" => "",
            "heading" => __("Button Left Padding", "js_composer"),
            "param_name" => "btn_padding_left",
            "value" => "",
            "min" => 10,
            "max" => 1000,
            "suffix" => "px",
            "description" => "",
            "dependency" => array("element" => "btn_size", "value" => "elearning-custom"),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "number",
            "class" => "",
            "heading" => __("Button Right Padding", "js_composer"),
            "param_name" => "btn_padding_right",
            "value" => "",
            "min" => 10,
            "max" => 1000,
            "suffix" => "px",
            "description" => "",
            "dependency" => array("element" => "btn_size", "value" => "elearning-custom"),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "number",
            "class" => "",
            "heading" => __("Button Top Padding", "js_composer"),
            "param_name" => "btn_padding_top",
            "value" => "",
            "min" => 10,
            "max" => 1000,
            "suffix" => "px",
            "description" => "",
            "dependency" => array("element" => "btn_size", "value" => "elearning-custom"),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "number",
            "class" => "",
            "heading" => __("Button Bottom Padding", "js_composer"),
            "param_name" => "btn_padding_bottom",
            "value" => "",
            "min" => 10,
            "max" => 1000,
            "suffix" => "px",
            "description" => "",
            "dependency" => array("element" => "btn_size", "value" => "elearning-custom"),
            "group" => "Webinar Button"
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Button Class', 'js_composer'),
            'param_name' => 'class',
            'value' => '',
            'description' => __('CSS Classe(s) For Direct Checkout Button', 'js_composer'),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Button Text Color", "js_composer"),
            "param_name" => "color_text",
            "value" => '#FFFFFF',
            "description" => __("Choose button text color", "js_composer"),
            "group" => "Webinar Button"
        ),
        array(
            "type" => "colorpicker",
            "class" => "",
            "heading" => __("Button Background Color", "js_composer"),
            "param_name" => "color_bg",
            "value" => '#FF0000',
            "description" => __("Choose button background color", "js_composer"),
            "group" => "Webinar Button"
        ),
        array(
            'type' => 'css_editor',
            'heading' => __('CSS box', 'js_composer'),
            'param_name' => 'css',
            'group' => __('Design Options', 'js_composer'),
        )
            )
    );

    $shortcodes['onstream_webinar_button'] = array(
        'name' => __('OnStream Webinar Button', 'js_composer'),
        'base' => 'onstream_webinar_button',
        'category' => __('Content', 'my-text-domain'),
        'description' => __('Add OnStream Webinar Button', 'js_composer'),
        'params' => $params,
        'post_type' => Vc_Grid_Item_Editor::postType(),
    );

    return $shortcodes;
}

// Output Function
add_shortcode('onstream_webinar_button', 'onstream_webinar_button_render', 10, 3);

function onstream_webinar_button_render($atts, $content, $tag) {
    global $attributes;
    $attributes = $atts;
    return '{{ onstream_webinar_button }}';
}

add_filter('vc_gitem_template_attribute_onstream_webinar_button', 'vc_gitem_template_attribute_onstream_webinar_button', 10, 2);

function vc_gitem_template_attribute_onstream_webinar_button($value, $data) {
    global $attributes;
    /**
     * @var Wp_Post $post
     * @var string $data
     */
    extract(array_merge(array('post' => null, 'data' => '', 'atts' => $attributes), $data));

    $registertext = $buytext = $viewtext = $class = $color_text = $color_bg = $username = $password = $webinar_id = $recording_id = '';
    $btn_size = $btn_width = $btn_height = $btn_padding_left = $btn_padding_right = $btn_padding_top = $btn_padding_bottom = $btn_align = $webcast_id = $company_id = '';

    $atts = vc_map_get_attributes('onstream_webinar_button', $atts);
    extract($atts);

    global $post;
    $post_id = 0;
    $media_type = '';
    if (isset($post) && $post) {
        $post_id = $post->ID;
        $post_type = get_post_type($post);
        if ($post_type && $post_type == 'product') {
            $post_id = $post_id;
        } else {
            $product_id_value_array = get_post_meta($post_id, 'product_id');
            if ($product_id_value_array && is_array($product_id_value_array) && sizeof($product_id_value_array)) {
                $product_id = $product_id_value_array[0];
                $post_id = $product_id;
            }
        }

        $media_type_value_array = get_post_meta($post_id, 'media_type');
        if ($media_type_value_array && is_array($media_type_value_array) && sizeof($media_type_value_array)) {
            $media_type = $media_type_value_array[0];
        }

        if ($media_type && $media_type != '') {
            if ($media_type == 'webinar' || $media_type == 'recording') {
                if ($media_type == 'webinar') {
                    $webinar_id_value_array = get_post_meta($post_id, 'webinar_id');
                    if ($webinar_id_value_array && is_array($webinar_id_value_array) && sizeof($webinar_id_value_array)) {
                        $webinar_id = $webinar_id_value_array[0];
                    }
                } elseif ($media_type == 'recording') {
                    $recording_id_value_array = get_post_meta($post_id, 'recording_id');
                    if ($recording_id_value_array && is_array($recording_id_value_array) && sizeof($recording_id_value_array)) {
                        $recording_id = $recording_id_value_array[0];
                    }
                }
                $onstream_username_value_array = get_post_meta($post_id, 'onstream_username');
                if ($onstream_username_value_array && is_array($onstream_username_value_array) && sizeof($onstream_username_value_array)) {
                    $username = $onstream_username_value_array[0];
                }
                $onstream_password_value_array = get_post_meta($post_id, 'onstream_password');
                if ($onstream_password_value_array && is_array($onstream_password_value_array) && sizeof($onstream_password_value_array)) {
                    $password = $onstream_password_value_array[0];
                }
            } elseif ($media_type == 'webcast') {
                $webcast_id_value_array = get_post_meta($post_id, 'webcast_id');
                if ($webcast_id_value_array && is_array($webcast_id_value_array) && sizeof($webcast_id_value_array)) {
                    $webcast_id = $webcast_id_value_array[0];
                }
                $company_id_value_array = get_post_meta($post_id, 'company_id');
                if ($company_id_value_array && is_array($company_id_value_array) && sizeof($company_id_value_array)) {
                    $company_id = $company_id_value_array[0];
                }
            }
        }
    }

    $customer_id = get_current_user_id();
    $role = get_user_meta($customer_id, 'elearning_onstream_role', true);

    $class .= ' ' . $btn_align;

    $new_class = '';
    if ($btn_size == 'elearning-custom') {
        $new_class .= 'width:' . $btn_width . 'px !important;';
        $new_class .= 'min-height:' . $btn_height . 'px !important;';
        $new_class .= 'padding-top:' . $btn_padding_top . 'px !important;';
        $new_class .= 'padding-right:' . $btn_padding_right . 'px !important;';
        $new_class .= 'padding-bottom:' . $btn_padding_bottom . 'px !important;';
        $new_class .= 'padding-left:' . $btn_padding_left . 'px !important;';
    } else {
        $class .= ' ' . $btn_size;
    }

    if ($color_text && $color_text != '') {
        $new_class .= 'color:' . $color_text . ' !important;';
    }

    if ($media_type == 'webinar') {
        return do_shortcode('[elearning_checkout_button product_id="' . $post_id . '" username="' . $username . '" password="' . $password . '" role="' . $role . '" webinar_id="' . $webinar_id . '" registertext="' . $registertext . '" buytext="' . $buytext . '" viewtext="' . $viewtext . '" class="' . $class . '" color="' . $color_bg . '" style="' . $new_class . '"]');
    } elseif ($media_type == 'recording') {
        return do_shortcode('[elearning_checkout_button product_id="' . $post_id . '" username="' . $username . '" password="' . $password . '" role="' . $role . '" recording_id="' . $recording_id . '" registertext="' . $registertext . '" buytext="' . $buytext . '" viewtext="' . $viewtext . '" class="' . $class . '" color="' . $color_bg . '" style="' . $new_class . '"]');
    } elseif ($media_type == 'webcast') {
        return do_shortcode('[elearning_checkout_button company_id="' . $company_id . '" webcast_id="' . $webcast_id . '" registertext="' . $registertext . '" buytext="' . $buytext . '" viewtext="' . $viewtext . '" class="' . $class . '" color="' . $color_bg . '" style="' . $new_class . '"]');
    }

    //return vc_include_template_custom('vc_onstream_button.php', array('post' => $post, 'data' => $data, 'atts' => $atts), true);
    //return var_export(get_post_meta($post->ID, 'media_type'), true);
}
