<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/public/partials
 */

//shortcode builder
// create shortcode to list all events in a grid
add_shortcode( 'list-events-grid', 'event_listing_shortcode_grid' );
function event_listing_shortcode_grid( $atts ) {
ob_start();
//print_r($atts);
// Grid Parameters
$counter = 1; // Start the counter
$grids = $atts['columns']; // Grids per row
    switch($grids){
        case 2:$gridstext = "two";break;
        case 3:$gridstext = "three";break;
        case 4:$gridstext = "four";break;
        case 5:$gridstext = "five";break;
        case 6:$gridstext = "six";break;
    }
$titlelength = 20; // Length of the post titles shown below the thumbnails
$current_user = get_currentuserinfo();
$current_user_id = get_current_user_id();
// The Query
$args = array(
    'post_type'              => array( 'sfwd-courses' ),
    'post_status'            => array( 'publish' ),
    'meta_query'             => array(
        'relation' => 'AND',
        array(
            'key'     => 'course_event',
            'value'   => '1',
            'compare' => '=',
        ),
    ),
);
$the_query = new WP_Query($args);
// The Loop

if ($atts['liststyle'] === 'Grid') {
?>

<div class="gridcontainer">
<?php    
while ( $the_query->have_posts() ) :
    $the_query->the_post(); setup_postdata( $the_query->post );

    // GET user_id
        $userID = $the_query->post->post_author;
        $username = get_user_meta($userID, 'els_onstream_account', true);
        $password = get_user_meta($userID, 'els_onstream_password', true);
        $webcaster_account = get_user_meta($userID, 'els_webcaster_account', true);

        $webinarid = get_post_meta( $the_query->post->ID, '_onstreamid', true );
        $webcastid = get_post_meta( $the_query->post->ID, '_webcastid', true );
        $otherid = get_post_meta( $the_query->post->ID, '_otherid', true );
        $has_access = sfwd_lms_has_access( $the_query->post->ID, $current_user_id );

if (!empty($webinarid)) {

    if ( $has_access ) { 
        $userlink = get_user_meta($current_user_id, $webinarid, true);
        if (empty($userlink)) {
            $baseurl = 'https://join.onstreammedia.com/api/1/';
            $curl = curl_init(); curl_setopt($curl, CURLOPT_URL, $baseurl.$username.'/user_id/email/'.$current_user->user_email.'/format/json'); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
            $current_user_onstream = json_decode(curl_exec($curl)); curl_close($curl);
            
        // GET invitees
            $curl = curl_init(); curl_setopt($curl, CURLOPT_URL, $baseurl.$username.'/invitee/id/'.$current_user_onstream[0]->id.'/session_id/'.$webinarid.'/format/json'); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
            $result = json_decode(curl_exec($curl)); curl_close($curl);

            update_user_meta($current_user_id, $webinarid, $result->personal_session_link);

                if ($result->error != 'Not found') {
                    $buttonlink = '<a target="_blank" class="webinar button" href="'.$result->personal_session_link.'">Join Now</a>';
                }        
        } else {
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$userlink.'">Join Now</a>';
        }
    } else {
        $buttonlink = '<a target="_blank" class="webinar button register" href="'.get_post_permalink( $the_query->post->ID ).'">Register Here</a>';
    }
}
if (!empty($webcastid)) {
    if ( $has_access ) { 
        $userlink = get_user_meta($current_user_id, $webcastid, true);
        if (empty($userlink)) {
            $userlink = get_post_meta( $the_query->post->ID, '_webcastid', true );
            $buttonhref = 'http://www.webcaster4.com/Webcast/Page/'.$webcaster_account.'/'.$userlink.'?Std1=' . $current_user->user_firstname . '&amp;Std2=' . $current_user->user_lastname . '&amp;Std4=' . $current_user->user_email;

            update_user_meta($current_user_id, $webcastid, $buttonhref);
            
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$buttonhref.'">Join Now</a>';
     
        } else {
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$userlink.'">Join Now</a>';
        }
    } else {
        $buttonlink = '<a target="_blank" class="webinar button register" href="'.get_post_permalink( $the_query->post->ID ).'">Register Here</a>';
    }
}
if (!empty($otherid)) {
    if ( $has_access ) { 
        $userlink = get_user_meta($current_user_id, $otherid, true);
        if (empty($userlink)) {
            $userlink = get_post_meta( $post->ID, '_otherid', true );
            $buttonhref = the_permalink();

            update_user_meta($current_user_id, $otherid, $buttonhref);
            
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$buttonhref.'">Watch Now</a>';
     
        } else {
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.the_permalink().'" title="'.the_title_attribute().'">Watch Now</a>';
        }
    } else {
        $buttonlink = '<a target="_blank" class="webinar button register" href="'.get_post_permalink( $the_query->post->ID ).'">Register Here</a>';
    }    
}
// Show all columns except the right hand side column
if($counter != $grids) :
?>
<div class="griditemleft <?php echo $gridstext; ?>">
    <div class="postimage">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
    </div><!-- .postimage -->
    <h2 class="postimage-title">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php if (mb_strlen($post->post_title) > $titlelength)
            { echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
        else { the_title(); } 
        echo $buttonlink;
?>
        </a>
    </h2>
</div><!-- .griditemleft -->
<?php
// Show the right hand side column
elseif($counter == $grids) :
?>
<div class="griditemright <?php echo $gridstext; ?>">
    <div class="postimage">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
    </div><!-- .postimage -->
    <h2 class="postimage-title">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php if (mb_strlen($post->post_title) > $titlelength)
            { echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
        else { the_title(); } 
        echo $buttonlink;
?>
        </a>
    </h2>
</div><!-- .griditemright -->

<div class="clear"></div>
<?php
$counter = 0;
endif;
$counter++;
endwhile;
wp_reset_postdata();

?>
</div><!-- .gridcontainer -->   
<?php } else {
?>

<div class="gridcontainer">
<?php    
while ( $the_query->have_posts() ) :
    $the_query->the_post(); setup_postdata( $the_query->post );

    // GET user_id
        $userID = $the_query->post->post_author;
        $username = get_user_meta($userID, 'els_onstream_account', true);
        $password = get_user_meta($userID, 'els_onstream_password', true);

        $webinarid = get_post_meta( $the_query->post->ID, '_onstreamid', true );
        $webcastid = get_post_meta( $the_query->post->ID, '_webcastid', true );
        $otherid = get_post_meta( $the_query->post->ID, '_otherid', true );
        $has_access = sfwd_lms_has_access( $the_query->post->ID, $current_user_id );

if (!empty($webinarid)) {

    if ( $has_access ) { 
        $userlink = get_user_meta($current_user_id, $webinarid, true);
        if (empty($userlink)) {
            $baseurl = 'https://join.onstreammedia.com/api/1/';
            $curl = curl_init(); curl_setopt($curl, CURLOPT_URL, $baseurl.$username.'/user_id/email/'.$current_user->user_email.'/format/json'); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
            $current_user_onstream = json_decode(curl_exec($curl)); curl_close($curl);
            
        // GET invitees
            $curl = curl_init(); curl_setopt($curl, CURLOPT_URL, $baseurl.$username.'/invitee/id/'.$current_user_onstream[0]->id.'/session_id/'.$webinarid.'/format/json'); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
            $result = json_decode(curl_exec($curl)); curl_close($curl);

            update_user_meta($current_user_id, $webinarid, $result->personal_session_link);

                if ($result->error != 'Not found') {
                    $buttonlink = '<a target="_blank" class="webinar button" href="'.$result->personal_session_link.'">Join Now</a>';
                }        
        } else {
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$userlink.'">Join Now</a>';
        }
    } else {
        $buttonlink = '<a target="_blank" class="webinar button register" href="'.get_post_permalink( $the_query->post->ID ).'">Register Here</a>';
    }
}
if (!empty($webcastid)) {
    if ( $has_access ) { 
        $userlink = get_user_meta($current_user_id, $webcastid, true);
        if (empty($userlink)) {
            $userlink = get_post_meta( $the_query->post->ID, '_webcastid', true );
            $buttonhref = 'http://www.webcaster4.com/Webcast/Page/3/'.$userlink.'?Std1=' . $current_user->user_firstname . '&amp;Std2=' . $current_user->user_lastname . '&amp;Std4=' . $current_user->user_email;

            update_user_meta($current_user_id, $webcastid, $buttonhref);
            
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$buttonhref.'">Join Now</a>';
     
        } else {
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$userlink.'">Join Now</a>';
        }
    } else {
        $buttonlink = '<a target="_blank" class="webinar button register" href="'.get_post_permalink( $the_query->post->ID ).'">Register Here</a>';
    }
}
if (!empty($otherid)) {
    if ( $has_access ) { 
        $userlink = get_user_meta($current_user_id, $otherid, true);
        if (empty($userlink)) {
            $userlink = get_post_meta( $post->ID, '_otherid', true );
            $buttonhref = the_permalink();

            update_user_meta($current_user_id, $otherid, $buttonhref);
            
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.$buttonhref.'">Watch Now</a>';
     
        } else {
            $buttonlink = '<a target="_blank" class="webinar button session" href="'.the_permalink().'" title="'.the_title_attribute().'">Watch Now</a>';
        }
    } else {
        $buttonlink = '<a target="_blank" class="webinar button register" href="'.get_post_permalink( $the_query->post->ID ).'">Register Here</a>';
    }    
}
// Show all columns except the right hand side column
?>
<div class="griditem list row col-12">
    <div class="col-2">
        <?php $eventdate = get_post_meta( $the_query->post->ID, '_startdate', true ); ?><?php $date=date_create($eventdate); ?>
        <time datetime="<?php echo $eventdate; ?>" class="icon">
            <em><?php echo date_format($date,"l"); ?></em>
            <strong><?php echo date_format($date,"F"); ?></strong>
            <span><?php echo date_format($date,"j"); ?></span>
        </time>
    </div>
    <div class="col-6">
        <h2 class="griditem-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php if (mb_strlen($post->post_title) > $titlelength)
            { echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
            else { the_title(); } 
?>
            </a>
        </h2>
        <?php the_excerpt(); ?>
    </div>
    <div class="col-2">
        <?php echo $buttonlink; ?>
    </div>
</div><!-- .griditemleft -->
<div class="clear"></div>
<?php
endwhile;
wp_reset_postdata();

?>
</div><!-- .gridcontainer -->   

<?php }
    $myvariable = ob_get_clean();
    return $myvariable;
}

// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions_eventlisting' );
 
function vc_before_init_actions_eventlisting() {
     
/*
Element Description: Onstream Webcast
*/
 
// Element Class 
class vcOnstreameventlisting extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vcOnstreameventlisting_mapping' ) );
        add_shortcode( 'vcOnstreameventlisting', array( $this, 'vcOnstreameventlisting_html' ) );
    }
     
    // Element Mapping
    public function vcOnstreameventlisting_mapping() {
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
         
        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Event Listings', 'text-domain'),
                'base' => 'vcOnstreameventlisting',
                'description' => __('Showing a Grid or List of Events with join or register button', 'text-domain'), 
                'category' => __('Onstream', 'text-domain'),   
                'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
                'params' => array(   
                         
                    array(
                        'type' => 'dropdown',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Listing Style', 'text-domain' ),
                        'param_name' => 'liststyle',
                        'value'       => array(
                            'grid'   => 'Grid',
                            'list'   => 'List',
                        ),
                        'std' => 'list', // Your default value
                        'description' => __( 'Choose between a Grid style with images or a List style' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),   
                    array(
                        'type' => 'dropdown',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Columns', 'text-domain' ),
                        'param_name' => 'columns',
                        'value'       => array(
                            '2'   => '2',
                            '3'   => '3',
                            '4'   => '4',
                            '5'   => '5',
                            '6'   => '6',
                        ),
                        'std' => '5', // Your default value
                        'description' => __( 'In Grid Listing - columns across - per row', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),                                                              
                ),
            )
        );                                
        
    }
     
     
    // Element HTML
    public function vcOnstreameventlisting_html( $atts ) {

        // Params extraction
        extract(
            shortcode_atts(
                array(
			'liststyle' => '',
            'columns' => '',
                ), 
                $atts
            )
        );

           return do_shortcode('[list-events-grid liststyle="'.$atts['liststyle'].'" columns="'.$atts['columns'].'"]');
   }
} // End Element Class
 
 
// Element Class Init
new vcOnstreameventlisting();    
     
}
