<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/public/partials
 */


// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions_webcast' );
 
function vc_before_init_actions_webcast() {
     
/*
Element Description: Onstream Webcast
*/
 
// Element Class 
class vcOnstreamWebcastLink extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vcOnstreamWebcast_mapping' ) );
        add_shortcode( 'vcOnstreamWebcast', array( $this, 'vcOnstreamWebcast_html' ) );
    }
     
    // Element Mapping
    public function vcOnstreamWebcast_mapping() {
    global $post;
    $user_id = get_current_user_id();
    $webcastid = get_post_meta( $post->ID, '_webcastid', true );
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
         
        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Onstream Webcast Link', 'text-domain'),
                'base' => 'vcOnstreamWebcast',
                'description' => __('Adding Webcast Link ', 'text-domain'), 
                'category' => __('Onstream', 'text-domain'),   
                'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Webcast ID', 'text-domain' ),
                        'param_name' => 'webcastid',
                        'value' => __( $webcastid, 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),  
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'buttontext', 'text-domain' ),
                        'param_name' => 'buttontext',
                        'value' => __( 'Button Text', 'text-domain' ),
                        'description' => __( 'Button Text', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),                      
                ),
            )
        );                                
        
    }
     
     
    // Element HTML
    public function vcOnstreamWebcast_html( $atts ) {

    $user_id = get_current_user_id();
    global $current_user; get_currentuserinfo();
    global $post;
        $userID = $post->post_author;
        $webcaster_account = get_user_meta($userID, 'els_webcaster_account', true);
        // Params extraction
        extract(
            shortcode_atts(
                array(
			'webcastid' => '',
            'buttontext' => '',
                ), 
                $atts
            )
        );

            $webcastid = $atts['webcastid'];
            if (empty($webcastid)) {
                $webcastid = get_post_meta( $post->ID, '_webcastid', true );
            }
    return '<a class="webcast button" target="_blank" href="//www.webcaster4.com/Webcast/Page/'.$webcaster_account.'/'.$webcastid.'?Std1=' . $current_user->user_firstname . '&amp;Std2=' . $current_user->user_lastname . '&amp;Std4=' . $current_user->user_email.'">'.$atts['buttontext'].'</a>';
   }
} // End Element Class
 
 
// Element Class Init
new vcOnstreamWebcastLink();    
     
}
