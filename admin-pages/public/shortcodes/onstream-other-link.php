<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/public/partials
 */


// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions_other' );
 
function vc_before_init_actions_other() {
     
/*
Element Description: Onstream Webcast
*/
 
// Element Class 
class vcOnstreamOtherLink extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vcOnstreamOther_mapping' ) );
        add_shortcode( 'vcOnstreamOther', array( $this, 'vcOnstreamOther_html' ) );
    }
     
    // Element Mapping
    public function vcOnstreamOther_mapping() {
    global $post;
    $user_id = get_current_user_id();
    $webcastid = get_post_meta( $post->ID, '_otherid', true );
         
        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }
         
        // Map the block with vc_map()
        vc_map( 
            array(
                'name' => __('Onstream Other Link', 'text-domain'),
                'base' => 'vcOnstreamOther',
                'description' => __('Adding Other Video ', 'text-domain'), 
                'category' => __('Onstream', 'text-domain'),   
                'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',            
                'params' => array(   
                         
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'Video URL', 'text-domain' ),
                        'param_name' => 'otherid',
                        'value' => $webcastid,
                        'description' => __( 'We support Animoto, Blip, Cloudup, CollegeHumor, DailyMotion, Facebook, Flickr, FunnyOrDie.com, Hulu, Imgur, Instagram, Issuu, Kickstarter, Meetup.com, Mixcloud, Photobucket, PollDaddy, Reddit, ReverbNation, Scribd, SlideShare, SmugMug, SoundCloud, Speaker Deck, Spotify, TED, Tumblr, Twitter, VideoPress, Vimeo, Vine, WordPress plugin directory, WordPress.tv, YouTube', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),  
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'width', 'text-domain' ),
                        'param_name' => 'width',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Width of Video', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),                      
                    array(
                        'type' => 'textfield',
                        'holder' => 'p',
                        'class' => 'title-class',
                        'heading' => __( 'height', 'text-domain' ),
                        'param_name' => 'height',
                        'value' => __( '', 'text-domain' ),
                        'description' => __( 'Height of Video', 'text-domain' ),
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Webcast Data',
                    ),                                          
                ),
            )
        );                                
        
    }
     
     
    // Element HTML
    public function vcOnstreamOther_html( $atts ) {

    $user_id = get_current_user_id();
    global $current_user; get_currentuserinfo();
    global $post;

        // Params extraction
        extract(
            shortcode_atts(
                array(
			'otherid' => '',
            'width' => '',
            'height' => '',
                ), 
                $atts
            )
        );

            $webcastid = $atts['otherid'];
            if (empty($webcastid)) {
                $webcastid = get_post_meta( $post->ID, '_otherid', true );
            }
    
            return do_shortcode('[vc_video link="'.$webcastid.'" width="'.$atts['width'].'" height="'.$atts['height'].' align="center"]');
   }
} // End Element Class
 
 
// Element Class Init
new vcOnstreamOtherLink();    
     
}
