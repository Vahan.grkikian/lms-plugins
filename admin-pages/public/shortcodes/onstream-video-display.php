<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/public/partials
 */


// Add Shortcode [onstreamvideo sourcekeyfile="" playertype="" linktype=""]  ex.. [onstreamvideo sourcekeyfile="FFmpeg/lesson1/Lesson1_Video_bootcamp.mp4" playertype="jwplayer" linktype="hls"]

function onstreamvideo_shortcode( $atts ) {
$els_streamingpublisher_account = get_option('els_streamingpublisher_account');
$els_streamingpublisher_password = get_option('els_streamingpublisher_password');
$username = $els_streamingpublisher_account;
$password = $els_streamingpublisher_password;
$userauth = base64_encode($username.':'.$password);

// Attributes
	$atts = shortcode_atts(
		array(
			'sourcekeyfile' => '',
			'playertype' => '',
			'linktype' => '',
		),
		$atts
	);

$service_url = 'https://ras1.onstreammedia.com/LMSRest/api/File/GetFilePreviewHtml';

$postArgs = json_encode($atts);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $service_url,
  CURLOPT_POST => true,
  CURLOPT_POSTFIELDS => $postArgs,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic $userauth",
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));

    $result = curl_exec($curl);
    curl_close($curl);

	$videoresponse =  json_decode(trim(stripslashes($result),'"'), true);
	  $sourcedata = $videoresponse[Value];
	  return '<embed>'.$sourcedata.'</embed>';

}
add_shortcode( 'onstreamvideo', 'onstreamvideo_shortcode' );


// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions' );

function vc_before_init_actions() {

/*
Element Description: Onstream Video
*/

// Element Class 
class vcOnstreamVideo extends WPBakeryShortCode {

    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vcOnstreamVideo_mapping' ) );
        add_shortcode( 'vcOnstreamVideo', array( $this, 'vcOnstreamVideo_html' ) );
    }

    // Element Mapping
    public function vcOnstreamVideo_mapping() {

        $videocpt_array = array();

        // the query
        $args=array('post_type' => 'sp_video_cpt','order'=>'ASC');

        $posts = get_posts( $args );

        if ( $posts ) {
            foreach ( $posts as $post ) {
                $videocpt_array[] = $post->post_title;
            }
        }

        // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Onstream Video Player', 'text-domain'),
                'base' => 'vcOnstreamVideo',
                'description' => __('Adding video ', 'text-domain'),
                'category' => __('Onstream', 'text-domain'),
                'icon' => get_template_directory_uri().'/assets/img/vc-icon.png',
                'params' => array(

                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Video File', 'text-domain' ),
                        'param_name' => 'sourcekeyfile',
                          "admin_label" => true,
                          "value"       => $videocpt_array, //value
                          "std"         => " ",
                          "description" => __( 'choose video to show', 'text-domain' ),
                        'holder' => 'p',
                        'class' => 'text-class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Video Data',
                    ),

                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Player Type', 'text-domain' ),
                        'param_name' => 'playertype',
                          "admin_label" => true,
                          "value"       => array(
                                                  'jwplayer'   => 'jwplayer',
                                                  'flowplayer'   => 'flowplayer'
                                                ), //value
                          "std"         => " ",
                          "description" => __( 'type of player to be used', 'text-domain' ),
                        'holder' => 'p',
                        'class' => 'text-class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Video Data',
                    ),

                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Link Type', 'text-domain' ),
                        'param_name' => 'linktype',
                          "admin_label" => true,
                          "value"       => array(
                                                  'standard'   => 'standard',
                                                  'hls'   => 'hls',
                                                  'hds'   => 'hds',
                                                  'rtmp'   => 'rtmp',
                                                  'download'   => 'download',
                                                  'oul'   => 'oul'
                                                ), //
                          "std"         => " ",
                          "description" => __( 'type of player to be used', 'text-domain' ),
                        'holder' => 'p',
                        'class' => 'text-class',
                        'admin_label' => false,
                        'weight' => 0,
                        'group' => 'Video Data',
                    ),

                ),
            )
        );

    }


    // Element HTML
    public function vcOnstreamVideo_html( $atts ) {
$els_streamingpublisher_account = get_option('els_streamingpublisher_account');
$els_streamingpublisher_password = get_option('els_streamingpublisher_password');
$username = $els_streamingpublisher_account;
$password = $els_streamingpublisher_password;
$userauth = base64_encode($username.':'.$password);

        // Params extraction
        extract(
            shortcode_atts(
                array(
			'sourcekeyfile' => '',
			'playertype' => '',
			'linktype' => '',
                ),
                $atts
            )
        );

$service_url = 'https://ras1.onstreammedia.com/LMSRest/api/File/GetFilePreviewHtml';

$postArgs = json_encode($atts);

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $service_url,
  CURLOPT_POST => true,
  CURLOPT_POSTFIELDS => $postArgs,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic $userauth",
    "cache-control: no-cache",
    "content-type: application/json"
  ),
));

	$result = curl_exec($curl);
	curl_close($curl);

	$videoresponse =  json_decode(trim(stripslashes($result),'"'), true);
	  $sourcedata = $videoresponse[Value];
	  return '<embed>'.$sourcedata.'</embed>';
    }

} // End Element Class


// Element Class Init
new vcOnstreamVideo();

}
