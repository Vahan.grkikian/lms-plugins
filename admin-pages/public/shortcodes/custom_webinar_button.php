<?php

/* =====================================================================================================================
  Element Description: VC Custom eLearning Onstream Webinar Checkout
  ======================================================================================================================= */

if (!class_exists('VCCustomWebinarButtonShortcodeEncoreSky')) {

    // Element Class 
    class VCCustomWebinarButtonShortcodeEncoreSky {

        protected $fields = array(
            'button_type' => '',
            'recording_id' => '',
            'webinar_id' => '',
            'username' => '',
            'password' => '',
            'company_id' => '',
            'webcast_id' => '',
            'registertext' => '',
            'buytext' => '',
            'viewtext' => '',
            'btn_align' => 'elearning-left',
            'btn_size' => 'elearning-normal',
            'btn_width' => '',
            'btn_height' => '',
            'btn_padding_left' => '',
            'btn_padding_right' => '',
            'btn_padding_top' => '',
            'btn_padding_bottom' => '',
            'class' => '',
            'color_text' => '#FFFFFF',
            'color_bg' => '#FF0000'
        );

        // Element Init
        function __construct() {
            add_action('init', array($this, 'vc_custom_shortcode_encoresky_mapping'));
            add_shortcode('vc_custom_shortcode_encoresky', array($this, 'vc_custom_shortcode_encoresky_html'));
            // Register style sheet.
            add_action('wp_enqueue_scripts', array($this, 'custom_shortcode_plugin_styles'));
        }

        /**
         * Registers a stylesheet.
         */
        public function custom_shortcode_plugin_styles() {
            wp_register_style('ELearning-Stylesheet', plugins_url('elearning-onstream-webinar-checkout/assets/elearning.css'));
            wp_enqueue_style('ELearning-Stylesheet');
        }

        // Element Mapping
        public function vc_custom_shortcode_encoresky_mapping() {

            // Stop all if VC is not enabled
            if (!defined('WPB_VC_VERSION')) {
                return;
            }

            // Map the block with vc_map()
            vc_map(
                    array(
                        'name' => __('eLearning OnStream Checkout Button', 'js_composer'),
                        'base' => 'vc_custom_shortcode_encoresky',
                        'icon' => 'icon-wpb-ui-custom_shortcode',
                        'show_settings_on_create' => true,
                        'category' => __('Custom Elements', 'text-domain'),
                        'description' => __('Text with Google fonts', 'js_composer'),
                        'params' => array(
                            array(
                                'type' => 'dropdown',
                                'heading' => __('Select Type', 'js_composer'),
                                'param_name' => 'button_type',
                                'value' => array(
                                    __('Webinar', 'js_composer') => 'webinar',
                                    __('Recording', 'js_composer') => 'recording',
                                    __('Webcast', 'js_composer') => 'webcast',
                                    __('Product', 'js_composer') => 'product',
                                ),
                                'description' => __('Select type.', 'js_composer'),
                            ),
			    array(
                                'type' => 'textfield',
                                'heading' => __('Product ID', 'js_composer'),
                                'param_name' => 'product_id',
                                'value' => '',
                                'dependency' => array(
                                    'element' => 'button_type',
                                    'value' => array('recording', 'webinar')
                                ),
                                'description' => __('Enter Product ID', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Recording ID', 'js_composer'),
                                'param_name' => 'recording_id',
                                'value' => '',
                                'dependency' => array(
                                    'element' => 'button_type',
                                    'value' => array('recording')
                                ),
                                'description' => __('Enter Recording ID', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Webinar ID', 'js_composer'),
                                'param_name' => 'webinar_id',
                                'value' => '',
                                'dependency' => array(
                                    'element' => 'button_type',
                                    'value' => array('webinar')
                                ),
                                'description' => __('Enter Webinar ID', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Username', 'js_composer'),
                                'param_name' => 'username',
                                'value' => '',
                                'dependency' => array(
                                    'element' => 'button_type',
                                    'value' => array('webinar', 'recording')
                                ),
                                'description' => __('Enter OnStream Username', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Password', 'js_composer'),
                                'param_name' => 'password',
                                'value' => '',
                                'dependency' => array(
                                    'element' => 'button_type',
                                    'value' => array('webinar', 'recording')
                                ),
                                'description' => __('Enter OnStream Password', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Webcast ID', 'js_composer'),
                                'param_name' => 'webcast_id',
                                'value' => '',
                                'dependency' => array(
                                    'element' => 'button_type',
                                    'value' => array('webcast')
                                ),
                                'description' => __('Enter Webcast ID', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Company ID', 'js_composer'),
                                'param_name' => 'company_id',
                                'value' => '',
                                'dependency' => array(
                                    'element' => 'button_type',
                                    'value' => array('webcast')
                                ),
                                'description' => __('Enter Webcast Company ID', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Register Text', 'js_composer'),
                                'param_name' => 'registertext',
                                'value' => '',
                                'description' => __('Text On Button For Logged Out User', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Buy Text', 'js_composer'),
                                'param_name' => 'buytext',
                                'value' => '',
                                'description' => __('Text On Button For Logged In User Who Has Not Purchased The Product Yet', 'js_composer'),
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('View Text', 'js_composer'),
                                'param_name' => 'viewtext',
                                'value' => '',
                                'description' => __('Text On Button For Logged In User Who Has Purchased The Product', 'js_composer'),
                            ),
                            array(
                                "type" => "dropdown",
                                "class" => "",
                                "heading" => __("Button Alignment", "js_composer"),
                                "param_name" => "btn_align",
                                "value" => array(
                                    "Left Align" => "elearning-left",
                                    "Center Align" => "elearning-center",
                                    "Right Align" => "elearning-right",
                                    "Inline" => "elearning-inline",
                                ),
                                "description" => "",
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "dropdown",
                                "class" => "",
                                "heading" => __("Button Size", "js_composer"),
                                "param_name" => "btn_size",
                                "value" => array(
                                    __("Normal Button", "js_composer") => "elearning-normal",
                                    __("Mini Button", "js_composer") => "elearning-mini",
                                    __("Small Button", "js_composer") => "elearning-small",
                                    __("Large Button", "js_composer") => "elearning-large",
                                    __("Button Block", "js_composer") => "elearning-block",
                                    __("Custom Size", "js_composer") => "elearning-custom",
                                ),
                                "description" => "",
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "number",
                                "class" => "",
                                "heading" => __("Button Width", "js_composer"),
                                "param_name" => "btn_width",
                                "value" => "",
                                "min" => 10,
                                "max" => 1000,
                                "suffix" => "px",
                                "description" => "",
                                "dependency" => Array("element" => "btn_size", "value" => "elearning-custom"),
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "number",
                                "class" => "",
                                "heading" => __("Button Height", "js_composer"),
                                "param_name" => "btn_height",
                                "value" => "",
                                "min" => 10,
                                "max" => 1000,
                                "suffix" => "px",
                                "description" => "",
                                "dependency" => Array("element" => "btn_size", "value" => "elearning-custom"),
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "number",
                                "class" => "",
                                "heading" => __("Button Left Padding", "js_composer"),
                                "param_name" => "btn_padding_left",
                                "value" => "",
                                "min" => 10,
                                "max" => 1000,
                                "suffix" => "px",
                                "description" => "",
                                "dependency" => Array("element" => "btn_size", "value" => "elearning-custom"),
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "number",
                                "class" => "",
                                "heading" => __("Button Right Padding", "js_composer"),
                                "param_name" => "btn_padding_right",
                                "value" => "",
                                "min" => 10,
                                "max" => 1000,
                                "suffix" => "px",
                                "description" => "",
                                "dependency" => Array("element" => "btn_size", "value" => "elearning-custom"),
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "number",
                                "class" => "",
                                "heading" => __("Button Top Padding", "js_composer"),
                                "param_name" => "btn_padding_top",
                                "value" => "",
                                "min" => 10,
                                "max" => 1000,
                                "suffix" => "px",
                                "description" => "",
                                "dependency" => Array("element" => "btn_size", "value" => "elearning-custom"),
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "number",
                                "class" => "",
                                "heading" => __("Button Bottom Padding", "js_composer"),
                                "param_name" => "btn_padding_bottom",
                                "value" => "",
                                "min" => 10,
                                "max" => 1000,
                                "suffix" => "px",
                                "description" => "",
                                "dependency" => Array("element" => "btn_size", "value" => "elearning-custom"),
                                "group" => "Styling"
                            ),
                            array(
                                'type' => 'textfield',
                                'heading' => __('Button Class', 'js_composer'),
                                'param_name' => 'class',
                                'value' => '',
                                'description' => __('CSS Classe(s) For Direct Checkout Button', 'js_composer'),
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "colorpicker",
                                "class" => "",
                                "heading" => __("Button Text Color", "js_composer"),
                                "param_name" => "color_text",
                                "value" => '#FFFFFF',
                                "description" => __("Choose button text color", "js_composer"),
                                "group" => "Styling"
                            ),
                            array(
                                "type" => "colorpicker",
                                "class" => "",
                                "heading" => __("Button Background Color", "js_composer"),
                                "param_name" => "color_bg",
                                "value" => '#FF0000',
                                "description" => __("Choose button background color", "js_composer"),
                                "group" => "Styling"
                            )
                        ),
                    )
            );
        }

        // Element HTML
        public function vc_custom_shortcode_encoresky_html($atts) {

            $registertext = $buytext = $viewtext = $class = $color_text = $color_bg = $button_type = $username = $password = $product_id = $webinar_id = $recording_id = '';
            $btn_size = $btn_width = $btn_height = $btn_padding_left = $btn_padding_right = $btn_padding_top = $btn_padding_bottom = $btn_align = $webcast_id = $company_id = '';

            // Params extraction
            extract(shortcode_atts(array(
                'button_type' => '',
                'product_id' => '',
                'recording_id' => '',
                'webinar_id' => '',
                'username' => '',
                'password' => '',
                'company_id' => '',
                'webcast_id' => '',
                'registertext' => '',
                'buytext' => '',
                'viewtext' => '',
                'btn_align' => 'elearning-left',
                'btn_size' => 'elearning-normal',
                'btn_width' => '',
                'btn_height' => '',
                'btn_padding_left' => '',
                'btn_padding_right' => '',
                'btn_padding_top' => '',
                'btn_padding_bottom' => '',
                'class' => '',
                'color_text' => '#FFFFFF',
                'color_bg' => '#FF0000'), $atts));

            $atts = vc_map_get_attributes('vc_custom_shortcode_encoresky', $atts);
            extract($atts);

            global $post;
            $post_id = 0;
            if (isset($post) && $post) {
                $post_id = $post->ID;
            }

            $customer_id = get_current_user_id();
            $role = get_user_meta($customer_id, 'elearning_onstream_role', true);

            $class .= ' ' . $btn_align;

            $new_class = '';
            if ($btn_size == 'elearning-custom') {
                $new_class .= 'width:' . $btn_width . 'px !important;';
                $new_class .= 'min-height:' . $btn_height . 'px !important;';
                $new_class .= 'padding-top:' . $btn_padding_top . 'px !important;';
                $new_class .= 'padding-right:' . $btn_padding_right . 'px !important;';
                $new_class .= 'padding-bottom:' . $btn_padding_bottom . 'px !important;';
                $new_class .= 'padding-left:' . $btn_padding_left . 'px !important;';
            } else {
                $class .= ' ' . $btn_size;
            }

            if ($color_text && $color_text != '') {
                $new_class .= 'color:' . $color_text . ' !important;';
            }

            if ($button_type == 'webinar') {
                return do_shortcode('[elearning_checkout_button product_id="' . $product_id . '" username="' . $username . '" password="' . $password . '" role="' . $role . '" webinar_id="' . $webinar_id . '" registertext="' . $registertext . '" buytext="' . $buytext . '" viewtext="' . $viewtext . '" class="' . $class . '" color="' . $color_bg . '" style="' . $new_class . '"]');
            } elseif ($button_type == 'recording') {
                return do_shortcode('[elearning_checkout_button product_id="' . $product_id . '" username="' . $username . '" password="' . $password . '" role="' . $role . '" recording_id="' . $recording_id . '" registertext="' . $registertext . '" buytext="' . $buytext . '" viewtext="' . $viewtext . '" class="' . $class . '" color="' . $color_bg . '" style="' . $new_class . '"]');
            } elseif ($button_type == 'webcast') {
                return do_shortcode('[elearning_checkout_button company_id="' . $company_id . '" webcast_id="' . $webcast_id . '" registertext="' . $registertext . '" buytext="' . $buytext . '" viewtext="' . $viewtext . '" class="' . $class . '" color="' . $color_bg . '" style="' . $new_class . '"]');
            }
        }

    }

    // Element Class Init
    new VCCustomWebinarButtonShortcodeEncoreSky();

    if (class_exists('WPBakeryShortCode')) {

        class vcELearningEncoreSky_CustomShortcode extends WPBakeryShortCode {
            
        }

    }
}
