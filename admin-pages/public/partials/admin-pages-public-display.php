<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    admin_pages
 * @subpackage admin_pages/public/partials
 */

//change event template to full width
add_filter('template_include', 'event_page_template', 99);

define('MY_PLUGIN_PATH', plugin_dir_path(__FILE__));
function event_page_template($template)
{
    global $post;
    if (is_singular('sfwd-courses')) {
        $my_post_meta = get_post_meta($post->ID, 'course_event', true);
        if (!empty ($my_post_meta)) {
            $new_template = MY_PLUGIN_PATH . '/templates/single-event.php';
            if ('' != $new_template) {
                return $new_template;
            }
        }
    }

    return $template;
}

/**
 * Auto Complete all WooCommerce orders.
 */
add_action('woocommerce_thankyou', 'custom_woocommerce_auto_complete_order');
function custom_woocommerce_auto_complete_order($order_id)
{
    if (!$order_id) {
        return;
    }

    $order = wc_get_order($order_id);
    $order->update_status('completed');
}

//add product ID to form fields
add_filter("gform_field_value_event_product_id", "populate_event_product_id");
function populate_event_product_id($value)
{
    if (get_post_meta(get_the_ID(), '_event_product_id', true) != '') {
        return get_post_meta(get_the_ID(), '_event_product_id', true);
    }
}

add_action('gform_user_registered', 'vc_gf_registration_autologin', 10, 4);
function vc_gf_registration_autologin($user_id, $user_config, $entry, $password)
{
    $user = get_userdata($user_id);
    $user_login = $user->user_login;
    $user_password = $password;
    wp_signon(array(
        'user_login' => $user_login,
        'user_password' => $user_password,
        'remember' => false
    ));
}
