<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           admin_pages
 *
 * @wordpress-plugin
 * Plugin Name:       WordPress Plugin Boilerplate
 * Plugin URI:        http://example.com/admin-pages-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Your Name or Your Company
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       admin-pages
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_filter( 'show_admin_bar', '__return_false' );
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-admin-pages-activator.php
 */
function activate_admin_pages() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-admin-pages-activator.php';
	admin_pages_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-admin-pages-deactivator.php
 */
function deactivate_admin_pages() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-admin-pages-deactivator.php';
	admin_pages_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_admin_pages' );
register_deactivation_hook( __FILE__, 'deactivate_admin_pages' );


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'login/login-functions.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-admin-pages.php';
require plugin_dir_path( __FILE__ ) . 'admin/partials/admin-pages-admin-display.php';
require plugin_dir_path( __FILE__ ) . 'public/class-admin-pages-public.php';
require plugin_dir_path( __FILE__ ) . 'public/shortcodes/custom_webinar_button.php';
require plugin_dir_path( __FILE__ ) . 'public/shortcodes/onstream-video-display.php';
require plugin_dir_path( __FILE__ ) . 'public/shortcodes/onstream_webinar_button.php';
require plugin_dir_path( __FILE__ ) . 'public/shortcodes/onstream-webcast-link.php';
require plugin_dir_path( __FILE__ ) . 'public/shortcodes/onstream-webinar-link.php';
require plugin_dir_path( __FILE__ ) . 'public/shortcodes/onstream-other-link.php';
require plugin_dir_path( __FILE__ ) . 'public/shortcodes/customer-event-listing.php';
require plugin_dir_path( __FILE__ ) . 'public/partials/admin-pages-public-display.php';
require plugin_dir_path( __FILE__ ) . 'admin/partials/plugin-options.php';
//require plugin_dir_path( __FILE__ ) . 'admin/partials/visualwebcaster-options.php';
require plugin_dir_path( __FILE__ ) . 'admin/partials/new-event.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_admin_pages() {

	$plugin = new admin_pages();
	$plugin->run();

}
run_admin_pages();

function replace_howdy( $wp_admin_bar ) {
 $my_account=$wp_admin_bar->get_node('my-account'); 
 $newtitle = str_replace( 'Howdy,', 'Hello, ', $my_account->title );
 $wp_admin_bar->add_node( array(
 'id' => 'my-account',
 'title' => $newtitle,
 'href' => '#',
 ) );
 }
 add_filter( 'admin_bar_menu', 'replace_howdy',25 );


function admin_default_page() {
  return '/wp-admin/index.php';
}

add_filter('login_redirect', 'admin_default_page');

//remove_action( 'plugins_loaded', '_wp_customize_include', 10);
//remove_action( 'admin_enqueue_scripts', '_wp_customize_loader_settings', 11);

